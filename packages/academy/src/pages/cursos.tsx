import { useQuery } from '@apollo/client';
import { Card } from '@Components/courses/card';
import Loader from '@Components/loader';
import { PublicCourse } from '@Interfaces/course/course.interface';
import { GSSProps } from '@Interfaces/props/gss-props.interface';
import { createApolloClient } from '@Lib/apollo/apollo-client';
import { getJwtFromCookie } from '@Lib/login/jwt-cookie.utils';
import { isRequestSSR, loadAuthProps } from '@Lib/utils/ssr.utils';
import { getThemeFromCookie } from '@Lib/utils/theme.utils';
import { decode } from 'jsonwebtoken';
import { GraphqlCourse } from 'learnthis-utils';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { FC } from 'react';

export type CoursesProps = {
	courses: PublicCourse[];
};

export const Courses: FC<CoursesProps> = () => {
	const { data } = useQuery(GraphqlCourse.course_public_find);

	const courses = data ? data.course_public_find : null;

	const coursesRendered = courses ? (
		courses.map((course: PublicCourse, index: number) => (
			<div key={index} className='px-1 py-2 mdlg:w-3/12 sm:w-6/12 xs:w-full'>
				<Card course={course} />
			</div>
		))
	) : (
		<Loader />
	);

	return (
		<div className='container-xl py-3'>
			<div className='w-full py-2'>
				<h1 className='text-3xl text-center text-white-dark dark:text-white'>
					Cursos
				</h1>
			</div>
			<div className='flex-c-st w-full flex-wrap'>{coursesRendered}</div>
		</div>
	);
};

export const getServerSideProps: GetServerSideProps = async (
	ctx: GetServerSidePropsContext
) => {
	const props: GSSProps = { lostAuth: false };
	const isSSR = isRequestSSR(ctx.req.url);

	const apolloClient = createApolloClient();
	const jwt = getJwtFromCookie(ctx.req.headers.cookie);

	if (jwt) {
		if (isSSR) {
			const authProps = await loadAuthProps(ctx.res, jwt, apolloClient);

			if (authProps) props.authProps = authProps;
		} else if (!decode(jwt)) props.lostAuth = true;
	}

	const query = GraphqlCourse.course_public_find;

	await apolloClient.query({
		query,
	});

	props.apolloCache = apolloClient.cache.extract();

	props.componentProps = {
		theme: getThemeFromCookie(ctx.req.headers.cookie),
	};

	return {
		props,
	};
};

export default Courses;
