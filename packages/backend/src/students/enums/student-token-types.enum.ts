/**
 * DB token types
 */
export enum StudentTokenTypes {
	RECOVER_TOKEN = 'RecoverToken',
	ACTIVATE_TOKEN = 'ActivateToken',
}
