import { NormalizedCacheObject } from '@apollo/client';
import { StudentProfile } from '@Interfaces/student/student.interface';

/**
 * Interface for authentication properties.
 */
export interface AuthProps {
	student?: StudentProfile;
	jwt?: string;
}

/**
 * Interface for props obtained trough getServerSideProps
 */
export interface GSSProps {
	authProps?: AuthProps;
	lostAuth?: boolean;
	componentProps?: any;
	apolloCache?: NormalizedCacheObject;
}
