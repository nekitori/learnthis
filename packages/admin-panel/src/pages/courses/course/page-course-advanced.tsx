import CourseAdvanced from '@Components/courses/course/course-advanced';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { withAuth } from '@Lib/hoc/withAuth';
import { FC } from 'react';
import { CourseWrapperProps } from 'src/types/props/with-course-props.type';

const PageCourseAdvanced: FC<CourseWrapperProps> = ({ course, refetch }) => (
	<CourseAdvanced course={course} refetch={refetch} />
);

export default withAuth(PageCourseAdvanced, [WorkerRoles.ADMIN]);
