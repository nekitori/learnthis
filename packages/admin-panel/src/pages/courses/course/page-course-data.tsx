import CourseFormData from '@Components/courses/course/course-form-data';
import CourseHeader from '@Components/courses/course/course-header';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { withAuth } from '@Lib/hoc/withAuth';
import { FC } from 'react';
import { CourseWrapperProps } from 'src/types/props/with-course-props.type';

const PageCourseData: FC<CourseWrapperProps> = ({ course, refetch }) => (
	<>
		<CourseHeader course={course} refetch={refetch} />
		<CourseFormData course={course} refetch={refetch} />
	</>
);

export default withAuth(PageCourseData, [WorkerRoles.ADMIN]);
