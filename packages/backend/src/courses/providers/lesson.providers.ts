import { LessonResolver } from '@Courses/resolvers/lesson.resolver';
import { LessonService } from '@Courses/services/lesson.service';
import { Provider } from '@nestjs/common';

export const lessonProviders: Provider[] = [LessonResolver, LessonService];
