import { Lesson } from '@Courses/gql-types/lesson.gqltype';
import { ObjectType } from '@nestjs/graphql';

/**
 * Graphql type: Student progress in a course.
 */
@ObjectType()
export class StudentCourseProgress {
	currentLesson: Lesson;
	lessons: StudentLessonProgress[];
}

/**
 * Graphql type: Student progress in a lesson.
 */
@ObjectType()
class StudentLessonProgress {
	lesson: Lesson;
	completed: boolean;
}
