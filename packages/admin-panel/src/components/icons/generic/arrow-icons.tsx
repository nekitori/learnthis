import { ComponentProps, FC } from 'react';

export const DownArrowIcon: FC<ComponentProps<'svg'>> = props => (
	<svg {...props} fill='none' stroke='currentColor' viewBox='0 0 24 24'>
		<path
			strokeLinecap='round'
			strokeLinejoin='round'
			strokeWidth={2}
			d='M19 9l-7 7-7-7'
		/>
	</svg>
);

export const UpArrowIcon: FC<ComponentProps<'svg'>> = props => (
	<svg {...props} fill='none' stroke='currentColor' viewBox='0 0 24 24'>
		<path
			strokeLinecap='round'
			strokeLinejoin='round'
			strokeWidth={2}
			d='M5 15l7-7 7 7'
		/>
	</svg>
);

export const LeftArrowIcon: FC<ComponentProps<'svg'>> = props => (
	<svg {...props} fill='none' stroke='currentColor' viewBox='0 0 24 24'>
		<path
			strokeLinecap='round'
			strokeLinejoin='round'
			strokeWidth={2}
			d='M15 19l-7-7 7-7'
		/>
	</svg>
);

export const RightArrowIcon: FC<ComponentProps<'svg'>> = props => (
	<svg {...props} fill='none' stroke='currentColor' viewBox='0 0 24 24'>
		<path
			strokeLinecap='round'
			strokeLinejoin='round'
			strokeWidth={2}
			d='M9 5l7 7-7 7'
		/>
	</svg>
);
