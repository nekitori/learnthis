import { registerEnumType } from '@nestjs/graphql';

/**
 * SocialType enum
 */
export enum SocialType {
	GOOGLE = 'GOOGLE',
	GITLAB = 'GITLAB',
	GITHUB = 'GITHUB',
}

registerEnumType(SocialType, { name: 'SocialType' });
