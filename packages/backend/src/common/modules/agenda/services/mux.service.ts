import { Env } from '@Common/enums/env.enum';
import { FOLDER_VIDEOS } from '@Common/utils/file-upload';
import Mux, { Asset, Upload, Webhooks } from '@mux/mux-node';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { promises } from 'fs';
import { join } from 'path';
import { AgendaService } from './agenda.service';

/**
 * Service to handle the management of the MUX video API
 *
 * Entities:
 *	- **Asset**: Piece of media content stored that can be streamed, An asset always have a duration and one or more tracks (audio and video data)
 *		- **Playback ID**: Identifier used to reproduce an asset with a specific policy (public or private)
 */
@Injectable()
export class MuxService {
	private muxClient: Mux;
	/**
	 * Dependency injection.
	 * @param configService Config service
	 */
	constructor(
		private readonly configService: ConfigService,
		private readonly agendaService: AgendaService
	) {
		this.muxClient = new Mux(
			this.configService.get(Env.MUX_TOKEN_ID),
			this.configService.get(Env.MUX_TOKEN_SECRET)
		);
	}

	/**
	 *
	 * @param fileName Name of video file
	 * @param signed Indicates if the video is public or signed
	 */
	private async uploadVideo(
		fileName: string,
		signed: boolean
	): Promise<Upload> {
		const path = join(FOLDER_VIDEOS, fileName);
		await promises.access(path);

		const { Video } = this.muxClient;

		let upload = await Video.Uploads.create({
			new_asset_settings: { playback_policy: signed ? 'signed' : 'public' },
		});

		this.agendaService.enQueueUploadVideoMux(path, upload.url);

		return upload;
	}

	/**
	 * Upload a video file and generate a public url
	 * @param fileName Name of video file
	 */
	async uploadPublicVideo(fileName: string): Promise<Upload> {
		return await this.uploadVideo(fileName, false);
	}

	/**
	 * Upload a video file and generate a signed url (Require jwt to playback)
	 * @param fileName Name of video file
	 */
	async uploadSignedVideo(fileName: string): Promise<Upload> {
		return await this.uploadVideo(fileName, true);
	}

	/**
	 * Returns a list of all available assets
	 */
	async listAssets(limit: number, page: number = 0): Promise<Asset[]> {
		const { Video } = this.muxClient;
		return await Video.Assets.list({ limit, page });
	}

	/**
	 *
	 * Verify that the call to the webhook is made by the service
	 * @param sig Header signature
	 * @param body Body of HTTP request
	 */
	verifyWebhook(sig: string, body: any): boolean {
		return Webhooks.verifyHeader(
			JSON.stringify(body),
			sig,
			this.configService.get(Env.MUX_WEBHOOK_SECRET)
		);
	}
}
