import { ApolloError, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import Input from '@Components/generic/form/input';
import Head from '@Components/generic/head';
import MainLayout from '@Components/layouts/main.layout';
import { AlertMessages, FormMessages } from '@Enums/config/constants';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { RedirectConditions } from '@Enums/redirect-conditions.enum';
import { ApolloMutation } from '@Interfaces/apollo/apollo-query.types';
import { GSSProps } from '@Interfaces/props/gss-props.interface';
import { IRedirect } from '@Interfaces/redirect.interface';
import { withCSRRedirect } from '@Lib/hoc/with-csr-redirect.hoc';
import { removeJwtCookie } from '@Lib/login/jwt-cookie.utils';
import { serverRedirect } from '@Lib/utils/ssr.utils';
import { getThemeFromCookie } from '@Lib/utils/theme.utils';
import { checkRecoverToken } from '@Lib/utils/user.utils';
import { Form, Formik, FormikConfig } from 'formik';
import { FormValidation, GraphqlStudent } from 'learnthis-utils';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import { FC } from 'react';
import { toast } from 'react-toastify';
import { object as YupObject, ref as YupRef, string as YupString } from 'yup';

//#region TS

/**
 * Enum for form field names
 */
enum ChangePasswordFields {
	NEW_PASSWORD = 'new_password',
	NEW_PASSWORD_CONFIRM = 'password_confirm',
}

/**
 * Interface for form field values
 */
interface IChangePassword {
	[ChangePasswordFields.NEW_PASSWORD]: string;
	[ChangePasswordFields.NEW_PASSWORD_CONFIRM]: string;
}

//#endregion

/**
 * Page component for set new student's password.
 * @param props.token RecoverToken
 */
const RecoverToken: FC<any> = ({ token }) => {
	const { changePasswordMutation, loading } = getChangePasswordMutation();

	const form = getForm(changePasswordMutation, token);

	return (
		<>
			<Head
				title='Cambiar contraseña | LearnThis'
				description='Cambia tu contraseña de LearnThis'
				url={`/recover/${token}`}
			/>
			<MainLayout title='Cambiar contraseña'>
				<Formik {...form}>
					<Form>
						<Input
							className='flexcol-s-s mt-0_5'
							name={ChangePasswordFields.NEW_PASSWORD}
							type='password'
							label='Contraseña'
						/>
						<Input
							className='flexcol-s-s mt-0_5'
							name={ChangePasswordFields.NEW_PASSWORD_CONFIRM}
							type='password'
							label='Confirmar contraseña'
						/>
						<Button
							loading={loading}
							className='mt-1'
							type='submit'
							kind='primary'>
							Cambiar contraseña
						</Button>
					</Form>
				</Formik>
			</MainLayout>
		</>
	);
};

/**
 * Gets the graphql mutation to change student's password.
 */
const getChangePasswordMutation = () => {
	const router = useRouter();

	const [changePasswordMutation, { loading }] = useMutation(
		GraphqlStudent.student_change_forgot_password,
		{
			onCompleted: () => {
				toast.success(AlertMessages.PASSWORD_CHANGED);
				router.push(MainPaths.LOGIN);
			},
			onError: (error: ApolloError) => {
				toast.error(error.message || AlertMessages.SERVER_ERROR);
			},
		}
	);

	return { changePasswordMutation, loading };
};

/**
 * Gets the formik data to build the form.
 * @param changePasswordMutation Graphql mutation
 * @param token Recover token
 */
const getForm = (
	changePasswordMutation: ApolloMutation,
	token: string
): FormikConfig<IChangePassword> => {
	const initialValues: IChangePassword = {
		[ChangePasswordFields.NEW_PASSWORD]: '',
		[ChangePasswordFields.NEW_PASSWORD_CONFIRM]: '',
	};

	const validationSchema = YupObject().shape({
		[ChangePasswordFields.NEW_PASSWORD]: YupString()
			.test('register.password', FormMessages.PASSWORD_ERROR, (value: any) =>
				FormValidation.passwordValidation(value || '')
			)
			.required(FormMessages.PASSWORD_REQUIRED),
		[ChangePasswordFields.NEW_PASSWORD_CONFIRM]: YupString()
			.oneOf(
				[YupRef(ChangePasswordFields.NEW_PASSWORD)],
				FormMessages.PASSWORD_CHECK
			)
			.required(FormMessages.PASSWORD_REQUIRED),
	});

	const onSubmit = async (values: IChangePassword): Promise<void> => {
		changePasswordMutation({
			variables: {
				input: {
					token,
					newPassword: values[ChangePasswordFields.NEW_PASSWORD],
				},
			},
		});
	};

	return {
		initialValues,
		onSubmit,
		validateOnChange: false,
		validationSchema,
	};
};

//#region Next

const redirect: IRedirect = {
	href: MainPaths.NOT_FOUND,
	statusCode: 302,
	condition: RedirectConditions.REDIRECT_WHEN_TOKEN_NOT_EXISTS,
};

export const getServerSideProps: GetServerSideProps = async ({
	params,
	req,
	res,
}) => {
	const props: GSSProps = { lostAuth: false };

	if (params && params.token) {
		const checkToken = await checkRecoverToken(params.token);
		if (checkToken) removeJwtCookie(res);
		else serverRedirect(res, redirect);
	}

	props.componentProps = {
		token: params?.token,
		theme: getThemeFromCookie(req.headers.cookie),
	};

	return { props };
};

//#endregion

export default withCSRRedirect(RecoverToken, redirect);
