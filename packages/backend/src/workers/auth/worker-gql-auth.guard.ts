import { CommonErrors } from '@Common/enums/common-errors.enum';
import { Env } from '@Common/enums/env.enum';
import { ObjectIdTokenPayload } from '@Common/types/objectid-token-payload.type';
import {
	CanActivate,
	ExecutionContext,
	Injectable,
	UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { WorkerService } from '@Workers/services/worker.service';

@Injectable()
export class WorkerGqlAuthGuard implements CanActivate {
	/**
	 * @ignore
	 */
	constructor(
		private readonly jwtservice: JwtService,
		private readonly workerService: WorkerService,
		private readonly configService: ConfigService
	) {}
	/**
	 * Function to generate correct context for passport
	 *
	 * @param  {ExecutionContext} context
	 */
	async canActivate(context: ExecutionContext) {
		const ctx = GqlExecutionContext.create(context);
		const { req } = ctx.getContext();
		const bearerToken: string = req.headers.authorization;
		if (!bearerToken)
			throw new UnauthorizedException(CommonErrors.UNAUTHORIZED);
		const token = bearerToken.replace('Bearer ', '');
		let payload: ObjectIdTokenPayload;
		try {
			payload = await this.jwtservice.verifyAsync(token, {
				secret: this.configService.get(Env.WORKER_TOKEN_KEY),
			});
		} catch (error) {
			throw new UnauthorizedException(CommonErrors.UNAUTHORIZED);
		}
		const { id } = payload;
		const worker = await this.workerService.findById(id);

		if (!worker || !worker.active)
			throw new UnauthorizedException(CommonErrors.UNAUTHORIZED);
		req.user = worker;
		return true;
	}
}
