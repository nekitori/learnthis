import { useQuery } from '@apollo/client';
import ClientsTable from '@Components/clients/table/clients-table';
import BlankCard from '@Components/generic/cards/blank-card';
import TitleCard from '@Components/generic/cards/title-card';
import SectionLayout from '@Components/layout/section.layout';
import Loader from '@Components/other/loader';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { withAuth } from '@Lib/hoc/withAuth';
import { GraphqlStudent } from 'learnthis-utils';
import { FC, useState } from 'react';
import { useLocation } from 'react-router-dom';

/**
 * Page component for clients user accounts management.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 */
const PageClients: FC = () => {
	const { data, loading, page, setPage } = getClients();

	const clients = data && !loading ? data.student_find.data : null;
	const totalPages =
		data && !loading ? Math.ceil(data.student_find.total / 10) : null;

	return (
		<SectionLayout title='Estudiantes'>
			<TitleCard
				className='w-3/4 xssm:w-full'
				title='Estudiantes'
				description='Listado de todos los estudiantes'>
				<ClientsTable
					clients={clients}
					totalPages={totalPages}
					page={page}
					setPage={setPage}
				/>
			</TitleCard>
			<BlankCard className='w-1/4 xssm:w-full'>
				<div className='flex flex-wrap px-1_5 pb-2'>
					<div className='w-1/2 flexcol-s-c'>
						<div className='flex-c-c'>
							<div className='h-1 w-1 bg-pink rounded-md mr-1'></div>
							<span className='font-semibold text-gray'>Estudiantes</span>
						</div>
						<span className='text-4xl font-bold px-2'>
							{data ? data.student_find.total : <Loader />}
						</span>
					</div>
				</div>
			</BlankCard>
		</SectionLayout>
	);
};

const getClients = () => {
	const location = useLocation<string>();

	const [page, setPage] = useState<number>(
		parseInt(new URLSearchParams(location.search).get('page') || '0')
	);
	const { data, loading } = useQuery(GraphqlStudent.student_find, {
		fetchPolicy: 'cache-and-network',
		variables: {
			paginate: {
				offset: page * 10,
				limit: 10,
			},
		},
	});

	return { data, loading, page, setPage };
};

export default withAuth(PageClients, [WorkerRoles.ADMIN]);
