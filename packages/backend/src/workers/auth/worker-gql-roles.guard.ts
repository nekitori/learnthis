import { BaseRoleGuard } from '@Common/auth/base-role.guard';
import { CommonErrors } from '@Common/enums/common-errors.enum';
import {
	CanActivate,
	ExecutionContext,
	ForbiddenException,
	Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';

@Injectable()
export class WorkerGqlRolesGuard extends BaseRoleGuard implements CanActivate {
	constructor(private reflector: Reflector) {
		super();
	}

	canActivate(context: ExecutionContext): boolean {
		const roles = this.reflector.get<WorkerRoles[]>(
			'roles',
			context.getHandler()
		);

		if (!roles) throw new ForbiddenException(CommonErrors.UNAUTHORIZED);

		const user = GqlExecutionContext.create(context).getContext().req.user;

		return this.isUserAllowed(user.roles, roles);
	}
}
