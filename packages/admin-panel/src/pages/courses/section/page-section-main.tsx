import SectionTable from '@Components/courses/section/section-table';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { withAuth } from '@Lib/hoc/withAuth';
import { SectionWrapperProps } from '@Types/props/with-section-props.type';
import { FC } from 'react';

const PageSectionMain: FC<SectionWrapperProps> = ({
	course,
	section,
	sectionId,
}) => {
	const baseUrl = MainPaths.LESSON.replace(
		MainParams.COURSE_ID,
		course?._id || ''
	).replace(MainParams.SECTION_ID, sectionId);

	return (
		<>
			<p className='text-gray mb-1_5'>{section?.description}</p>
			<SectionTable lessons={section?.lessons || null} baseUrl={baseUrl} />
		</>
	);
};

export default withAuth(PageSectionMain);
