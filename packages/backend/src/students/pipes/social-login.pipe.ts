import { Env } from '@Common/enums/env.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SocialLoginDto } from '@Students/dto/social-login.dto';
import { SocialType } from '@Students/enums/social-type.enum';
import { TokenService } from '@Students/services/student-token.service';
import { SocialAccountInfo } from '@Students/types/social-account-info.type';
import CryptoJS from 'crypto-js';

@Injectable()
export class SocialLoginPipe implements PipeTransform {
	constructor(
		private readonly tokenService: TokenService,
		private readonly configService: ConfigService
	) {}

	async transform(value: SocialLoginDto): Promise<SocialAccountInfo> {
		const { token, type } = value;

		const key = CryptoJS.enc.Base64.parse(this.configService.get(Env.AES_KEY));
		const iv = CryptoJS.enc.Base64.parse(this.configService.get(Env.AES_IV));

		const tokenDecrypted = CryptoJS.AES.decrypt(token, key, { iv }).toString(
			CryptoJS.enc.Utf8
		);

		if (type === SocialType.GOOGLE) {
			return await this.tokenService
				.getGoogleAccessTokenInfo(tokenDecrypted)
				.catch(() => {
					throw new BadRequestException();
				});
		} else if (type === SocialType.GITLAB) {
			return await this.tokenService
				.getGitlabAccessTokenInfo(tokenDecrypted)
				.catch(() => {
					throw new BadRequestException();
				});
		} else if (type === SocialType.GITHUB) {
			return await this.tokenService
				.getGithubAccessTokenInfo(tokenDecrypted)
				.catch(() => {
					throw new BadRequestException();
				});
		} else {
			throw new BadRequestException();
		}
	}
}
