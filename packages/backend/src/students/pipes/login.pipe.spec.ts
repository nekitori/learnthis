import { BadRequestException } from '@nestjs/common';
import { LoginDto } from '@Students/dto/login.dto';
import { LoginPipe } from './login.pipe';

describe('LoginPipe', () => {
	let loginPipe: LoginPipe;

	beforeAll(() => (loginPipe = new LoginPipe()));

	it('Email y contraseña correctos', () => {
		const login: LoginDto = { email: 'algo@algo.com', password: 'asd123ASD.' };
		expect(loginPipe.transform(login)).toBe(login);
	});

	it('Email incorrecto y contraseña correcta', () => {
		const login: LoginDto = {
			email: 'algo@algo.com.',
			password: 'asd123ASD.',
		};
		expect(() => loginPipe.transform(login)).toThrow(BadRequestException);
	});

	it('Email correcto y contraseña incorrecta', () => {
		const login: LoginDto = {
			email: 'algo@algo.com',
			password: 'asd12',
		};
		expect(() => loginPipe.transform(login)).toThrow(BadRequestException);
	});

	it('Email incorrecto y contraseña incorrecta', () => {
		const login: LoginDto = {
			email: 'algo@algo.com.',
			password: 'asd12',
		};
		expect(() => loginPipe.transform(login)).toThrow(BadRequestException);
	});
});
