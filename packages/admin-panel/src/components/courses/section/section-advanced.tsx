import { ApolloError, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import { AlertMessages } from '@Enums/config/constants';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { ICourse } from '@Interfaces/course.interface';
import { ISection } from '@Interfaces/section.interface';
import { GraphqlCourseSection } from 'learnthis-utils';
import { FC, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

enum FormStates {
	BASE,
	CONFIRM_PUBLISH,
	CONFIRM_UNPUBLISH,
	CONFIRM_REMOVE_STEP_1,
	CONFIRM_REMOVE_STEP_2,
}

type SectionAdvancedProps = {
	course: ICourse;
	section: ISection;
	sectionId: string;
	refetch: ApolloQueryRefetch;
};

const SectionAdvanced: FC<SectionAdvancedProps> = ({
	course,
	sectionId,
	section,
	refetch,
}) => {
	const [formState, setFormState] = useState<FormStates>(FormStates.BASE);

	const {
		publishSectionMutation,
		unpublishSectionMutation,
		deleteSectionMutation,
		loading,
	} = getMutations(course, sectionId, refetch);

	const confirmPopups: [string, () => void][] = [];
	confirmPopups[FormStates.CONFIRM_PUBLISH] = [
		'¿Está seguro de que desea publicar la sección?',
		() => {
			publishSectionMutation();
			setFormState(FormStates.BASE);
		},
	];
	confirmPopups[FormStates.CONFIRM_UNPUBLISH] = [
		'¿Está seguro de que desea despublicar la sección?',
		() => {
			unpublishSectionMutation();
			setFormState(FormStates.BASE);
		},
	];
	confirmPopups[FormStates.CONFIRM_REMOVE_STEP_1] = [
		'¿Está seguro de que desea eliminar definitivamente la sección?',
		() => setFormState(FormStates.CONFIRM_REMOVE_STEP_2),
	];
	confirmPopups[FormStates.CONFIRM_REMOVE_STEP_2] = [
		'Esta acción no se puede deshacer, ¿está seguro?',
		() => {
			deleteSectionMutation();
			setFormState(FormStates.BASE);
		},
	];

	if (formState !== FormStates.BASE) {
		const popup = confirmPopups[formState];
		return (
			<div className='w-full max-w-20 mx-auto'>
				<p className='text-center mb-1_5 text-black dark:text-white'>
					{popup[0]}
				</p>
				<div className='flex-c-c'>
					<Button
						loading={loading}
						kind='primary'
						className='mr-1'
						onClick={popup[1]}>
						Aceptar
					</Button>
					<Button
						kind='red'
						className='ml-1'
						onClick={() => setFormState(FormStates.BASE)}>
						Cancelar
					</Button>
				</div>
			</div>
		);
	} else {
		return (
			<div className='w-full flex-c-c'>
				<div className='w-full max-w-25'>
					<h2 className='font-semibold mb-0_5 text-black dark:text-white'>
						{section.visibility ? ' Despublicar sección' : 'Publicar sección'}
					</h2>
					<p className='text-black dark:text-white mb-1'>
						{section.visibility
							? 'La sección es pública. Si se despublica, los usuarios no podrán acceder a ella, ni a ninguna de sus lecciones.'
							: 'La sección es privada. Si se publica, los usuarios tendrán acceso a todas las lecciones públicas de la misma.'}
					</p>
					{section.visibility ? (
						<Button
							kind='red'
							onClick={() => setFormState(FormStates.CONFIRM_UNPUBLISH)}>
							Desactivar
						</Button>
					) : (
						<Button
							kind='green'
							onClick={() => setFormState(FormStates.CONFIRM_PUBLISH)}>
							Activar
						</Button>
					)}
					<hr className='m-1_5 border-gray-light dark:border-gray-dark' />
					<h2 className='font-semibold mb-0_5 text-black dark:text-white'>
						Eliminar sección
					</h2>
					<p className='text-black dark:text-white'>
						Esta opción implica eliminar completamente la sección de la base de
						datos. Para ello, es necesario eliminar primero todas sus lecciones.
					</p>
					<p className='my-1 text-black dark:text-white'>
						Esta acción <strong>NO</strong> se puede deshacer.
					</p>
					<Button
						kind='border-red'
						onClick={() => setFormState(FormStates.CONFIRM_REMOVE_STEP_1)}>
						Eliminar permanentemente
					</Button>
				</div>
			</div>
		);
	}
};

const getMutations = (
	course: ICourse,
	sectionId: string,
	refetch: ApolloQueryRefetch
) => {
	const history = useHistory();

	const options = {
		variables: { courseId: course._id, sectionId },
		onError: (error: ApolloError) => {
			toast.error(error.message);
		},
	};

	const [publishSectionMutation, { loading: publishLoading }] = useMutation(
		GraphqlCourseSection.course_admin_section_publish,
		{
			...options,
			onCompleted: () => {
				toast.success(AlertMessages.SECTION_PUBLISHED);
				refetch();
			},
		}
	);

	const [unpublishSectionMutation, { loading: unpublishLoading }] = useMutation(
		GraphqlCourseSection.course_admin_section_unpublish,
		{
			...options,
			onCompleted: () => {
				toast.success(AlertMessages.SECTION_UNPUBLISHED);
				refetch();
			},
		}
	);

	const [deleteSectionMutation, { loading: deleteLoading }] = useMutation(
		GraphqlCourseSection.course_admin_section_delete,
		{
			...options,
			onCompleted: () => {
				toast.success(AlertMessages.SECTION_DELETED);
				refetch();
				history.push(
					MainPaths.COURSE.replace(MainParams.COURSE_ID, course._id)
				);
			},
		}
	);

	return {
		publishSectionMutation,
		unpublishSectionMutation,
		deleteSectionMutation,
		loading: publishLoading || unpublishLoading || deleteLoading,
	};
};

export default SectionAdvanced;
