import gql from 'graphql-tag';

export const student_login = gql`
	query student_login($input: LoginDto!) {
		student_login(input: $input) {
			user {
				email
				name
				surname
				username
				photo
				cart
				coursesEnrolled {
					course
				}
			}
			token
		}
	}
`;

export const student_valid_forgot_password_token = gql`
	query student_valid_forgot_password_token($token: ID!) {
		student_valid_forgot_password_token(token: $token)
	}
`;

export const student_profile = gql`
	query student_profile {
		student_profile {
			email
			name
			surname
			username
			photo
			cart
			coursesEnrolled {
				course
			}
			bio
			gender
			birthDate
			isSocialLogin
			socialAccounts {
				id
				type
			}
		}
	}
`;

export const student_profile_min = gql`
	query student_profile {
		student_profile {
			email
			name
			surname
			username
			photo
			cart
			coursesEnrolled {
				course
			}
		}
	}
`;

export const student_social_login = gql`
	mutation student_social_login($input: SocialLoginDto!) {
		student_social_login(input: $input) {
			token
			user {
				email
				name
				surname
				username
				photo
				isSocialLogin
				cart
				coursesEnrolled {
					course
				}
				socialAccounts {
					id
					type
				}
			}
		}
	}
`;

export const student_link_social_profile = gql`
	mutation student_link_social_profile($input: SocialLoginDto!) {
		student_link_social_profile(input: $input)
	}
`;

export const student_unlink_social_profile = gql`
	mutation student_unlink_social_profile($input: SocialUnlinkDto!) {
		student_unlink_social_profile(input: $input)
	}
`;

export const student_register = gql`
	mutation student_register($input: RegisterDto!) {
		student_register(input: $input)
	}
`;

export const student_activate_account = gql`
	mutation student_activate_account($token: ID!) {
		student_activate_account(token: $token)
	}
`;

export const student_create_forgot_password_token = gql`
	mutation student_create_forgot_password_token($email: String!) {
		student_create_forgot_password_token(email: $email)
	}
`;

export const student_change_forgot_password = gql`
	mutation student_change_forgot_password($input: RecoverPasswordDto!) {
		student_change_forgot_password(input: $input)
	}
`;

export const student_change_password = gql`
	mutation student_change_password($input: ChangePasswordDto!) {
		student_change_password(input: $input)
	}
`;

export const student_modify_profile = gql`
	mutation student_modify_profile($input: ModifyProfileDto!) {
		student_modify_profile(input: $input) {
			name
			surname
		}
	}
`;

export const student_change_email = gql`
	mutation student_change_email($newEmail: String!) {
		student_change_email(newEmail: $newEmail)
	}
`;

export const student_change_username = gql`
	mutation student_change_username($newUsername: String!) {
		student_change_username(newUsername: $newUsername)
	}
`;

export const student_course_progress = gql`
	query student_course_progress($courseUrl: String!) {
		student_course_progress(courseUrl: $courseUrl) {
			currentLesson {
				title
				url
			}
			lessons {
				lesson {
					title
					url
				}
				completed
			}
		}
	}
`;

export const student_update_cart = gql`
	mutation student_update_cart($courses: [ID!]!) {
		student_update_cart(courses: $courses)
	}
`;

export const student_find = gql`
	query student_find($paginate: PaginateDto) {
		student_find(paginate: $paginate) {
			data {
				_id
				coursesEnrolled {
					course
				}
				active
				orders
			}
			offset
			limit
			total
		}
	}
`;
