import { ObjectID } from '@Common/types/object-id.type';
import { IStudentDoc } from '@Students/interfaces/student-document.interface';
import { IWorkerDoc } from '@Workers/interfaces/worker-document.interface';
import { Document } from 'mongoose';
import { ILessonDoc } from './lesson-document.interface';

export type StudentOrObjectID = IStudentDoc | ObjectID;
export type LessonOrObjectID = ILessonDoc | ObjectID;
export type WorkerOrObjectID = IWorkerDoc | ObjectID;

export type TCourseModel = ICourseDoc<
	LessonOrObjectID,
	StudentOrObjectID,
	WorkerOrObjectID
>;

export interface ICourse<
	S extends LessonOrObjectID = ObjectID,
	T extends StudentOrObjectID = ObjectID,
	U extends WorkerOrObjectID = ObjectID
> {
	url: string;
	title: string;
	description: string;
	image?: string;
	background?: string;
	price: number;
	compareAtPrice?: number;
	visibility: boolean;
	studentsEnrolled: T[];
	sections: ISection<S>[];
	tutors: U[];
	studentsCount?: number;
}

export interface ISection<T extends LessonOrObjectID = ObjectID> {
	_id?: ObjectID;
	title: string;
	description?: string;
	visibility: boolean;
	lessons: T[];
}

export interface ICourseDoc<
	S extends LessonOrObjectID = ObjectID,
	T extends StudentOrObjectID = ObjectID,
	U extends WorkerOrObjectID = ObjectID
> extends ICourse<S, T, U>,
		Document {}

export interface ISectionDoc<T extends LessonOrObjectID = ObjectID>
	extends ISection<T> {}
