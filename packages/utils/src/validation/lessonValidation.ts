/**
 * Title validation
 *
 * - Number of characters must be between 1 to 80
 * @param value Value to validate
 * @returns Is valid
 */
export const titleValidation = (value: string): boolean => {
	const title = value.trim();
	if (title.length < 1 || title.length > 80) return false;
	return true;
};

/**
 * Description validation
 *
 * - Number of characters must be between 1 to 140
 * @param value Value to validate
 * @returns Is valid
 */
export const descriptionValidation = (value: string): boolean => {
	const description = value.trim();
	if (description.length < 1 || description.length > 140) return false;
	return true;
};

/**
 * Url link validation
 *
 * - Only HTTP URL
 * @param value Value to validate
 * @returns Is valid
 */
export const linkValidation = (value: string): boolean => {
	let url: URL;

	try {
		url = new URL(value);
	} catch (_) {
		return false;
	}

	return url.protocol === 'http:' || url.protocol === 'https:';
};
