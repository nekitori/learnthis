import { ObjectID } from '@Common/types/object-id.type';

export type ObjectIdTokenPayload = {
	id: ObjectID;
};
