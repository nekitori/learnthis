import { CourseModels } from '@Courses/enums/course-models.enum';
import { ILessonDoc } from '@Courses/interfaces/lesson-document.interface';
import { Field, Int, InterfaceType, ObjectType } from '@nestjs/graphql';
import { LessonResources } from './lesson-resources.gqltype';

/**
 * Graphql interface type: Lesson of a course with all its data (for admin purposes).
 */
@InterfaceType({
	resolveType(lesson: ILessonDoc) {
		if (lesson.kind === CourseModels.LESSON_VIDEO) {
			return VideoLesson;
		} else if (lesson.kind === CourseModels.LESSON_INFO) {
			return InfoLesson;
		}
	},
})
export abstract class Lesson {
	_id: string;
	title: string;
	description: string;
	visibility: boolean;
	@Field(type => Int)
	section: number;
	resources: LessonResources;
}

/**
 * Graphql type: Video lesson of a course with all its data (for admin purposes).
 */
@ObjectType({ implements: [Lesson] })
export class VideoLesson extends Lesson {
	videoSrc?: string;
	@Field(type => Int)
	duration: number;
}

/**
 * Graphql type: Info lesson of a course with all its data (for admin purposes).
 */
@ObjectType({ implements: [Lesson] })
export class InfoLesson extends Lesson {
	content?: string;
}
