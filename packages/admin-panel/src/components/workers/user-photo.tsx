import { ComponentProps, FC, SyntheticEvent } from 'react';

interface UserPhotoProps extends ComponentProps<'img'> {
	photo?: string;
	name: string;
	surname: string;
}

const UserPhoto: FC<UserPhotoProps> = ({ photo, name, surname, ...props }) => (
	<img
		{...props}
		src={
			photo ||
			`https://avatars.dicebear.com/api/initials/${name}-${surname}.svg`
		}
		onError={(event: SyntheticEvent<HTMLImageElement, Event>) => {
			event.currentTarget.src = `https://avatars.dicebear.com/api/initials/${name}-${surname}.svg`;
		}}
	/>
);

export default UserPhoto;
