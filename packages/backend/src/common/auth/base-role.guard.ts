import { CommonErrors } from '@Common/enums/common-errors.enum';
import { ForbiddenException } from '@nestjs/common';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';

export abstract class BaseRoleGuard {
	protected isUserAllowed(
		userRoles: WorkerRoles[],
		allowedRoles: WorkerRoles[]
	) {
		for (const userRole of userRoles) {
			for (const allowedRole of allowedRoles) {
				if (userRole === allowedRole) return true;
			}
		}

		throw new ForbiddenException(CommonErrors.UNAUTHORIZED);
	}
}
