import { CommonErrors } from '@Common/enums/common-errors.enum';
import { trimAllStrings } from '@Common/utils/clean-object';
import { LessonResourcesDto } from '@Courses/dto/lesson-resources.dto';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { LessonValidation } from 'learnthis-utils';

@Injectable()
export class LessonResourcesPipe implements PipeTransform {
	transform(value: LessonResourcesDto): LessonResourcesDto {
		const { links, files } = value;

		const errors = [];

		for (const link of links) {
			if (!LessonValidation.linkValidation(link))
				errors.push(CommonErrors.FORMAT_URL);
		}

		for (const file of files) {
			if (!LessonValidation.linkValidation(file))
				errors.push(CommonErrors.FORMAT_URL);
		}

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		trimAllStrings(value);

		return value;
	}
}
