import { ComponentProps, FC } from 'react';

export const CheckIcon: FC<ComponentProps<'svg'>> = props => (
	<svg {...props} fill='none' stroke='currentColor' viewBox='0 0 24 24'>
		<path
			strokeLinecap='round'
			strokeLinejoin='round'
			strokeWidth={2}
			d='M5 13l4 4L19 7'
		/>
	</svg>
);

export const CheckCircleIcon: FC<ComponentProps<'svg'>> = props => (
	<svg {...props} fill='none' stroke='currentColor' viewBox='0 0 24 24'>
		<path
			strokeLinecap='round'
			strokeLinejoin='round'
			strokeWidth={2}
			d='M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z'
		/>
	</svg>
);
