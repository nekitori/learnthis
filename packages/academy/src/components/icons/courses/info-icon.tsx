import { ComponentProps, FC } from 'react';

export const InfoIcon: FC<ComponentProps<'svg'>> = props => (
	<svg {...props} viewBox='0 0 512 512'>
		<path d='M459.36 100.64l-96-96A16 16 0 0 0 352 0H96C69.5 0 48 21.5 48 48v416c0 26.5 21.5 48 48 48h320c26.5 0 48-21.5 48-48V112a16 16 0 0 0-4.64-11.36zM432 464c0 8.837-7.163 16-16 16H96c-8.837 0-16-7.163-16-16V48c0-8.837 7.163-16 16-16h240v64c0 17.673 14.327 32 32 32h64v336zM112 224h288v32H112zm0 80h288v32H112zm0 80h288v32H112z' />
	</svg>
);
