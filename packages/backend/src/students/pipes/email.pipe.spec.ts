import { BadRequestException } from '@nestjs/common';
import { EmailPipe } from './email.pipe';

describe('EmailPipe', () => {
	let emailPipe: EmailPipe;

	beforeAll(() => (emailPipe = new EmailPipe()));

	it('Email correcto - Prueba 1', () => {
		const email = 'algo@algo.com';
		expect(emailPipe.transform(email)).toBe(email);
	});

	it('Email correcto - Prueba 2', () => {
		const email = 'algo@algo.com.es';
		expect(emailPipe.transform(email)).toBe(email);
	});

	it('Email incorrecto - Prueba 1', () => {
		expect(() => emailPipe.transform('algo@algo.com.es.')).toThrow(
			BadRequestException
		);
	});

	it('Email incorrecto - Prueba 2', () => {
		expect(() => emailPipe.transform('algoalgo.com.es')).toThrow(
			BadRequestException
		);
	});

	it('Email incorrecto - Prueba 3', () => {
		expect(() => emailPipe.transform('1@3.')).toThrow(BadRequestException);
	});
});
