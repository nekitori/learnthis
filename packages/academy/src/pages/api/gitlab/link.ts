import nextConnect from 'next-connect';
import passport from 'passport';

import { LoginStrategies } from '@Enums/config/login-strategies.enum';

import { gLabStrategy } from '@Lib/login/gitlab';

const signin = nextConnect();

passport.use(gLabStrategy);

signin.use(passport.initialize());

signin.get((req: any, res, next) => {
	const state = Buffer.from(JSON.stringify({ link: true })).toString('base64');
	const authenticator = passport.authenticate(LoginStrategies.GITLAB, {
		session: false,
		state,
	});
	authenticator(req, res, next);
});

export default signin;
