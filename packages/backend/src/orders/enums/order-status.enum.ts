import { registerEnumType } from '@nestjs/graphql';

export enum OrderStatus {
	UNPAID = 'Unpaid',
	PAID = 'Paid',
	PROCESSED = 'Processed',
	REFUNDED = 'Refunded',
}

registerEnumType(OrderStatus, { name: 'OrderStatus' });
