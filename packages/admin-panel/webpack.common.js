const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const TS_CONFIG_DIR = __dirname;

const getAlias = () => {
	const { paths, baseUrl } = require('./tsconfig.json').compilerOptions;

	const alias = {};

	for (const tsPath of Object.entries(paths)) {
		const key = tsPath[0].replace('/*', '');
		const value = tsPath[1][0].replace('/*', '');

		alias[key] = path.join(TS_CONFIG_DIR, baseUrl, value);
	}

	return alias;
};

module.exports = {
	target: 'web',
	entry: path.join(__dirname, 'src/index.tsx'),
	resolve: {
		extensions: ['.ts', '.tsx', '.js'],
		alias: getAlias(),
		fallback: {
			buffer: require.resolve('buffer/'),
			util: require.resolve('util/'),
			stream: require.resolve('stream-browserify'),
			crypto: require.resolve('crypto-browserify'),
			process: require.resolve('process'),
		},
	},
	module: {
		rules: [
			{
				test: /\.(ts|tsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.css$/i,
				include: path.resolve(__dirname, 'src'),
				exclude: /node_modules/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader'],
			},
		],
	},
	output: {
		filename: '[name].bundle.js',
		chunkFilename: '[id].chunk.js',
		publicPath: '/',
		path: path.resolve(__dirname, 'build'),
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.join(__dirname, 'public/index.html'),
		}),
		new ForkTsCheckerWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: 'styles.css',
		}),
		new CopyPlugin({
			patterns: [{ from: './public/static/*', to: 'static', flatten: true }],
		}),
		new webpack.DefinePlugin({
			'process.env.NODE_DEBUG': JSON.stringify(process.env.NODE_DEBUG),
			'process.platform': JSON.stringify(process.platform),
			'process.browser': JSON.stringify(process.browser),
			'process.cwd': JSON.stringify(process.cwd),
			'process.version': JSON.stringify(process.version),
		}),
	],
};
