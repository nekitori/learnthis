import { ApolloError, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import Input from '@Components/generic/form/input';
import TextArea from '@Components/generic/form/textarea';
import Loader from '@Components/other/loader';
import { AlertMessages, FormMessages } from '@Enums/config/constants';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { ApolloMutation } from '@Interfaces/apollo/apollo-query.types';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { ICourse } from '@Interfaces/course.interface';
import { Form, Formik, FormikConfig } from 'formik';
import { GraphqlCourse } from 'learnthis-utils';
import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
	number as YupNumber,
	object as YupObject,
	ref as YupRef,
	string as YupString,
} from 'yup';

//#region TS

enum CourseModifyFields {
	TITLE = 'course_title',
	DESCRIPTION = 'course_description',
	PRICE = 'course_price',
	COMPAREATPRICE = 'course_compare_at_price',
}

interface ICourseModifyInput {
	course_title: string;
	course_description: string;
	course_price: number;
	course_compare_at_price?: number;
}

type CourseFormDataProps = {
	course?: ICourse;
	refetch: ApolloQueryRefetch;
};

//#endregion

const CourseFormData: FC<CourseFormDataProps> = ({ course, refetch }) => {
	const { modifyCourseMutation, loading } = getModifyCourseMutation(
		refetch,
		course
	);

	const form = getForm(modifyCourseMutation, course);

	if (!course) return <Loader />;
	return (
		<Formik {...form}>
			<Form className='w-full mx-auto flex-c-s flex-wrap'>
				<h3 className='mb-2 text-white-dark dark:text-white text-2xl font-semibold'>
					Modificar curso
				</h3>
				<Input
					name={CourseModifyFields.TITLE}
					className='w-full p-0_5'
					type='text'
					label='Título'
				/>
				<TextArea
					name={CourseModifyFields.DESCRIPTION}
					className='w-full p-0_5'
					label='Descripción'
					rows={4}
				/>
				<Input
					name={CourseModifyFields.PRICE}
					className='w-6/12 p-0_5'
					type='number'
					min={0}
					step='any'
					label='Precio'
				/>
				<Input
					name={CourseModifyFields.COMPAREATPRICE}
					className='w-6/12 p-0_5'
					type='number'
					min={0}
					step='any'
					label='Precio sin oferta'
				/>
				<Button
					loading={loading}
					className='mt-2 p-0_75 w-10'
					type='submit'
					kind='primary'>
					Modificar
				</Button>
			</Form>
		</Formik>
	);
};

/**
 * Gets the graphql mutation to modify course's data
 */
const getModifyCourseMutation = (
	refetch: ApolloQueryRefetch,
	course?: ICourse
) => {
	const history = useHistory();

	const [modifyCourseMutation, { loading }] = useMutation(
		GraphqlCourse.course_admin_modify,
		{
			onCompleted: () => {
				toast.success(AlertMessages.COURSE_MODIFY_SUCCESS);
				refetch();
				history.push(
					MainPaths.COURSE.replace(MainParams.COURSE_ID, course?._id || '')
				);
			},
			onError: (error: ApolloError) => {
				toast.error(error.message || AlertMessages.SERVER_ERROR);
			},
		}
	);

	return { modifyCourseMutation, loading };
};

/**
 * Gets the formik data to build the form.
 * @param modifyCourseMutation Graphql mutation
 * @param course Course to modify
 */
const getForm = (
	modifyCourseMutation: ApolloMutation,
	course?: ICourse
): FormikConfig<ICourseModifyInput> => {
	const initialValues: ICourseModifyInput = {
		course_title: course?.title || '',
		course_description: course?.description || '',
		course_price: course?.price || 0,
		course_compare_at_price: course?.compareAtPrice || undefined,
	};

	const validationSchema = YupObject().shape({
		[CourseModifyFields.TITLE]: YupString().required(
			FormMessages.TITLE_REQUIRED
		),
		[CourseModifyFields.DESCRIPTION]: YupString().required(
			FormMessages.DESCRIPTION_REQUIRED
		),
		[CourseModifyFields.PRICE]: YupNumber()
			.positive(FormMessages.MIN_PRICE)
			.required(FormMessages.PRICE_REQUIRED),
		[CourseModifyFields.COMPAREATPRICE]: YupNumber()
			.positive(FormMessages.MIN_PRICE)
			.moreThan(
				YupRef(CourseModifyFields.PRICE),
				FormMessages.COMPAREATPRICE_CHECK
			)
			.nullable(),
	});

	const onSubmit = (values: ICourseModifyInput) => {
		modifyCourseMutation({
			variables: {
				courseId: course?._id,
				input: {
					title: values[CourseModifyFields.TITLE],
					description: values[CourseModifyFields.DESCRIPTION],
					price: values[CourseModifyFields.PRICE],
					compareAtPrice: values[CourseModifyFields.COMPAREATPRICE],
				},
			},
		});
	};

	return {
		initialValues,
		onSubmit,
		validationSchema,
	};
};

export default CourseFormData;
