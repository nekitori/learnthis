import { MainPaths } from '@Enums/paths/main-paths.enum';
import { withSuspense } from '@Lib/hoc/withSuspense';
import { FC, lazy, ReactElement } from 'react';
import { Route, RouteProps, Switch } from 'react-router-dom';

//#region Lazy imports

// Dashboard
const Dashboard = lazy(() => import('@Pages/page-dashboard'));
// Workers
const Workers = lazy(() => import('@Pages/workers/page-workers'));
const WorkerCreate = lazy(
	() => import('@Pages/workers/worker/page-worker-create')
);
const WorkerRouter = lazy(() => import('@Pages/workers/worker/router-worker'));
// Courses
const CourseCreate = lazy(
	() => import('@Pages/courses/course/page-course-create')
);
const CoursesPage = lazy(() => import('@Pages/courses/page-courses'));
const CourseRouter = lazy(() => import('@Pages/courses/course/router-course'));
const SectionRouter = lazy(
	() => import('@Pages/courses/section/router-section')
);
const SectionCreate = lazy(
	() => import('@Pages/courses/section/page-section-create')
);
// Clients
const ClientsPage = lazy(() => import('@Pages/page-clients'));
// 404
const NotFound = lazy(() => import('@Pages/page-not-found'));

//#endregion

/**
 * Component to handle page routes in app.
 */
const AppRoutes: FC = () => {
	const routes = mapRoutes();
	return <Switch>{routes}</Switch>;
};

/**
 * Maps all page paths to their corresponding react-router's Route component
 */
const mapRoutes = (): ReactElement[] => {
	const routes: RouteProps[] = [
		// Dashboard
		{
			path: MainPaths.DASHBOARD,
			exact: true,
			component: withSuspense(Dashboard),
		},
		// Workers
		{
			path: MainPaths.WORKERS_LIST,
			exact: true,
			component: withSuspense(Workers),
		},
		{
			path: MainPaths.WORKER_CREATE,
			exact: true,
			component: withSuspense(WorkerCreate),
		},
		{
			path: [
				MainPaths.WORKERS,
				MainPaths.WORKER_DATA,
				MainPaths.WORKER_ADVANCED,
				MainPaths.WORKER_COURSES,
			],
			exact: true,
			component: withSuspense(WorkerRouter),
		},
		// Courses
		{
			path: MainPaths.COURSES_LIST,
			exact: true,
			component: withSuspense(CoursesPage),
		},
		{
			path: MainPaths.COURSE_CREATE,
			exact: true,
			component: withSuspense(CourseCreate),
		},
		{
			path: [
				MainPaths.COURSE,
				MainPaths.COURSE_DATA,
				MainPaths.COURSE_SECTIONS,
				MainPaths.COURSE_TUTORS,
				MainPaths.COURSE_ADVANCED,
			],
			exact: true,
			component: withSuspense(CourseRouter),
		},
		{
			path: MainPaths.COURSE_CREATE_SECTION,
			exact: true,
			component: withSuspense(SectionCreate),
		},
		{
			path: [
				MainPaths.SECTION,
				MainPaths.SECTION_DATA,
				MainPaths.SECTION_LESSONS,
				MainPaths.SECTION_ADVANCED,
			],
			exact: true,
			component: withSuspense(SectionRouter),
		},
		// Clients
		{
			path: MainPaths.CLIENTS_LIST,
			exact: true,
			component: withSuspense(ClientsPage),
		},
		// 404
		{
			path: MainPaths.NOT_FOUND,
			exact: false,
			component: withSuspense(NotFound),
		},
	];

	return routes.map((i, index) => <Route key={index} {...i} />);
};

export default AppRoutes;
