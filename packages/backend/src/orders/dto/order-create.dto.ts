import { Field, Float, InputType } from '@nestjs/graphql';

@InputType()
export class OrderCreateDto {
	@Field()
	studentId: string;
	@Field(type => [String])
	courseIds: string[];
	@Field(type => Float)
	tax: number;
}
