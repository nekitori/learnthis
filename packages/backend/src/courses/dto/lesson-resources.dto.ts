import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class LessonResourcesDto {
	@Field(type => [String], { nullable: true })
	links: string[];
	@Field(type => [String], { nullable: true })
	files: string[];
}
