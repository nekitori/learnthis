import { StudentErrors } from '@Students/enums/student-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { ChangePasswordDto } from '@Students/dto/change-password.dto';
import { FormValidation } from 'learnthis-utils';

@Injectable()
export class ChangePasswordPipe implements PipeTransform {
	transform(value: ChangePasswordDto) {
		if (value.oldPassword === value.newPassword)
			throw new BadRequestException(StudentErrors.FORMAT_SAME_PASSWORD);

		if (
			value.oldPassword &&
			!FormValidation.passwordValidation(value.oldPassword)
		)
			throw new BadRequestException(StudentErrors.FORMAT_PASSWORD);
		if (!FormValidation.passwordValidation(value.newPassword))
			throw new BadRequestException(StudentErrors.FORMAT_PASSWORD);

		return value;
	}
}
