import { trimAllStrings } from '@Common/utils/clean-object';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { WorkerRegisterDto } from '@Workers/dto/worker-register.dto';
import { WorkerErrors } from '@Workers/enums/worker-errors.enum';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import { FormValidation } from 'learnthis-utils';

@Injectable()
export class WorkerRegisterPipe implements PipeTransform {
	transform(value: WorkerRegisterDto): WorkerRegisterDto {
		const { email, name, surname, password, roles } = value;

		const errors = [];

		if (!FormValidation.emailValidation(email))
			errors.push(WorkerErrors.FORMAT_EMAIL);

		if (!FormValidation.nameValidation(name))
			errors.push(WorkerErrors.FORMAT_NAME);

		if (!FormValidation.nameValidation(surname))
			errors.push(WorkerErrors.FORMAT_SURNAME);

		if (!FormValidation.passwordValidation(password))
			errors.push(WorkerErrors.FORMAT_PASSWORD);

		if (roles.includes(WorkerRoles.ADMIN) && roles.length > 1)
			errors.push(WorkerErrors.ADMIN_ONLY_ALLOW_ONE_ROLE);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		trimAllStrings(value);

		return {
			...value,
			email: value.email.toLowerCase(),
		};
	}
}
