import SectionEditForm from '@Components/courses/section/section-edit-form';
import Loader from '@Components/other/loader';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { withAuth } from '@Lib/hoc/withAuth';
import { SectionWrapperProps } from '@Types/props/with-section-props.type';
import { FC } from 'react';

const PageSectionData: FC<SectionWrapperProps> = ({
	course,
	section,
	sectionId,
	refetch,
}) => {
	return section && course ? (
		<SectionEditForm
			section={section}
			sectionId={sectionId}
			courseId={course._id}
			refetch={refetch}
		/>
	) : (
		<Loader />
	);
};

export default withAuth(PageSectionData, [WorkerRoles.ADMIN]);
