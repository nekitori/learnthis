import { InfoIcon } from '@Components/icons/lessons/info-icon';
import { VideoIcon } from '@Components/icons/lessons/video-icon';
import { LessonTypes } from '@Enums/course/lesson-types.enum';
import { ILesson } from '@Interfaces/lesson.interface';
import { formatDuration } from '@Lib/utils/date.utils';
import { forwardRef } from 'react';
import { IItemProps } from 'react-movable';
import CoursePublishedSpan from '../course-published-span';

interface SectionRowProps extends IItemProps {
	lesson: ILesson;
}

const SectionRowDrag = forwardRef<HTMLDivElement, SectionRowProps>(
	({ lesson, ...props }, ref) => (
		<div
			className='flex flex-wrap w-full p-0_75 border-b border-gray-light dark:border-gray-dark'
			ref={ref}
			{...props}>
			<div className='w-1/12 xssm:w-1/6 text-center text-primary'>
				{lesson.__typename === LessonTypes.VIDEO ? (
					<VideoIcon className='h-1_25 w-1_25' />
				) : (
					<InfoIcon className='h-1_25 w-1_25' />
				)}
			</div>
			<div className='w-8/12 xssm:w-5/6 xssm:mb-0_75'>
				<span className='font-semibold text-sm text-black dark:text-white'>
					{lesson.title}
				</span>
			</div>
			<div className='w-2/12 xssm:w-1/2 xssm:order-4 xssm:text-right'>
				<span className='font-semibold text-sm text-black dark:text-white'>
					<CoursePublishedSpan visibility={lesson.visibility} />
				</span>
			</div>
			<div className='w-1/12 xssm:w-1/2 xssm:order-3 text-right xssm:text-left'>
				<span className='text-sm text-black dark:text-white'>
					{lesson.duration ? formatDuration(lesson.duration) : '-'}
				</span>
			</div>
		</div>
	)
);

export default SectionRowDrag;
