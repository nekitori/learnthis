import { Provider } from '@nestjs/common';
import { WorkerResolver } from '@Workers/resolvers/worker.resolver';
import { WorkerService } from '@Workers/services/worker.service';

export const workerProviders: Provider[] = [WorkerResolver, WorkerService];
