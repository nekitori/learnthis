import CourseSummary from '@Components/courses/course-summary';
import CoursesNav from '@Components/courses/courses-nav';
import BlankCard from '@Components/generic/cards/blank-card';
import Loader from '@Components/other/loader';
import { NavTypes } from '@Enums/course/nav.types.enum';
import { UrlParams } from '@Enums/paths/main-paths.enum';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { ICourse } from '@Interfaces/course.interface';
import { AuthContext } from '@Lib/context/auth.context';
import { FC, useContext } from 'react';
import { useParams } from 'react-router-dom';
import SectionLayout from './section.layout';

type CourseLayoutProps = {
	title: string;
	course: ICourse | null;
	type: NavTypes;
};

const CourseLayout: FC<CourseLayoutProps> = ({
	title,
	course,
	type,
	children,
}) => {
	const { authState } = useContext(AuthContext);
	const params = useParams<UrlParams>();

	return (
		<SectionLayout title={title}>
			{authState.worker?.roles.includes(WorkerRoles.ADMIN) && (
				<CoursesNav type={type} {...params} />
			)}
			<BlankCard className='w-2/5 xssm:w-full xssm:order-3'>
				{course ? <CourseSummary course={course} /> : <Loader />}
			</BlankCard>
			<BlankCard className='w-3/5 xssm:w-full xssm:order-2'>
				{children}
			</BlankCard>
		</SectionLayout>
	);
};

export default CourseLayout;
