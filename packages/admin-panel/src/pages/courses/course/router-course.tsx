import { ApolloError, useQuery } from '@apollo/client';
import CourseLayout from '@Components/layout/course.layout';
import { NavTypes } from '@Enums/course/nav.types.enum';
import { MainPaths, UrlParams } from '@Enums/paths/main-paths.enum';
import { ICourse } from '@Interfaces/course.interface';
import { withSuspense } from '@Lib/hoc/withSuspense';
import { CourseWrapperProps } from '@Types/props/with-course-props.type';
import { GraphqlCourse } from 'learnthis-utils';
import { FC, lazy } from 'react';
import { Redirect, Route, Switch, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

//#region Lazy components

const CourseData = lazy(() => import('@Pages/courses/course/page-course-data'));
const CourseSections = lazy(
	() => import('@Pages/courses/course/page-course-sections')
);
const CourseTutors = lazy(
	() => import('@Pages/courses/course/page-course-tutors')
);
const CourseMain = lazy(() => import('@Pages/courses/course/page-course-main'));
const CourseAdvanced = lazy(
	() => import('@Pages/courses/course/page-course-advanced')
);
//#endregion

const RouterCourse: FC = () => {
	const { data, loading, error, refetch } = getCourse();

	if (error) return <Redirect to={MainPaths.NOT_FOUND} />;

	const course: ICourse =
		!loading && data ? data.course_admin_find_by_id : undefined;

	const props: CourseWrapperProps = {
		course,
		refetch,
	};

	const routes = [
		{
			path: MainPaths.COURSE,
			exact: true,
			component: withSuspense(CourseMain, props),
		},
		{
			path: MainPaths.COURSE_DATA,
			exact: true,
			component: withSuspense(CourseData, props),
		},
		{
			path: MainPaths.COURSE_SECTIONS,
			exact: true,
			component: withSuspense(CourseSections, props),
		},
		{
			path: MainPaths.COURSE_TUTORS,
			exact: true,
			component: withSuspense(CourseTutors, props),
		},
		{
			path: MainPaths.COURSE_ADVANCED,
			exact: true,
			component: withSuspense(CourseAdvanced, props),
		},
	];

	return (
		<CourseLayout title='Cursos' type={NavTypes.COURSE} course={course}>
			<Switch>
				{routes.map((route, i) => (
					<Route
						key={i}
						path={route.path}
						exact={route.exact}
						component={route.component}
					/>
				))}
			</Switch>
		</CourseLayout>
	);
};

const getCourse = () => {
	const { courseId } = useParams<UrlParams>();

	const { data, loading, error, refetch } = useQuery(
		GraphqlCourse.course_admin_find_by_id,
		{
			variables: {
				courseId,
			},
			onError: (error: ApolloError) => {
				toast.error(error.message);
			},
		}
	);

	return { data, loading, error, refetch };
};

export default RouterCourse;
