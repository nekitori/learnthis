import { FC } from 'react';

type ActiveSpanProps = {
	active: boolean;
};

const ActiveSpan: FC<ActiveSpanProps> = ({ active }) => {
	const className = `px-0_5 py-0_25 text-xs font-semibold rounded-full ${
		active ? 'text-white bg-green' : 'text-white bg-red'
	}`;

	const text = active ? 'ACTIVE' : 'INACTIVE';

	return <span className={className}>{text}</span>;
};

export default ActiveSpan;
