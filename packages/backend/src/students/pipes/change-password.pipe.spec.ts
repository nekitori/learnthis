import { BadRequestException } from '@nestjs/common';
import { ChangePasswordDto } from '@Students/dto/change-password.dto';
import { ChangePasswordPipe } from './change-password.pipe';

describe('ChangePasswordPipe', () => {
	let changePasswordPipe: ChangePasswordPipe;

	beforeAll(() => (changePasswordPipe = new ChangePasswordPipe()));

	it('Contraseña correcta', () => {
		const passwords: ChangePasswordDto = {
			oldPassword: 'asd123ASD.',
			newPassword: 'ASDasd123.',
		};
		expect(changePasswordPipe.transform(passwords)).toBe(passwords);
	});

	it('Contraseña correcta sin anterior', () => {
		const passwords: ChangePasswordDto = {
			oldPassword: '',
			newPassword: 'ASDasd123.',
		};
		expect(changePasswordPipe.transform(passwords)).toBe(passwords);
	});

	it('Contraseña incorrecto - Nueva igual a anterior', () => {
		const passwords: ChangePasswordDto = {
			oldPassword: 'asd123ASD.',
			newPassword: 'asd123ASD.',
		};
		expect(() => changePasswordPipe.transform(passwords)).toThrow(
			BadRequestException
		);
	});

	it('Contraseña incorrecto - Anterior incorrecta', () => {
		const passwords: ChangePasswordDto = {
			oldPassword: 'asd12',
			newPassword: 'asd123ASD.',
		};
		expect(() => changePasswordPipe.transform(passwords)).toThrow(
			BadRequestException
		);
	});

	it('Contraseña incorrecto - Nueva incorrecta', () => {
		const passwords: ChangePasswordDto = {
			oldPassword: 'asd123ASD.',
			newPassword: 'asd12',
		};
		expect(() => changePasswordPipe.transform(passwords)).toThrow(
			BadRequestException
		);
	});
});
