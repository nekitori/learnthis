import Paginated from '@Common/gql-types/paginate-filter.gqltypes';
import { ObjectType } from '@nestjs/graphql';
import { Worker } from './worker.gqltype';

/**
 * Graphql type: Graphql type: Paginated list of workers
 */
@ObjectType()
export class WorkerPaginated extends Paginated<Worker>(Worker) {}
