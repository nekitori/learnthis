import gql from 'graphql-tag';

//#region Order

export const order_create = gql`
	mutation order_create($coursesIds: [String!]!) {
		order_create(coursesIds: $coursesIds)
	}
`;

//#endregion
