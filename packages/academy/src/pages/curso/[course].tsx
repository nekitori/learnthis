import { useQuery } from '@apollo/client';
import CourseHead from '@Components/courses/course/course-head';
import CourseSummary from '@Components/courses/course/course-summary';
import Loader from '@Components/loader';
import { PublicFullCourse } from '@Interfaces/course/course.interface';
import { GSSProps } from '@Interfaces/props/gss-props.interface';
import { createApolloClient } from '@Lib/apollo/apollo-client';
import { getJwtFromCookie } from '@Lib/login/jwt-cookie.utils';
import { isRequestSSR, loadAuthProps } from '@Lib/utils/ssr.utils';
import { getThemeFromCookie } from '@Lib/utils/theme.utils';
import { decode } from 'jsonwebtoken';
import { GraphqlCourse } from 'learnthis-utils';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { useRouter } from 'next/router';
import { FC } from 'react';

export type CourseProps = {
	course: PublicFullCourse;
};

export const Course: FC<CourseProps> = () => {
	const router = useRouter();
	const { data, loading } = useQuery(GraphqlCourse.course_public_find_by_url, {
		variables: {
			courseUrl: router.query.course,
		},
	});

	const course = data ? data.course_public_find_by_url : null;

	return loading ? (
		<Loader />
	) : (
		<>
			<CourseHead
				id={course._id}
				image={course.image}
				background={course.background}
				title={course.title}
				description={course.description}
			/>
			<CourseSummary sections={course.sections} />
		</>
	);
};

export const getServerSideProps: GetServerSideProps = async (
	ctx: GetServerSidePropsContext
) => {
	const props: GSSProps = { lostAuth: false };
	const isSSR = isRequestSSR(ctx.req.url);

	const jwt = getJwtFromCookie(ctx.req.headers.cookie);

	const apolloClient = createApolloClient();

	if (jwt) {
		if (isSSR) {
			const authProps = await loadAuthProps(ctx.res, jwt, apolloClient);

			if (authProps) props.authProps = authProps;
		} else if (!decode(jwt)) props.lostAuth = true;
	}

	const query = GraphqlCourse.course_public_find_by_url;

	await apolloClient.query({
		query,
		variables: { courseUrl: ctx.params?.course },
	});

	props.componentProps = {
		theme: getThemeFromCookie(ctx.req.headers.cookie),
	};

	props.apolloCache = apolloClient.cache.extract();

	return {
		props,
	};
};

export default Course;
