# NOTIFICACIONES DEL WEBHOOK DE MUX DURANTE LA SUBIDA

## ACABA DE SUBIR AL BACKEND
type: 'video.upload.created',
type: 'upload',
## INICIA LA SUBIDA A MUX
type: 'video.asset.created',
type: 'asset',
type: 'video.upload.asset_created',
type: 'upload',
## COMIENZA A PROCESAR EL MP4
type: 'video.asset.ready',
type: 'asset',