import { Field, InputType } from '@nestjs/graphql';

/**
 * Grpahql input type: Worker login
 */
@InputType()
export class WorkerLoginDto {
	/** Worker's email */
	@Field()
	email: string;
	/** Worker's password */
	@Field()
	password: string;
}
