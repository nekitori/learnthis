import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { IWorker } from '@Interfaces/worker.interface';

export interface WorkerProfileProps {
	worker: IWorker;
	refetch: ApolloQueryRefetch;
}
