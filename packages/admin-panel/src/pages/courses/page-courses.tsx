import SectionLayout from '@Components/layout/section.layout';
import CoursesTable from '@Components/courses/table/courses-table';
import { withAuth } from '@Lib/hoc/withAuth';
import { FC, useState } from 'react';
import { useQuery } from '@apollo/client';
import { GraphqlCourse } from 'learnthis-utils';
import { Link, useLocation } from 'react-router-dom';
import TitleCard from '@Components/generic/cards/title-card';
import BlankCard from '@Components/generic/cards/blank-card';
import Loader from '@Components/other/loader';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { AddIcon } from '@Components/icons/generic/add-icon';
import LinkButton from '@Components/generic/form/button/link-button';

/**
 * Page component for course management.
 *
 * Authenticated: YES
 * Allowed roles: Admin and Teacher
 */
const PageCourses: FC = () => {
	const { data, loading, page, setPage } = getCourses();

	const courses = data && !loading ? data.course_admin_find.data : null;
	const totalPages =
		data && !loading ? Math.ceil(data.course_admin_find.total / 10) : null;

	return (
		<SectionLayout title='Cursos'>
			<TitleCard
				className='w-3/4 xssm:w-full'
				title='Cursos'
				description='Listado de todos los cursos'>
				<CoursesTable
					courses={courses}
					totalPages={totalPages}
					page={page}
					setPage={setPage}
				/>
			</TitleCard>
			<BlankCard className='w-1/4 xssm:w-full'>
				<div className='flex flex-wrap px-1_5 pb-2'>
					<div className='w-1/2 flexcol-s-c'>
						<div className='flex-c-c'>
							<div className='h-1 w-1 bg-pink rounded-md mr-1'></div>
							<span className='font-semibold text-gray'>Cursos</span>
						</div>
						<span className='text-4xl font-bold px-2 text-black dark:text-white'>
							{data ? data.course_admin_find.total : <Loader />}
						</span>
					</div>
				</div>
				<div className='flex-c-c'>
					<LinkButton kind='primary' to={MainPaths.COURSE_CREATE}>
						Crear curso
					</LinkButton>
				</div>
			</BlankCard>
		</SectionLayout>
	);
};

const getCourses = () => {
	const location = useLocation<string>();

	const [page, setPage] = useState(
		parseInt(new URLSearchParams(location.search).get('page') || '0')
	);
	const { data, loading } = useQuery(GraphqlCourse.course_admin_find, {
		fetchPolicy: 'cache-and-network',
		variables: {
			paginate: {
				offset: page * 10,
				limit: 10,
			},
		},
	});

	return { data, loading, page, setPage };
};

export default withAuth(PageCourses);
