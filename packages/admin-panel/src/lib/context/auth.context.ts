import { IWorker } from '@Interfaces/worker.interface';
import { createContext } from 'react';

export interface IAuthState {
	jwt: string | null;
	worker?: IWorker | null;
}

interface IAuthContext {
	login: (jwt: string, worker: IWorker) => void;
	logout: () => void;
	updateUser: (w: Partial<IWorker>) => void;
	authState: IAuthState;
}

export const AuthContext = createContext<IAuthContext>({} as any);
