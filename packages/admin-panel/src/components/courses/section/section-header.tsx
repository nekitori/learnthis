import { FC } from 'react';
import CoursePublishedSpan from '../course-published-span';

type SectionHeaderProps = {
	section: number | null;
	title: string | undefined;
	visibility: boolean | undefined;
};

const SectionHeader: FC<SectionHeaderProps> = ({
	section,
	title,
	visibility,
}) => {
	return (
		<h2 className='mb-1 flex-s-c'>
			<div className='flex-c-c rounded-full bg-primary h-2_5 w-2_5 mr-1'>
				<span className='text-white font-semibold text-1xl'>
					{typeof section === 'number' ? section + 1 : ''}
				</span>
			</div>
			<div className='flexcol-c-s'>
				<span className='font-semibold text-lg text-black dark:text-white mb-0_25'>
					{title}
				</span>
				<CoursePublishedSpan visibility={visibility} />
			</div>
		</h2>
	);
};

export default SectionHeader;
