import { FC, useContext } from 'react';
import Link from 'next/link';

import { RestEndPoints } from '@Enums/paths/rest-endpoints.enum';

import { GithubIcon } from '@Icons/social/github-icon';
import { GitlabIcon } from '@Icons/social/gitlab-icon';
import { GoogleIcon } from '@Icons/social/google-icon';
import { useRouter } from 'next/router';
import { generateQueryParams } from '@Lib/utils/url.utils';
import {
	AccountSocial,
	AccountSocialType,
} from '@Enums/config/account-social.enum';
import { GraphqlStudent } from 'learnthis-utils';
import { ApolloQueryResult, useMutation } from '@apollo/client';
import { toast } from 'react-toastify';
import { AlertMessages } from '@Enums/config/constants';
import { LoginStrategies } from '@Enums/config/login-strategies.enum';
import { StudentProfileOtherInfo } from '@Interfaces/student/student.interface';
import { ProfileContext } from '@Lib/context/profile.context';

export interface SocialLoginLinkProps {
	kind: AccountSocialType;
	className?: string;
}

const SocialLoginLink: FC<SocialLoginLinkProps> = ({
	children,
	kind,
	className,
	...props
}) => {
	const router = useRouter();
	const { profile, refetch } = useContext(ProfileContext);

	const listOfLinks = getListOfLinks(profile, refetch);

	const linkProps = listOfLinks[kind];

	const Icon = linkProps.icon;

	const classNames = [
		'flex-s-c px-1 py-0_75 shadow-sm rounded-lg',
		linkProps.className,
	];

	if (className) classNames.push(className);

	className = classNames.join(' ');

	if (linkProps.href) {
		let url: string = linkProps.href;

		if (Object.keys(router.query).length > 0) {
			let queryString;

			if (router.query)
				queryString = generateQueryParams(
					router.query as Record<string, string>
				);

			url = queryString ? `${url}?${queryString}` : url;
		}

		return (
			<Link href={url}>
				<a className={className} {...props}>
					<Icon className='h-1_5 w-1_5 mr-1 ' />
					<span>{linkProps.text}</span>
				</a>
			</Link>
		);
	}

	if (linkProps.onClick)
		return (
			<button
				className={className}
				onClick={() => linkProps.onClick()}
				{...props}>
				<Icon className='h-1_5 w-1_5 mr-1 ' />
				<span>{linkProps.text}</span>
			</button>
		);

	return null;
};

const getListOfLinks = (
	profile: StudentProfileOtherInfo,
	refetch: (
		variables?: Partial<Record<string, any>> | undefined
	) => Promise<ApolloQueryResult<any>>
) => {
	const unlinkGoogle = getUnlinkSocialMutation(
		profile,
		refetch,
		LoginStrategies.GOOGLE
	);
	const unlinkGitlab = getUnlinkSocialMutation(
		profile,
		refetch,
		LoginStrategies.GITLAB
	);
	const unlinkGithub = getUnlinkSocialMutation(
		profile,
		refetch,
		LoginStrategies.GITHUB
	);

	return {
		[AccountSocial.GITHUB_LINK]: {
			icon: GithubIcon,
			text: 'Enlazar con Github',
			className: 'text-white bg-github hover:bg-github-hover',
			href: RestEndPoints.GITHUB_LINK,
		},
		[AccountSocial.GITLAB_LINK]: {
			icon: GitlabIcon,
			text: 'Enlazar con Gitlab',
			className: 'text-white bg-gitlab hover:bg-gitlab-hover',
			href: RestEndPoints.GITLAB_LINK,
		},
		[AccountSocial.GOOGLE_LINK]: {
			icon: GoogleIcon,
			text: 'Enlazar con Google',
			className:
				'border border-google-border hover:bg-google-hover dark:bg-white dark:text-white dark:text-white-dark dark:hover:bg-white-dark dark:hover:text-white',
			href: RestEndPoints.GOOGLE_LINK,
		},
		[AccountSocial.GITHUB_UNLINK]: {
			icon: GithubIcon,
			text: 'Eliminar cuenta',
			className: 'text-white bg-github hover:bg-github-hover',
			href: undefined,
			onClick: unlinkGithub,
		},
		[AccountSocial.GITLAB_UNLINK]: {
			icon: GitlabIcon,
			text: 'Eliminar cuenta',
			className: 'text-white bg-gitlab hover:bg-gitlab-hover',
			href: undefined,
			onClick: unlinkGitlab,
		},
		[AccountSocial.GOOGLE_UNLINK]: {
			icon: GoogleIcon,
			text: 'Eliminar cuenta',
			className:
				'border border-google-border hover:bg-google-hover dark:bg-white dark:text-white dark:text-white-dark dark:hover:bg-white-dark dark:hover:text-white',
			href: undefined,
			onClick: unlinkGoogle,
		},
	};
};

const getUnlinkSocialMutation = (
	profile: StudentProfileOtherInfo,
	refetch: (
		variables?: Partial<Record<string, any>> | undefined
	) => Promise<ApolloQueryResult<any>>,
	strategy: LoginStrategies
) => {
	const mutation = GraphqlStudent.student_unlink_social_profile;

	const [unlinkSocialMutation] = useMutation(mutation, {
		onCompleted: async () => {
			refetch();
			toast.success(AlertMessages.SOCIAL_UNLINK_SUCCESS);
		},
		onError: error => {
			toast.error(error.message);
		},
		variables: {
			input: {
				id: profile.socialAccounts.filter(
					account => account.type === strategy.toUpperCase()
				)[0]?.id,
				type: strategy,
			},
		},
	});

	return unlinkSocialMutation;
};

export default SocialLoginLink;
