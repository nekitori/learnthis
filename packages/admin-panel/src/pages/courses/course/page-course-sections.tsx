import { ApolloError, useMutation } from '@apollo/client';
import CourseDragRow from '@Components/courses/course/course-drag-row';
import Button from '@Components/generic/form/button/button';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { ISection } from '@Interfaces/section.interface';
import { withAuth } from '@Lib/hoc/withAuth';
import { CourseWrapperProps } from '@Types/props/with-course-props.type';
import { GraphqlCourse } from 'learnthis-utils';
import { FC, useEffect, useState } from 'react';
import { arrayMove, List } from 'react-movable';
import { toast } from 'react-toastify';

const PageCourseSections: FC<CourseWrapperProps> = ({ course, refetch }) => {
	const [sections, setSections] = useState<ISection[]>([]);

	const { sortSectionsMutation, loading } = getMutation(refetch);

	useEffect(() => {
		if (course) setSections(course.sections);
	}, [course]);

	return (
		<>
			<h3 className='mb-2 text-center text-white-dark dark:text-white text-2xl font-semibold'>
				Reordenar secciones
			</h3>
			<List
				values={sections}
				lockVertically
				onChange={({ oldIndex, newIndex }) =>
					setSections(arrayMove(sections, oldIndex, newIndex))
				}
				renderList={({ children, props }) => <div {...props}>{children}</div>}
				renderItem={({ value, index, props }) => (
					<CourseDragRow section={value} sectionIndex={index} {...props} />
				)}
			/>
			<div className='mt-1 text-center'>
				<Button
					kind='primary'
					loading={loading}
					disabled={course?.sections === sections}
					onClick={() => {
						sortSectionsMutation({
							variables: {
								courseId: course?._id,
								sectionIdsSorted: sections.map(section => section._id),
							},
						});
					}}>
					Reordenar
				</Button>
			</div>
		</>
	);
};

const getMutation = (refetch: ApolloQueryRefetch) => {
	const [sortSectionsMutation, { loading }] = useMutation(
		GraphqlCourse.course_admin_sort_sections,
		{
			onCompleted: () => {
				toast.success('Lecciones reordenadas con éxito');
				refetch();
			},
			onError: (error: ApolloError) => {
				toast.error(error.message);
			},
		}
	);

	return { sortSectionsMutation, loading };
};

export default withAuth(PageCourseSections, [WorkerRoles.ADMIN]);
