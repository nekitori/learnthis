import { Field, InputType } from '@nestjs/graphql';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';

/**
 * Graphql input type: Register a new worker
 */
@InputType()
export class WorkerRegisterDto {
	/** Worker's email */
	@Field()
	email: string;
	/** Worker's name */
	@Field()
	name: string;
	/** Worker's surname */
	@Field()
	surname: string;
	/** Worker's password */
	@Field()
	password: string;
	/** Worker's roles on the platform */
	@Field(type => [WorkerRoles])
	roles: WorkerRoles[];
}
