import gql from 'graphql-tag';

//#region Public

export const course_public_find = gql`
	query course_public_find {
		course_public_find {
			_id
			compareAtPrice
			image
			price
			description
			title
			url
		}
	}
`;

export const course_public_find_by_url = gql`
	query course_public_find_by_url($courseUrl: String!) {
		course_public_find_by_url(courseUrl: $courseUrl) {
			_id
			compareAtPrice
			image
			background
			price
			description
			title
			url
			studentsCount
			sections {
				title
				description
				lessons {
					__typename
					title
					... on VideoLessonPublic {
						duration
					}
				}
			}
		}
	}
`;

export const course_public_find_by_url_array = gql`
	query course_public_find_by_url_array($coursesUrls: [String!]!) {
		course_public_find_by_url_array(coursesUrls: $coursesUrls) {
			_id
			compareAtPrice
			image
			price
			description
			title
			url
		}
	}
`;

//#endregion

//#region Find

export const course_admin_find = gql`
	query course_admin_find($paginate: PaginateDto) {
		course_admin_find(paginate: $paginate) {
			data {
				_id
				compareAtPrice
				image
				background
				price
				description
				title
				url
				studentsCount
				visibility
			}
			offset
			limit
			total
		}
	}
`;

export const course_admin_find_by_id = gql`
	query course_admin_find_by_id($courseId: ID!) {
		course_admin_find_by_id(courseId: $courseId) {
			_id
			compareAtPrice
			image
			background
			price
			description
			title
			url
			studentsCount
			visibility
			sections {
				_id
				title
				description
				visibility
				lessons {
					__typename
					_id
					title
					visibility
					... on VideoLesson {
						duration
					}
				}
			}
		}
	}
`;

export const course_admin_find_by_id_array = gql`
	query course_admin_find_by_id_array($courseIds: [ID!]) {
		course_admin_find_by_id_array(courseIds: $courseIds) {
			_id
			title
			image
		}
	}
`;

//#endregion

//#region Course

export const course_admin_create = gql`
	mutation course_admin_create($courseData: CourseCreateDto!) {
		course_admin_create(courseData: $courseData)
	}
`;

export const course_admin_modify = gql`
	mutation course_admin_modify($courseId: ID!, $input: CourseModifyDto!) {
		course_admin_modify(courseId: $courseId, input: $input)
	}
`;

export const course_admin_sort_sections = gql`
	mutation course_admin_sort_sections(
		$courseId: ID!
		$sectionIdsSorted: [ID!]!
	) {
		course_admin_sort_sections(
			courseId: $courseId
			sectionIdsSorted: $sectionIdsSorted
		)
	}
`;

export const course_admin_delete = gql`
	mutation course_admin_delete($courseId: ID!) {
		course_admin_delete(courseId: $courseId)
	}
`;

//#endregion

//#region Publish

export const course_admin_publish = gql`
	mutation course_admin_publish($courseId: ID!) {
		course_admin_publish(courseId: $courseId)
	}
`;

export const course_admin_unpublish = gql`
	mutation course_admin_unpublish($courseId: ID!) {
		course_admin_unpublish(courseId: $courseId)
	}
`;

//#endregion
