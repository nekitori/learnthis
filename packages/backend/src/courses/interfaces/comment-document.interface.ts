import { ObjectID } from '@Common/types/object-id.type';
import { IStudentDoc } from '@Students/interfaces/student-document.interface';
import { Document } from 'mongoose';

export type AuthorOrObjectId = IStudentDoc | ObjectID;
export type RootCommentOrObjectId = IRootCommentDoc | ObjectID;
export type ReplyCommentOrObjectId = IReplyCommentDoc | ObjectID;

export type TCommentModel = ICommentDoc<AuthorOrObjectId>;
export type TRootCommentModel = IRootCommentDoc<
	ReplyCommentOrObjectId,
	AuthorOrObjectId
>;
export type TReplyCommentModel = IReplyCommentDoc<
	RootCommentOrObjectId,
	AuthorOrObjectId
>;

export interface IComment<T extends AuthorOrObjectId = ObjectID> {
	author: T;
	message: string;
}

export interface ICommentDoc<T extends AuthorOrObjectId = ObjectID>
	extends IComment<T>,
		Document {
	kind?: string;
}

export interface IRootComment<
	T extends ReplyCommentOrObjectId = ObjectID,
	S extends AuthorOrObjectId = ObjectID
> extends IComment<S> {
	replies: T[];
}
export interface IRootCommentDoc<
	T extends ReplyCommentOrObjectId = ObjectID,
	S extends AuthorOrObjectId = ObjectID
> extends IRootComment<T, S>,
		Document {}

export interface IReplyComment<
	T extends RootCommentOrObjectId = ObjectID,
	S extends AuthorOrObjectId = ObjectID
> extends IComment<S> {
	replyTo: T;
}
export interface IReplyCommentDoc<
	T extends RootCommentOrObjectId = ObjectID,
	S extends AuthorOrObjectId = ObjectID
> extends IReplyComment<T, S>,
		Document {}
