import { Course } from '@Courses/gql-types/course.gqltype';
import { Field, Float, ObjectType } from '@nestjs/graphql';
import { OrderStatus } from '@Orders/enums/order-status.enum';

export class OrderBase {
	student: string;
	status: OrderStatus;
	@Field(type => Float)
	totalWithTax?: number;
	@Field(type => Float)
	totalWithoutTax?: number;
}

@ObjectType()
export class Order extends OrderBase {
	orderLines: OrderLines[];
}

@ObjectType()
export class FullOrder extends OrderBase {
	orderLines: FullOrderLines[];
}

@ObjectType()
export class OrderLines {
	priceWithoutTax: number;
	priceWithTax: number;
	course: string;
}

@ObjectType()
export class FullOrderLines {
	priceWithoutTax: number;
	priceWithTax: number;
	course: Course;
}
