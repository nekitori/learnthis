import { withAuth } from '@Lib/hoc/withAuth';
import { FC } from 'react';

/**
 * Page component for the main panel, whose content depends on each role
 *
 * Authenticated: YES
 * Allowed roles: All
 */
const PageDashboard: FC = () => {
	return <></>;
};

export default withAuth(PageDashboard);
