/**
 * Model DB names
 */
export enum StudentModels {
	STUDENT = 'Student',
	TOKEN = 'StudentToken',
}
