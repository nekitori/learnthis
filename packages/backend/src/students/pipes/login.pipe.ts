import { StudentErrors } from '@Students/enums/student-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation } from 'learnthis-utils';
import { LoginDto } from '../dto/login.dto';

/**
 * LoginDto pipe validator
 */
@Injectable()
export class LoginPipe implements PipeTransform {
	/**
	 * LoginDto hanler validator
	 *
	 * @param  {LoginDto} value
	 * @returns LoginDto
	 */
	transform(value: LoginDto): LoginDto {
		if (
			!FormValidation.emailValidation(value.email) ||
			!FormValidation.passwordValidation(value.password)
		)
			throw new BadRequestException(StudentErrors.INVALID_LOGIN);
		return value;
	}
}
