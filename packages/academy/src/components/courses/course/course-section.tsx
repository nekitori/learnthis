import { DownArrowIcon } from '@Icons/menu/down-arrow-icon';
import { PublicSection } from '@Interfaces/course/course.interface';
import {
	InfoLessonPublic,
	VideoLessonPublic,
} from '@Interfaces/course/lesson.interface';
import { Dispatch, FC, SetStateAction } from 'react';
import LessonCourse from './course-lesson';

export type SectionLessonProps = {
	section: PublicSection;
	index: number;
	sectionState: number;
	setSectionState: Dispatch<SetStateAction<number>>;
};

const SectionCourse: FC<SectionLessonProps> = ({
	section,
	index,
	sectionState,
	setSectionState,
}) => {
	return (
		<div className='shadow-lg flexcol-c-c mb-1 rounded-md box-border overflow-hidden'>
			<div
				className='bg-white dark:bg-white-dark py-1 px-1_5 w-full cursor-pointer flex-s-c'
				onClick={() => changeSectionState(index, setSectionState)}>
				<div className='flex-s-c w-11/12'>
					<p className='mr-1 font-semibold text-xl text-white-dark dark:text-white'>
						{index + 1}
					</p>
					<h3 className='font-semibold text-xl text-white-dark dark:text-white'>
						{section.title}
					</h3>
				</div>
				<div className='w-1/12'>
					<DownArrowIcon
						className={`ml-0_5 w-1_5 text-white-dark dark:text-white fill-current transform transition-all-eio-250 ${
							sectionState === index ? 'rotate-0' : '-rotate-90'
						}`}
					/>
				</div>
			</div>
			<div
				className={`w-full transition-all-eio-250 ${
					sectionState === index ? 'opacity-1 max-h-30' : 'opacity-0 max-h-0'
				}`}>
				{section.lessons.map(
					(
						lesson: VideoLessonPublic | InfoLessonPublic,
						indexLesson: number
					) => (
						<LessonCourse key={indexLesson} lesson={lesson} />
					)
				)}
			</div>
		</div>
	);
};

const changeSectionState = (
	index: number,
	setSectionState: Dispatch<SetStateAction<number>>
) =>
	setSectionState((oldState: number) => {
		if (oldState === index) return -1;
		else return index;
	});

export default SectionCourse;
