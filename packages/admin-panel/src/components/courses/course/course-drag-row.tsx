import { ISection } from '@Interfaces/section.interface';
import { forwardRef } from 'react';
import { IItemProps } from 'react-movable';
import CoursePublishedSpan from '../course-published-span';

interface CourseRowProps extends IItemProps {
	section: ISection;
	sectionIndex?: number;
}

const CourseDragRow = forwardRef<HTMLDivElement, CourseRowProps>(
	({ section, sectionIndex, ...props }, ref) => (
		<div
			className='flex flex-wrap w-full p-0_75 border-b border-gray-light dark:border-gray-dark'
			ref={ref}
			{...props}>
			<div className='w-1/12 xssm:w-1/6'>
				<div className='flex-c-c rounded-full bg-primary h-2_5 w-2_5'>
					<span className='text-white font-semibold text-1xl'>
						{sectionIndex ? sectionIndex + 1 : 1}
					</span>
				</div>
			</div>
			<div className='w-8/12 xssm:w-5/6 xssm:mb-0_75 flex-s-c px-0_5'>
				<span className='font-semibold text-md text-black dark:text-white'>
					{section.title}
				</span>
			</div>
			<div className='w-2/12 xssm:w-1/2 xssm:order-4 xssm:text-right flex-c-c'>
				<span className='font-semibold text-md text-black dark:text-white'>
					<CoursePublishedSpan visibility={section.visibility} />
				</span>
			</div>
		</div>
	)
);

export default CourseDragRow;
