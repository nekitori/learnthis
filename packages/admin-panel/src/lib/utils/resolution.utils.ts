import { ResolutionBreakpoints } from '@Enums/config/resolution-breakpoint.enum';
import { ResolutionBreakpointValues } from '@Config/resolution.settings';
import { IResolutionState } from '@Interfaces/states/resolution-state.interface';

export const getResolution = (): IResolutionState => {
	const innerWidth = window.innerWidth;

	let resolution = ResolutionBreakpoints.LG;
	let isMobile = false;

	if (innerWidth <= ResolutionBreakpointValues.XS) {
		resolution = ResolutionBreakpoints.XS;
		isMobile = true;
	} else if (innerWidth <= ResolutionBreakpointValues.SM) {
		resolution = ResolutionBreakpoints.SM;
		isMobile = true;
	} else if (innerWidth <= ResolutionBreakpointValues.MD) {
		resolution = ResolutionBreakpoints.MD;
		isMobile = false;
	}

	return { resolution, isMobile };
};
