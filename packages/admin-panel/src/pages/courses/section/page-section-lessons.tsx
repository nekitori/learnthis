import { ApolloError, useMutation } from '@apollo/client';
import SectionDragRow from '@Components/courses/section/section-drag-row';
import Button from '@Components/generic/form/button/button';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { ILesson } from '@Interfaces/lesson.interface';
import { withAuth } from '@Lib/hoc/withAuth';
import { SectionWrapperProps } from '@Types/props/with-section-props.type';
import { GraphqlCourseSection } from 'learnthis-utils';
import { FC, useEffect, useState } from 'react';
import { arrayMove, List } from 'react-movable';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

const PageSectionLessons: FC<SectionWrapperProps> = ({
	course,
	section,
	sectionId,
	refetch,
}) => {
	const [lessons, setLessons] = useState<ILesson[]>([]);

	const { sortLessonsMutation, loading } = getSortLessonsMutation(refetch);

	useEffect(() => {
		if (section) setLessons(section.lessons);
	}, [section]);

	return (
		<>
			<List
				values={lessons}
				lockVertically
				onChange={({ oldIndex, newIndex }) =>
					setLessons(arrayMove(lessons, oldIndex, newIndex))
				}
				renderList={({ children, props }) => <div {...props}>{children}</div>}
				renderItem={({ value, props }) => (
					<SectionDragRow lesson={value} {...props} />
				)}
			/>
			<div className='mt-1 flex-sa-c'>
				<Button
					kind='primary'
					loading={loading}
					disabled={section?.lessons === lessons}
					onClick={() => {
						sortLessonsMutation({
							variables: {
								courseId: course?._id,
								sectionId,
								lessonIdsSorted: lessons.map(lesson => lesson._id),
							},
						});
					}}>
					Reordenar
				</Button>
				<Link
					to={MainPaths.SECTION_CREATE_LESSON.replace(
						MainParams.COURSE_ID,
						course?._id || ''
					).replace(MainParams.SECTION_ID, sectionId || '')}
					className='flex-s-c bg-primary px-1 py-0_5 rounded-md shadow-md text-white'>
					Crear lección
				</Link>
			</div>
		</>
	);
};

const getSortLessonsMutation = (refetch: ApolloQueryRefetch) => {
	const [sortLessonsMutation, { loading }] = useMutation(
		GraphqlCourseSection.course_admin_section_sort_lessons,
		{
			onCompleted: () => {
				toast.success('Lecciones reordenadas con éxito');
				refetch();
			},
			onError: (error: ApolloError) => {
				toast.error(error.message);
			},
		}
	);

	return { sortLessonsMutation, loading };
};

export default withAuth(PageSectionLessons, [WorkerRoles.ADMIN]);
