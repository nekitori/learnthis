import { Logger } from '@nestjs/common';
import { AsyncResource } from 'async_hooks';
import os from 'os';
import { Observable, Subscriber } from 'rxjs';
import { Worker } from 'worker_threads';

interface QueueItem<K> {
	data: any;
	message?: string;
	sub: Subscriber<K>;
	onStart?: (wkr: Worker) => void;
}

class WorkerPoolTaskInfo extends AsyncResource {
	constructor() {
		super('WorkerPoolTaskInfo');
	}

	emit(
		target: any,
		callback: (this: null, ...args: any[]) => unknown,
		result: any
	) {
		this.runInAsyncScope(callback, target, ...result);
	}

	done(
		target: any,
		callback: (this: null, ...args: any[]) => unknown,
		result: any
	) {
		this.runInAsyncScope(callback, target, ...result);
		this.emitDestroy(); // `TaskInfo`s done are used only once.
	}
}

export class RxQueueWorkerPool {
	private queue: QueueItem<any>[] = [];
	private workers: Worker[] = [];
	private errWorkers = new WeakSet<Worker>();
	private activeWorkers: WeakMap<Worker, boolean> = new WeakMap();
	private tasksInfos: WeakMap<Worker, WorkerPoolTaskInfo> = new WeakMap();
	/**
	 * @param  workerPath file path of worker
	 * @param  numberOfThreads number of threads used by queue
	 */
	constructor(
		public readonly workerPath: string,
		public readonly numberOfThreads: number = 2
	) {
		this.init();
	}

	/**
	 * get any inactive worker
	 */
	private getInactiveWorker(): Worker | null {
		for (let index = 0; index < this.workers.length; index++) {
			if (this.activeWorkers.get(this.workers[index]) === false)
				return this.workers[index];
		}
		return null;
	}

	/**
	 * Load resources used by queue
	 */
	private init() {
		if (this.numberOfThreads > os.cpus().length)
			Logger.warn(
				'Threads pools are greater than number of cpus',
				'RxQueueWorkerPool'
			);
		else if (this.numberOfThreads < 0)
			Logger.warn('Ilegal nº threads defined', 'RxQueueWorkerPool');

		for (let i = 0; i < this.numberOfThreads; i++) {
			const worker = new Worker(this.workerPath, {
				stdout: true,
			});
			Logger.log(`Thread init: ${worker.threadId}`, 'RxQueueWorkerPool');
			this.workers.push(worker);
			this.activeWorkers.set(worker, false);
		}
	}

	/**
	 * @param data data for create task
	 * @param msg init message for worker actions
	 */
	public run<K>(data: any, msg: string = '') {
		return new Observable<K>(subscriber => {
			const queueItem: QueueItem<K> = {
				data,
				message: msg,
				sub: subscriber,
			};
			let availableWorker = this.getInactiveWorker();
			if (availableWorker === null)
				this.queue.push({
					...queueItem,
					onStart: wkr => (availableWorker = wkr),
				});
			else this.runWorker(availableWorker, queueItem);

			return () => {
				if (availableWorker) {
					availableWorker.removeAllListeners();
					if (this.errWorkers.has(availableWorker)) {
						this.errWorkers.delete(availableWorker);
						this.workers = this.workers.filter(
							curretnWkr => curretnWkr !== availableWorker
						);
						const wkr = new Worker(this.workerPath);
						this.workers.push(wkr);
						this.activeWorkers.set(wkr, false);
					}
					if (this.queue.length > 0)
						this.runWorker(availableWorker, this.queue.shift());
					else this.activeWorkers.set(availableWorker, false);
				} else Logger.warn('Pool thread concurrency warn');
			};
		});
	}

	/**
	 * @param worker worker thread to run a task
	 * @param queueItem data info of task
	 */
	private runWorker(worker: Worker, queueItem: QueueItem<any>) {
		queueItem.onStart && queueItem.onStart(worker);
		this.activeWorkers.set(worker, true);
		this.tasksInfos.set(worker, new WorkerPoolTaskInfo());
		const messageCallback = (result: any) => {
			if (result)
				this.tasksInfos
					.get(worker)
					?.emit(queueItem.sub, queueItem.sub.next, [result]);
			else
				this.tasksInfos
					.get(worker)
					?.done(queueItem.sub, queueItem.sub.complete, []);
		};
		const errorCallback = (error: Error) => {
			this.errWorkers.add(worker);
			this.tasksInfos
				.get(worker)
				?.done(queueItem.sub, queueItem.sub.error, [error]);
		};
		worker.on('message', messageCallback);
		worker.once('error', errorCallback);
		worker.postMessage({
			data: queueItem.data,
			message: queueItem.message,
		});
	}
	/**
	 * Resest all resources
	 */
	async reset() {
		await this.destroy();
		this.init();
	}
	/**
	 * Destroy all resources used by queue
	 */
	async destroy() {
		for (let index = 0; index < this.workers.length; index++) {
			if (this.activeWorkers.get(this.workers[index]) === false) {
				this.workers[index].removeAllListeners();
				await this.workers[index].terminate();
			}
		}
		this.errWorkers = new WeakSet();
		this.workers = [];
		this.tasksInfos = new WeakMap();
		this.activeWorkers = new WeakMap();
		this.queue = [];
	}
}
