import BlankCard from '@Components/generic/cards/blank-card';
import PlugIcon from '@Components/icons/other/plug-icon';
import Error404 from '@Components/other/error-404';
import { withAuth } from '@Lib/hoc/withAuth';
import { FC } from 'react';

const PageNotFound: FC = () => <Error404 />;

export default withAuth(PageNotFound);
