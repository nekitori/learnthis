import { BaseRoleGuard } from '@Common/auth/base-role.guard';
import { CommonErrors } from '@Common/enums/common-errors.enum';
import {
	CanActivate,
	ExecutionContext,
	ForbiddenException,
	Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';

@Injectable()
export class WorkerRestRolesGuard extends BaseRoleGuard implements CanActivate {
	constructor(private readonly reflector: Reflector) {
		super();
	}

	canActivate(context: ExecutionContext): boolean {
		const roles = this.reflector.get<WorkerRoles[]>(
			'roles',
			context.getHandler()
		);

		if (!roles) throw new ForbiddenException(CommonErrors.UNAUTHORIZED);

		const user = context.switchToHttp().getRequest().user;

		return this.isUserAllowed(user.roles, roles);
	}
}
