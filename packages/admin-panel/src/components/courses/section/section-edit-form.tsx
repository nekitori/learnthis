import { ApolloError, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import Input from '@Components/generic/form/input';
import TextArea from '@Components/generic/form/textarea';
import { AlertMessages, FormMessages } from '@Enums/config/constants';
import { ApolloMutation } from '@Interfaces/apollo/apollo-query.types';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { ISection } from '@Interfaces/section.interface';
import { Form, Formik, FormikConfig } from 'formik';
import { GraphqlCourseSection } from 'learnthis-utils';
import { FC } from 'react';
import { toast } from 'react-toastify';
import { object as YupObject, string as YupString } from 'yup';

//#region TS

/**
 * Enum for form field names
 */
enum SectionEditFields {
	TITLE = 'title',
	DESCRIPTION = 'description',
}

/**
 * Interface for form field values
 */
interface ISectionEditInput {
	[SectionEditFields.TITLE]: string;
	[SectionEditFields.DESCRIPTION]: string;
}

//#endregion

type SectionEditFormProps = {
	section: ISection;
	sectionId: string;
	courseId: string;
	refetch: ApolloQueryRefetch;
};

const SectionEditForm: FC<SectionEditFormProps> = ({
	section,
	sectionId,
	courseId,
	refetch,
}) => {
	const { editSectionMutation, loading } = getEditSectionMutation(refetch);

	const formikConfig = getForm(
		section,
		sectionId,
		courseId,
		editSectionMutation
	);

	return (
		<Formik {...formikConfig}>
			<Form className='w-full max-w-24 mx-auto'>
				<Input
					name={SectionEditFields.TITLE}
					className='mt-1'
					type='text'
					label='Título'
				/>
				<TextArea
					name={SectionEditFields.DESCRIPTION}
					className='mt-1'
					rows={6}
					label='Descripción'
				/>
				<Button
					loading={loading}
					className='mt-2 p-0_75 w-10'
					type='submit'
					kind='primary'>
					Actualizar
				</Button>
			</Form>
		</Formik>
	);
};

const getEditSectionMutation = (refetch: ApolloQueryRefetch) => {
	const [editSectionMutation, { loading }] = useMutation(
		GraphqlCourseSection.course_admin_section_modify,
		{
			onCompleted: () => {
				toast.success(AlertMessages.SECTION_MODIFY_SUCCESS);
				refetch();
			},
			onError: (error: ApolloError) => {
				toast.error(error.message || AlertMessages.SERVER_ERROR);
			},
		}
	);

	return { editSectionMutation, loading };
};

const getForm = (
	section: ISection,
	sectionId: string,
	courseId: string,
	editSectionMutation: ApolloMutation
): FormikConfig<ISectionEditInput> => {
	const initialValues = {
		[SectionEditFields.TITLE]: section.title || '',
		[SectionEditFields.DESCRIPTION]: section.description || '',
	};

	const validationSchema = YupObject().shape({
		[SectionEditFields.TITLE]: YupString().required(
			FormMessages.TITLE_REQUIRED
		),
		[SectionEditFields.DESCRIPTION]: YupString().required(
			FormMessages.DESCRIPTION_REQUIRED
		),
	});

	const onSubmit = (values: ISectionEditInput) => {
		const input: Partial<ISectionEditInput> = {};

		Object.keys(values).forEach((key: any) => {
			// @ts-ignore
			if (values[key] !== initialValues[key]) {
				// @ts-ignore
				input[key] = values[key];
			}
		});

		if (!Object.keys(input).length)
			toast.error(AlertMessages.NOTHING_TO_MODIFY);
		else
			editSectionMutation({
				variables: {
					courseId,
					sectionId,
					input: {
						...input,
					},
				},
			});
	};

	return {
		initialValues,
		validationSchema,
		onSubmit,
		validateOnChange: false,
	};
};

export default SectionEditForm;
