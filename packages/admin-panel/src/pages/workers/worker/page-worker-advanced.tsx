import { ApolloError, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { WorkerProfileProps } from '@Interfaces/props/worker-profile-props.interface';
import { GraphqlWorker } from 'learnthis-utils';
import { FC, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

enum FormStates {
	BASE,
	CONFIRM_ENABLE,
	CONFIRM_DISABLE,
	CONFIRM_REMOVE_STEP_1,
	CONFIRM_REMOVE_STEP_2,
}

/**
 * Subpage component to perform advanced actions on the worker.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 *
 * @param props.worker Worker data
 * @param props.refetch Apollo worker query refetch
 */
const PageWorkerAdvanced: FC<WorkerProfileProps> = ({ worker, refetch }) => {
	const [formState, setFormState] = useState<FormStates>(FormStates.BASE);

	const {
		enableWorkerMutation,
		disableWorkerMutation,
		deleteWorkerMutation,
		loading,
	} = getMutations(worker._id, refetch);

	const confirmPopups: [string, () => void][] = [];
	confirmPopups[FormStates.CONFIRM_DISABLE] = [
		`¿Está seguro de que desea deshabilitar a ${worker.displayName}?`,
		() => disableWorkerMutation(),
	];
	confirmPopups[FormStates.CONFIRM_ENABLE] = [
		`¿Está seguro de que desea habilitar a ${worker.displayName}?`,
		() => enableWorkerMutation(),
	];
	confirmPopups[FormStates.CONFIRM_REMOVE_STEP_1] = [
		`¿Está seguro de que desea eliminar definitivamente a ${worker.displayName}?`,
		() => setFormState(FormStates.CONFIRM_REMOVE_STEP_2),
	];
	confirmPopups[FormStates.CONFIRM_REMOVE_STEP_2] = [
		'Esta acción no se puede deshacer, ¿está seguro?',
		() => deleteWorkerMutation(),
	];

	if (formState !== FormStates.BASE) {
		const popup = confirmPopups[formState];
		return (
			<div className='w-full max-w-20 mx-auto'>
				<p className='text-center mb-1_5 text-black dark:text-white'>
					{popup[0]}
				</p>
				<div className='flex-c-c'>
					<Button
						loading={loading}
						kind='border'
						className='mr-1'
						onClick={popup[1]}>
						Aceptar
					</Button>
					<Button
						kind='primary'
						className='ml-1'
						onClick={() => setFormState(FormStates.BASE)}>
						Cancelar
					</Button>
				</div>
			</div>
		);
	} else {
		return (
			<div className='w-full flex-c-c'>
				<div className='w-full max-w-25'>
					<h2 className='font-semibold mb-0_5 text-black dark:text-white'>
						{`${worker.active ? ' Desactivar' : 'Activar'} a ${
							worker.displayName
						}`}
					</h2>
					<p className='text-black dark:text-white mb-1'>
						{worker.active
							? 'El usuario se encuentra activo. Si se desactiva, no podrá iniciar sesión en la plataforma, ni realizar ninguna de las acciones asociadas a su rol.'
							: 'El usuario se encuentra desactivado. Si se activa, podrá iniciar sesión en la plataforma y realizar todas las acciones asociadas a su rol.'}
					</p>
					{worker.active ? (
						<Button
							kind='red'
							onClick={() => setFormState(FormStates.CONFIRM_DISABLE)}>
							Desactivar
						</Button>
					) : (
						<Button
							kind='green'
							onClick={() => setFormState(FormStates.CONFIRM_ENABLE)}>
							Activar
						</Button>
					)}
					<hr className='m-1_5 border-gray-light dark:border-gray-dark' />
					<h2 className='font-semibold mb-0_5 text-black dark:text-white'>{`Eliminar a ${worker.displayName}`}</h2>
					<p className='text-black dark:text-white'>
						Esta opción implica eliminar completamente al usuario de la base de
						datos. Para ello, es necesario eliminar primero al usuario de todos
						los cursos de los que es profesor.
					</p>
					<p className='my-1 text-black dark:text-white'>
						Esta acción <strong>NO</strong> se puede deshacer.
					</p>
					<Button
						kind='border-red'
						onClick={() => setFormState(FormStates.CONFIRM_REMOVE_STEP_1)}>
						Eliminar permanentemente
					</Button>
				</div>
			</div>
		);
	}
};

/**
 * Gets the mutations associated with the advanced options for the user.
 *
 * @param workerId Worker identifier
 * @param refetch Apollo worker query refetch
 */
const getMutations = (workerId: string, refetch: ApolloQueryRefetch) => {
	const history = useHistory();

	const options = {
		variables: { workerId },
		onError: (error: ApolloError) => {
			toast.error(error.message);
		},
	};

	const [enableWorkerMutation, { loading: enableLoading }] = useMutation(
		GraphqlWorker.worker_enable,
		{
			...options,
			onCompleted: () => {
				toast.success('Trabajador activado');
				refetch();
			},
		}
	);
	const [disableWorkerMutation, { loading: disableLoading }] = useMutation(
		GraphqlWorker.worker_disable,
		{
			...options,
			onCompleted: () => {
				toast.success('Trabajador desactivado');
				refetch();
			},
		}
	);
	const [deleteWorkerMutation, { loading: deleteLoading }] = useMutation(
		GraphqlWorker.worker_permanent_delete,
		{
			...options,
			onCompleted: () => {
				toast.success('Trabajador eliminado');
				history.push(MainPaths.WORKERS_LIST);
			},
		}
	);

	return {
		enableWorkerMutation,
		disableWorkerMutation,
		deleteWorkerMutation,
		loading: enableLoading || disableLoading || deleteLoading,
	};
};

export default PageWorkerAdvanced;
