import { Strategy as GitLabStrategy } from 'passport-gitlab2';
import { RestEndPoints } from '@Enums/paths/rest-endpoints.enum';
import { callbackStrategy } from './utils';

export const gLabStrategy = new GitLabStrategy(
	{
		clientID: process.env.GITLAB_CLIENT_ID,
		clientSecret: process.env.GITLAB_CLIENT_SECRET,
		callbackURL:
			process.env.NEXT_PUBLIC_SITE_URL + RestEndPoints.GITLAB_REDIRECT,
	},
	callbackStrategy
);
