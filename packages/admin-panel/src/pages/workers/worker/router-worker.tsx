import { ApolloError, useQuery } from '@apollo/client';
import UserLayout from '@Components/layout/user.layout';
import Loader from '@Components/other/loader';
import { MainPaths, UrlParams } from '@Enums/paths/main-paths.enum';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { WorkerProfileProps } from '@Interfaces/props/worker-profile-props.interface';
import { withAuth } from '@Lib/hoc/withAuth';
import { withSuspense } from '@Lib/hoc/withSuspense';
import { GraphqlWorker } from 'learnthis-utils';
import { lazy } from 'react';
import { Redirect, Route, Switch, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

//#region Lazy imports

const WorkerProfileSummary = lazy(
	() => import('@Pages/workers/worker/page-worker-main')
);
const WorkerProfileData = lazy(
	() => import('@Pages/workers/worker/page-worker-data')
);
const WorkerProfileAdvanced = lazy(
	() => import('@Pages/workers/worker/page-worker-advanced')
);
const WorkerProfileCourses = lazy(
	() => import('@Pages/workers/worker/page-worker-courses')
);

//#endregion

/**
 * Wrapper for worker data pages.
 * - Loads worker data.
 * - Switch component to render.
 */
export const RouterWorker = () => {
	const { data, loading, refetch, error } = getWorker();

	if (error) return <Redirect to={MainPaths.NOT_FOUND} />;

	const props: WorkerProfileProps = {
		worker: data?.worker_find_by_id,
		refetch,
	};

	const routes = [
		{
			exact: true,
			path: MainPaths.WORKER_DATA,
			component: withSuspense(WorkerProfileData, props),
		},
		{
			exact: true,
			path: MainPaths.WORKER_ADVANCED,
			component: withSuspense(WorkerProfileAdvanced, props),
		},
		{
			exact: true,
			path: MainPaths.WORKER_COURSES,
			component: withSuspense(WorkerProfileCourses, props),
		},
		{
			exact: true,
			path: MainPaths.WORKERS,
			component: withSuspense(WorkerProfileSummary, props),
		},
	];

	return (
		<UserLayout workerId={data?.worker_find_by_id._id}>
			{loading ? (
				<Loader />
			) : (
				<Switch>
					{routes.map((route, i) => (
						<Route
							key={i}
							path={route.path}
							exact={route.exact}
							component={route.component}
						/>
					))}
				</Switch>
			)}
		</UserLayout>
	);
};

/**
 *	Graphql query to get worker data.
 */
const getWorker = () => {
	const { workerId } = useParams<UrlParams>();

	const { data, loading, refetch, error } = useQuery(
		GraphqlWorker.worker_find_by_id,
		{
			variables: {
				workerId,
			},
			onError: (error: ApolloError) => {
				toast.error(error.message);
			},
		}
	);

	return { data, loading, refetch, error };
};

export default withAuth(RouterWorker, [WorkerRoles.ADMIN]);
