import { Document } from 'mongoose';
import { StudentTokenTypes } from '../enums/student-token-types.enum';
import { ObjectID } from '@Common/types/object-id.type';

export interface TokenDocument extends Document {
	token: string;
	student: ObjectID;
	type: StudentTokenTypes;
	expiresAt: Date;
}
