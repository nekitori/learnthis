import { ApolloError, useLazyQuery } from '@apollo/client';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { IWorker } from '@Interfaces/worker.interface';
import { withAuth } from '@Lib/hoc/withAuth';
import { CourseWrapperProps } from '@Types/props/with-course-props.type';
import { FC, useRef, useState } from 'react';
import { GraphqlWorker } from 'learnthis-utils';
import { AlertMessages } from '@Enums/config/constants';
import { toast } from 'react-toastify';
import UserPhoto from '@Components/workers/user-photo';
import { AddIcon } from '@Components/icons/generic/add-icon';
import { ApolloLazyQuery } from '@Interfaces/apollo/apollo-query.types';

const PageCourseTutors: FC<CourseWrapperProps> = ({ course, refetch }) => {
	const [selectableWorkers, setSelectableWorkers] = useState<IWorker[]>([]);

	const dropDownRef = useRef<HTMLDivElement>(null);

	const getWorkers = getWorkersQuery(setSelectableWorkers);

	return (
		<>
			<h3 className='mb-2 text-center text-white-dark dark:text-white text-2xl font-semibold'>
				Tutores
			</h3>
			<div className='mb-2 w-full px-1 py-0_5 rounded-lg border bg-background dark:bg-background-dark text-black dark:text-white relative'>
				<input
					className='appearance-none w-full bg-background-transparent'
					onChange={ev => {
						console.log('CHANGE', ev.target.value, ev.target.value.length);
						if (ev.target.value.length >= 3) {
							getWorkers({ variables: { workerEmail: ev.target.value } });
						} else setSelectableWorkers([]);
					}}></input>
				<div
					className={`w-full flex-col-c-c absolute left-0 top-2_5 transition-max-h-eio-250 shadow-lg overflow-y-hidden ${
						selectableWorkers.length ? 'max-h-8' : 'max-h-0'
					}`}
					onTransitionEnd={ev => {
						dropDownRef.current?.classList.toggle('overflow-y-hidden');
						dropDownRef.current?.classList.toggle('overflow-y-auto');
					}}>
					<div
						ref={dropDownRef}
						className={`w-full bg-background dark:bg-background-dark overflow-y-hidden border-t-2 border-gray ${
							selectableWorkers.length ? 'max-h-8' : 'max-h-0'
						}`}>
						{selectableWorkers.length ? (
							selectableWorkers.map((worker, index) => (
								<div
									key={index}
									className='w-full flex-wrap flex-s-c px-1 py-0_5'>
									<UserPhoto
										className='h-2_25 w-2_25 mr-1 rounded-full'
										photo={worker.photo}
										name={worker.displayName}
										surname=''
									/>
									<div className='flex flex-wrap w-5/6'>
										<div className='flex-col w-5/6'>
											<p className='font-semibold text-black dark:text-white'>
												{worker.displayName}
											</p>
											<p className='text-gray'>{worker.email}</p>
										</div>
										<div className='flex-c-c w-1/6'>
											<AddIcon className='h-1_75 w-1_75 text-black dark:text-white' />
										</div>
									</div>
								</div>
							))
						) : (
							<></>
						)}
					</div>
				</div>
			</div>
			<h4 className='mb-1 text-center text-white-dark dark:text-white text-lg font-semibold'>
				Lista de tutores
			</h4>
			<div></div>
		</>
	);
};

const getWorkersQuery = (
	setSelectableWorkers: React.Dispatch<React.SetStateAction<IWorker[]>>
): ApolloLazyQuery => {
	const query = GraphqlWorker.worker_find_by_regex_email;

	const [getWorkers] = useLazyQuery(query, {
		onCompleted: ({ worker_find_by_regex_email: workers }) => {
			console.log('COMPLETED');
			setSelectableWorkers(workers);
		},
		onError: () => (error: ApolloError) => {
			console.log('ERROR', error);
			toast.error(error.message || AlertMessages.SERVER_ERROR);
		},
	});

	return getWorkers;
};

export default withAuth(PageCourseTutors, [WorkerRoles.ADMIN]);
