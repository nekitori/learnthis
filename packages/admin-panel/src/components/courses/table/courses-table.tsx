import Pagination from '@Components/generic/pagination';
import Loader from '@Components/other/loader';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { ICourse } from '@Interfaces/course.interface';
import { Dispatch, FC, SetStateAction } from 'react';
import { useHistory } from 'react-router-dom';
import CourseRow from './course-row';

type CourseTableProps = {
	courses: ICourse[] | null;
	totalPages: number | null;
	page: number;
	setPage: Dispatch<SetStateAction<number>>;
};

const CoursesTable: FC<CourseTableProps> = ({
	courses,
	totalPages,
	page,
	setPage,
}) => {
	const history = useHistory<string>();

	return (
		<div className='font-semibold text-sm'>
			<div
				key={-1}
				className='flex-c-c xssm:hidden border-b border-gray-light dark:border-gray-dark pb-0_75'>
				<div className='xssm:w-full mdlg:w-5/12 flex-s-c px-1_5'>
					<span className='font-semibold text-sm text-black dark:text-white'>
						CURSO
					</span>
				</div>
				<div className='xssm:w-full mdlg:w-2/12 text-center'>
					<span className='font-semibold text-sm text-black dark:text-white'>
						ESTADO
					</span>
				</div>
				<div className='xssm:w-full mdlg:w-2/12 text-center'>
					<span className='font-semibold text-sm text-black dark:text-white'>
						PRECIO
					</span>
				</div>
				<div className='xssm:w-full mdlg:w-2/12 text-center'>
					<span className='font-semibold text-sm text-black dark:text-white'>
						ALUMNOS
					</span>
				</div>
				<div className='xssm:w-full mdlg:w-1/12 text-center'></div>
			</div>
			{courses && totalPages ? (
				<>
					{courses.map((i, index) => (
						<CourseRow key={index} course={i} />
					))}
					<Pagination
						current={page}
						total={totalPages}
						onPageChange={e => {
							setPage(e);
							history.push(MainPaths.COURSES_LIST + '?page=' + e);
						}}
					/>
				</>
			) : (
				<Loader />
			)}
		</div>
	);
};

export default CoursesTable;
