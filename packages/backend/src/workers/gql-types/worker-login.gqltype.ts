import { JwtToken } from '@Common/types/jwt-token.type';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Worker } from './worker.gqltype';

/**
 * Graphql type: Worker login response
 */
@ObjectType()
export class WorkerLogin {
	/** JWT token */
	@Field(() => ID)
	token: JwtToken;
	/** Worker data */
	user: Worker;
}
