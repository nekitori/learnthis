import { LeftArrowIcon } from '@Components/icons/generic/arrow-icons';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { ICourse } from '@Interfaces/course.interface';
import { FC } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import CoursePhoto from './course-photo';
import CoursePublishedSpan from './course-published-span';

type CourseSummaryProps = {
	course: ICourse;
};

const CourseSummary: FC<CourseSummaryProps> = ({ course }) => {
	const match = useRouteMatch([
		MainPaths.COURSE_SECTIONS,
		MainPaths.COURSE_TUTORS,
		MainPaths.COURSE_DATA,
		MainPaths.COURSE_ADVANCED,
		MainPaths.COURSE,
	]);

	return (
		<>
			{(!match || !match.isExact) && (
				<Link
					to={MainPaths.COURSE.replace(MainParams.COURSE_ID, course._id)}
					className='flex-s-c text-primary mb-1_5'>
					<LeftArrowIcon className='h-1_25 w-1_25 mr-0_5' />
					Volver al curso
				</Link>
			)}
			<div className='flex-s-c mb-1'>
				<CoursePhoto
					className='h-2 w-2 rounded-full mr-1'
					title={course.title}
					photo={course.image}
				/>
				<h2 className='text-lg font-semibold text-black dark:text-white'>
					{course.title}
				</h2>
			</div>
			<CoursePublishedSpan visibility={course.visibility} />
			<p className='text-gray my-1'>{course.description}</p>

			<ul className='w-full flex flex-wrap'>
				<li className='w-1/2 p-0_75'>
					<div className='flex-s-c'>
						<span className='h-1 w-1 bg-orange mr-1 rounded-md' />
						<span className='text-gray'>Precio</span>
					</div>
					<div className='flex-s-c p-0_5'>
						<span className='text-2xl font-semibold text-black dark:text-white'>{`${course.price}€`}</span>
						{course.compareAtPrice && (
							<span className='ml-1 text-2xl line-through text-orange'>{`${course.compareAtPrice}€`}</span>
						)}
					</div>
				</li>
				<li className='w-1/2 p-0_75'>
					<div className='flex-s-c'>
						<span className='h-1 w-1 bg-pink mr-1 rounded-md' />
						<span className='text-gray'>Usuarios</span>
					</div>
					<div className='flex-s-c p-0_5'>
						<span className='text-2xl font-semibold text-black dark:text-white'>
							{course.studentsCount}
						</span>
					</div>
				</li>
			</ul>
		</>
	);
};

export default CourseSummary;
