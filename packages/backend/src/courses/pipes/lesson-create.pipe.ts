import { CommonErrors } from '@Common/enums/common-errors.enum';
import { trimAllStrings } from '@Common/utils/clean-object';
import { LessonCreateDto } from '@Courses/dto/lesson-create.dto';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { LessonValidation } from 'learnthis-utils';

@Injectable()
export class LessonCreatePipe implements PipeTransform {
	transform(value: LessonCreateDto): LessonCreateDto {
		const { title, description } = value;

		const errors = [];

		if (!LessonValidation.titleValidation(title))
			errors.push(CommonErrors.FORMAT_INVALID_TITLE);

		if (!LessonValidation.descriptionValidation(description))
			errors.push(CommonErrors.FORMAT_INVALID_DESCRIPTION);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		trimAllStrings(value);

		return value;
	}
}
