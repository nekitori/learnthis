import { Gender } from '@Enums/generic/gender.enum';
import { LoginStrategies } from '@Enums/config/login-strategies.enum';

export interface StudentProfile {
	email: string;
	name: string;
	surname: string;
	cart: string[];
	coursesEnrolled: CourseEnrolled[];
	username?: string;
	photo?: string;
}

export interface StudentProfileOtherInfo {
	bio?: string;
	gender?: Gender;
	birthDate?: string;
	isSocialLogin: boolean;
	socialAccounts: SocialAccount[];
}

export interface FullStudentProfile
	extends StudentProfile,
		StudentProfileOtherInfo {}

export interface SocialAccount {
	id: string;
	type: LoginStrategies;
}

export interface CourseEnrolled {
	course: string;
}
