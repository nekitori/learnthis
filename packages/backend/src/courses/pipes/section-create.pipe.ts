import { CommonErrors } from '@Common/enums/common-errors.enum';
import { trimAllStrings } from '@Common/utils/clean-object';
import { SectionCreateDto } from '@Courses/dto/section-create.dto';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { SectionValidation } from 'learnthis-utils';

@Injectable()
export class SectionCreatePipe implements PipeTransform {
	transform(value: SectionCreateDto): SectionCreateDto {
		const { title, description } = value;

		const errors = [];

		if (!SectionValidation.titleValidation(title))
			errors.push(CommonErrors.FORMAT_INVALID_TITLE);

		if (!SectionValidation.descriptionValidation(description))
			errors.push(CommonErrors.FORMAT_INVALID_DESCRIPTION);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		trimAllStrings(value);

		return value;
	}
}
