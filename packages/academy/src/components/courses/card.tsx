import Button from '@Components/generic/form/button/button';
import { PublicCourse } from '@Interfaces/course/course.interface';
import Link from 'next/link';
import { FC } from 'react';

export type CardProps = {
	course: PublicCourse;
};

export const Card: FC<CardProps> = ({ course }) => (
	<Link href={`curso/${course.url}`}>
		<div className='shadow-lg transform transition-all-eio-125 hover:scale-105 rounded-xl bg-white dark:bg-white-dark text-white-dark dark:text-white cursor-pointer'>
			<img
				src={course.image}
				className='h-8 w-full object-cover rounded-t-xl'></img>
			<h2 className='flex-c-c h-4 pt-1 box-content text-center text-lg font-semibold'>
				{course.title}
			</h2>
			<p className='truncate-3-lines min-h-4_5 my-0_75 px-1_25'>
				{course.description}
			</p>
			<div className='py-0_75 px-1_25 flex-sa-c'>
				{course.compareAtPrice && (
					<p className='line-through text-xl font-semibold'>
						{course.compareAtPrice}€
					</p>
				)}
				<p className='text-3xl font-semibold'>{course.price}€</p>
			</div>
			<div className='pt-0_5 pb-1_25 px-1_25 flex-c-s'>
				<Button kind='cta'>Mas info</Button>
			</div>
		</div>
	</Link>
);
