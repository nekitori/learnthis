import { FC } from 'react';

type CoursePublishedSpanProps = {
	visibility: boolean | undefined;
};

const CoursePublishedSpan: FC<CoursePublishedSpanProps> = ({ visibility }) => {
	if (visibility === undefined) return null;

	const className = `inline-flex px-0_5 py-0_25 text-xs font-semibold rounded-full ${
		visibility ? 'text-white bg-green' : 'text-white bg-red'
	}`;

	const text = visibility ? 'Pública' : 'Privada';

	return <span className={className}>{text}</span>;
};

export default CoursePublishedSpan;
