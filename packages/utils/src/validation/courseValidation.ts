/**
 * Title validation
 *
 * - Number of characters must be between 1 to 80
 * @param value Value to validate
 * @returns Is valid
 */
export const titleValidation = (value: string): boolean => {
	const title = value.trim();
	if (title.length < 1 || title.length > 80) return false;
	return true;
};

/**
 * Description validation
 *
 * - Minimum length 1 character
 * @param value Value to validate
 * @returns Is valid
 */
export const descriptionValidation = (value: string): boolean => {
	const description = value.trim();
	if (description.length < 1) return false;
	return true;
};

/**
 * Price validation
 *
 * - Positive number
 * @param value Value to validate
 * @returns Is valid
 */
export const priceValidation = (value: number): boolean => {
	if (value < 0) return false;
	return true;
};

/**
 * Url validation
 *
 * - Only contains alphanumeric characters and hyphen
 * @param value Value to validate
 * @returns Is valid
 */
export const urlValidation = (value: string): boolean => {
	const url = value.trim();
	const regex = /^[A-Za-z0-9-]+$/g;

	if (!url.match(regex)) return false;
	return true;
};
