import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Course } from './course.gqltype';
import { Section } from './section.gqltype';

/**
 * Graphql type: Course with sections with all its data (for admin purposes).
 */
@ObjectType()
export class CourseWithSections extends Course {
	@Field(type => Int)
	studentsCount: number;
	sections: Section[];
}
