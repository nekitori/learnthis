import { CommonErrors } from '@Common/enums/common-errors.enum';
import { ObjectID } from '@Common/types/object-id.type';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { isValidObjectId } from 'mongoose';

/**
 * Array id's validator
 */
@Injectable()
export class ObjectIDArrayPipe implements PipeTransform {
	/**
	 * Array handler validator
	 *
	 * @param  {ObjectID[]} values Array of ID's courses
	 * @returns ObjectID[]
	 */
	transform(values?: string[]): ObjectID[] {
		values &&
			values.forEach(course => {
				if (!isValidObjectId(course))
					throw new BadRequestException(CommonErrors.FORMAT_OBJECT_ID);
			});

		return values;
	}
}
