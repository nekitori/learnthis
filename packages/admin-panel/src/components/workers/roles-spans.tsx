import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { FC } from 'react';

type RolesSpansProps = {
	roles: WorkerRoles[];
};

const RolesSpans: FC<RolesSpansProps> = ({ roles }) => {
	const classNameBase =
		'px-0_5 py-0_25 px-0_5 py-0_25 text-xs font-semibold rounded-full text-white';

	const classNames = {
		[WorkerRoles.ADMIN]: `${classNameBase} bg-blue`,
		[WorkerRoles.TEACHER]: `${classNameBase} bg-orange`,
	};

	return (
		<>
			{roles.map((i, index) => (
				<span key={index} className={classNames[i]}>
					{i}
				</span>
			))}
		</>
	);
};

export default RolesSpans;
