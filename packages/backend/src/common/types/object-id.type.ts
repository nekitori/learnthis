/** Type to explicitly clarify that the string is a Mongoose objectid */
export type ObjectID = string;
