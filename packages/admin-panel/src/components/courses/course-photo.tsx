import { ComponentProps, FC, SyntheticEvent } from 'react';

interface CoursePhotoProps extends ComponentProps<'img'> {
	photo: string;
	title: string;
}

const CoursePhoto: FC<CoursePhotoProps> = ({ photo, title, ...props }) => (
	<img
		{...props}
		src={
			photo || `https://avatars.dicebear.com/api/initials/${title}-${title}.svg`
		}
		onError={(event: SyntheticEvent<HTMLImageElement, Event>) => {
			event.currentTarget.src = `https://avatars.dicebear.com/api/initials/${title}-${title}.svg`;
		}}
	/>
);

export default CoursePhoto;
