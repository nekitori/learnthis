import { MainPaths } from '@Enums/paths/main-paths.enum';
import { CustomNavLink } from 'src/types/icon-nav-link.type';

export const courseLinks: Array<CustomNavLink> = [
	{
		to: MainPaths.COURSE,
		exact: true,
		text: 'Ficha',
	},
	{
		to: MainPaths.COURSE_DATA,
		exact: true,
		text: 'Datos',
	},
	{
		to: MainPaths.COURSE_SECTIONS,
		exact: true,
		text: 'Secciones',
	},
	{
		to: MainPaths.COURSE_TUTORS,
		exact: true,
		text: 'Tutores',
	},

	{
		to: MainPaths.COURSE_ADVANCED,
		exact: true,
		text: 'Avanzado',
	},
];

export const sectionLinks: Array<CustomNavLink> = [
	{
		to: MainPaths.SECTION,
		exact: true,
		text: 'Ficha',
	},
	{
		to: MainPaths.SECTION_DATA,
		exact: true,
		text: 'Datos',
	},
	{
		to: MainPaths.SECTION_LESSONS,
		exact: true,
		text: 'Lecciones',
	},
	{
		to: MainPaths.SECTION_ADVANCED,
		exact: true,
		text: 'Avanzado',
	},
];

export const lessonLinks: Array<CustomNavLink> = [
	{
		to: MainPaths.LESSON,
		exact: true,
		text: 'Ficha',
	},
	{
		to: MainPaths.LESSON_DATA,
		exact: true,
		text: 'Datos',
	},
	{
		to: MainPaths.LESSON_ADVANCED,
		exact: true,
		text: 'Avanzado',
	},
];
