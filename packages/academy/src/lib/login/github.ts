import { RestEndPoints } from '@Enums/paths/rest-endpoints.enum';
import { Strategy as GitHubStrategy } from 'passport-github2';
import { callbackStrategy } from './utils';

export const ghStrategy = new GitHubStrategy(
	{
		clientID: process.env.GITHUB_CLIENT_ID || '',
		clientSecret: process.env.GITHUB_CLIENT_SECRET || '',
		callbackURL:
			process.env.NEXT_PUBLIC_SITE_URL + RestEndPoints.GITHUB_REDIRECT,
	},
	callbackStrategy
);
