import { ComponentProps, FC } from 'react';
import { ButtonStyles, ButtonType } from '@Enums/generic/button-types.enum';
import Loader from '@Components/other/loader';

export interface ButtonProps extends ComponentProps<'button'> {
	kind: ButtonType;
	loading?: boolean;
}

/**
 * Custom button element with predefined stypes for each button kind.
 *
 * @param props.kind Button predefined kind
 * @param props.classname Additional className
 * @param props.children Child elements
 * @param props.props Other button props
 *
 */
const Button: FC<ButtonProps> = ({
	kind,
	className,
	loading,
	children,
	...props
}) => {
	if (loading) return <Loader />;

	return (
		<button
			className={`${ButtonStyles[kind]} ${className} disabled:bg-gray disabled:border-0 disabled:text-white disabled:cursor-default`}
			{...props}>
			{children}
		</button>
	);
};

export default Button;
