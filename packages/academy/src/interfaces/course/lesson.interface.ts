import { LessonTypes } from '@Enums/courses/lesson-types.enum';

interface PublicLesson {
	__typename: LessonTypes;
	_id: string;
	url: string;
	title: string;
	section: number;
}

export interface VideoLessonPublic extends PublicLesson {
	__typename: LessonTypes.VIDEO;
	duration: number;
}

export interface InfoLessonPublic extends PublicLesson {
	__typename: LessonTypes.INFO;
}
