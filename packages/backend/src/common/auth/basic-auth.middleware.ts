import { CommonErrors } from '@Common/enums/common-errors.enum';
import {
	Injectable,
	NestMiddleware,
	UnauthorizedException,
} from '@nestjs/common';
import { WorkerService } from '@Workers/services/worker.service';
import { compare } from 'bcrypt';
import { NextFunction, Request, Response } from 'express';

@Injectable()
export class BasicAuthMiddleware implements NestMiddleware {
	constructor(private readonly workerService: WorkerService) {}

	async use(req: Request, res: Response, next: NextFunction): Promise<void> {
		const b64auth = (req.headers.authorization || '').split(' ')[1] || '';
		const [username, password] = Buffer.from(b64auth, 'base64')
			.toString()
			.split(':');

		const user = await this.workerService.findByEmail(username);

		if (
			user &&
			(await compare(password, user.password)) !== false &&
			user.active
		) {
			(req as any).user = user;
			return next();
		}

		res.set('WWW-Authenticate', 'Basic realm="Authentication required."');
		throw new UnauthorizedException(CommonErrors.UNAUTHORIZED);
	}
}
