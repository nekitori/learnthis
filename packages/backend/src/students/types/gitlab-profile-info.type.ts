export type GitlabProfileInfo = {
	id: number;
	name: string;
	username: string;
	avatar_url: string;
	bio: string;
	bio_html: string;
	public_email: string;
	website_url: string;
	organization: string;
	email: string;
};
