import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { AuthState } from '@Interfaces/states/browser-context.interface';
import { ShoppingCartState } from '@Interfaces/states/shoppingcart-context.interface';
import { useEffect, useState } from 'react';
import cookie from 'cookie';
import { GraphqlStudent } from 'learnthis-utils';
import { toast } from 'react-toastify';

/**
 * Update shopping cart cookie value.
 * @param value New value
 */
const setCartCookie = (value: string | string[]): void => {
	document.cookie = cookie.serialize(
		process.env.NEXT_PUBLIC_CART_COOKIE_NAME || 'shoppingCart',
		value.toString(),
		{
			path: '/',
		}
	);
};

/**
 * Gets the array of courses stored in the cookie.
 */
const getCoursesFromCookie = (): string[] => {
	const cartCookie = cookie.parse(document.cookie)[
		process.env.NEXT_PUBLIC_CART_COOKIE_NAME || 'shoppingCart'
	];

	return cartCookie ? cartCookie.split(',') : [];
};

/**
 * Hook to handle shopping cart state and its associated side effects.
 * @param stateAuth Auth state
 * @param apolloClient Apollo client
 */
export const useShoppingCart = (
	stateAuth: AuthState,
	apolloClient: ApolloClient<NormalizedCacheObject>
) => {
	const [shoppingCart, setShoppingCart] = useState<ShoppingCartState>({
		courses: stateAuth?.student?.cart || [],
	});

	// Loads user current cart state
	useEffect(() => {
		const cartCookieCourses = getCoursesFromCookie();

		if (stateAuth.student) {
			//Remove courses adquired from the user but gets without session
			const filteredCartCookie = cartCookieCourses.filter(
				cartCourseId =>
					!stateAuth.student?.coursesEnrolled.filter(
						courseEnrolled => courseEnrolled.course === cartCourseId
					).length
			);

			const courseSet = Array.from(
				new Set([...stateAuth.student.cart, ...filteredCartCookie])
			);

			if (courseSet.length > stateAuth.student.cart.length)
				updateDBShoppingCart(courseSet);
			else setShoppingCart({ courses: courseSet });
		} else setShoppingCart({ courses: cartCookieCourses });
	}, [stateAuth]);

	/**
	 * Adds a new course to the shopping cart, if it doesn't exist previously.
	 * @param courseId Course object id
	 */
	const addItemShoppingCart = async (courseId: string) => {
		const newItem = shoppingCart.courses.includes(courseId);

		if (!newItem) {
			const coursesCart = [...shoppingCart.courses, courseId];

			if (!stateAuth?.student) {
				setCartCookie(coursesCart);
				setShoppingCart({ courses: coursesCart });
			} else await updateDBShoppingCart(coursesCart);
		}
	};

	/**
	 * Removes a course from the shopping cart, if it exists previously.
	 * @param courseId Course object id
	 */
	const removeItemShoppingCart = async (courseId: string) => {
		const existingItem = shoppingCart.courses.includes(courseId);

		if (existingItem) {
			const coursesCart = shoppingCart.courses.filter(id => id !== courseId);

			if (!stateAuth?.student) {
				setCartCookie(coursesCart);
				setShoppingCart({ courses: coursesCart });
			} else await updateDBShoppingCart(coursesCart);
		}
	};

	/**
	 * Clear the shopping cart, both in the cookie, and in the database if the user exists.
	 */
	const clearShoppingCart = async () => {
		if (!stateAuth?.student) setCartCookie('');
		else await updateDBShoppingCart([]);
	};

	/**
	 * Updates user cart on database, and clears the cookie if it succeeds.
	 * @param newCart
	 */
	const updateDBShoppingCart = async (newCart: string[]) => {
		try {
			await apolloClient.mutate({
				mutation: GraphqlStudent.student_update_cart,
				variables: {
					courses: newCart,
				},
			});

			setCartCookie('');
			setShoppingCart({ courses: newCart });
		} catch (error) {
			toast.error(error.message);
		}
	};

	return {
		shoppingCart,
		addItemShoppingCart,
		removeItemShoppingCart,
		clearShoppingCart,
	};
};
