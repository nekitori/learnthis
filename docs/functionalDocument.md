## Plataforma Open Source de E-learning

[[_TOC_]]

## 1. Overview del proyecto

### Propósito general de la aplicación

El objetivo del proyecto es la creación de una plataforma para que los creadores de contenido puedan distribuir sus propios cursos de forma totalmente personalizable.

### Resumen global de la plataforma

1. El creador de contenido genera un curso y lo sube a la plataforma en forma de lecciones, cada una de ellas correspondiente a un vídeo, con la opción de añadir materiales adicionales.
2. Los usuarios acceden a la plataforma y consultan el catálogo de cursos disponibles.
3. Los usuarios deciden inscribirse a un curso, bien sea de forma gratuita o de pago, para lo que será necesario que se registren previamente en la plataforma.
4. Los usuarios disponen de un perfil, donde pueden introducir sus datos personales.
5. Los usuarios pueden disfrutar los cursos adquiridos a través de un sistema de navegación por vídeos, donde podrán dejar sus comentarios.
6. Los usuarios podrán dejar su feedback.

### Análisis de las alternativas

En este apartado se analizan las principales alternativas existentes en el mercado, para intentar analizar si alguna de ellas cumple con los requisitos que se esperan de la plataforma, y tratar de valorar si merece la pena el esfuerzo de llevar a cabo el proyecto.

- Los marketplace de cursos como Udemy o Coursera tienen muchos inconvenientes que interfieren con el modelo de negocio y que limitan mucho la capacidad de actuación:
  - Las comisiones por el servicio son demasiado altas.
  - Los precios de los cursos los define la plataforma, no el creador.
  - Los clientes que el creador lleve a la plataforma, no son del creador sino de la plataforma.
  - El marketing de los cursos se delega en gran parte a la plataforma.
- Las empresas que proporcionan tu web de cursos a cambio de una suscripción mensual, tales como Teachable o Hotmart:
  - El uso de las plataformas implica un coste fijo bastante alto.
  - Habitualmente, estas plataformas cobran una comisión por ventas sobre la facturación total del negocio, con lo que se terminan convirtiendo en socios encubiertos de la plataforma, únicamente por prestar un servicio que ya de por sí no es gratuito.
  - A nivel de programación, estas páginas tienen bastantes problemas de rendimiento y fallos de seguridad.
  - A nivel de personalización, estas webs están muy limitadas por la plantilla escogida, y limitan bastante la innovación.
- Adaptar un CMS existente (Wordpress…) para poder utilizarlo como plataforma:
  - La gestión de plantillas de estos CMS añaden gran cantidad de código innecesario y empeoran el rendimiento.
  - La gestión de estos CMS limita mucho a la hora de escoger una tecnología u otra.
  - La personalización de funcionalidades en estos CMS suele implicar una cantidad de trabajo mucho mayor, para obtener un resultado mucho peor.

## 2. Roles de la aplicación

La aplicación distinguirá los siguientes roles:

- **Clientes**: Personas que utilizan la plataforma para disfrutar de los servicios de la misma:
  - **Visitante**: Todo usuario de la página que acceda a la misma y navegue de forma anónima, sin iniciar sesión en la plataforma. Este tipo de usuarios podrán:
    - Crear su cuenta de alumno en la plataforma mediante el formulario de registro.
    - Recuperar contraseña.
    - Consultar el listado de cursos disponibles.
    - Ver los detalles de un curso.
    - Inscribirse / adquirir un curso, en cuyo caso se solicitará el registro durante dicho proceso.
    - Disponer de un formulario de contacto.
    - Consultar páginas legales, FAQ y resto de páginas estáticas.
  - **Alumno**: Usuario registrado en la plataforma y autenticado. Este tipo de usuarios podrán:
    - Todos los casos de uso de visitante.
    - Visualizar su perfil y sus datos personales, y poder modificar y/o completar los mismos.
    - Visualizar el dashboard de los cursos en los que está inscrito (progreso del curso, siguiente lección…)
    - Consumir contenidos de los cursos en los que está inscrito:
      - Consumir los vídeos con un reproductor estándar
      - Poder dejar comentarios en cada vídeo
      - Consultar/descargar los materiales adicionales del curso
    - Dejar su opinión sobre la plataforma o sus sugerencias a futuro
- **Trabajadores**: Personas que utilizan la sección de administración de la plataforma para proveer los servicios a los clientes de la misma. Este tipo de usuarios podran:
  - Deberán ser registrados en la misma por un administrador, no existiendo formulario de registro ni solicitud.
  - Disponer de al menos un rol de trabajador dentro de la plataforma.
  - Iniciar y mantener una sesión en la plataforma.
  - **Administrador**: Usuario responsable de la gestión íntegra de la plataforma:
    - Disponer de una sección donde poder gestionar todos los cursos:
      - Crear, modificar y eliminar el contenido de los cursos.
        - Añadir, modificar o eliminar los materiales adjuntos a cualquier lección del curso.
      - Visualizar y responder comentarios del curso, accediendo de forma sencilla a los no respondidos.
      - Visualizar los alumnos inscritos en el curso, junto con el nivel de progreso de los mismos.
    - Modificar / Eliminar clientes en la plataforma.
      - Acceder a los datos del cliente de la plataforma (datos personales, pedidos…)

## 3. Requisitos funcionales

### Área pública

#### Login y registro de usuarios

- Registro
- Login
- Olvido de contraseña

#### Área personal del alumno

- Mi perfil público
- Datos personales
  - Consultar
  - Modificar
- Mis cursos
- Mis pedidos
- Ajustes
  - Notificaciones
  - Personalización apariencia
  - Privacidad

#### Catálogo de cursos

- Listado de cursos
- Descripción del curso
- Checkout

#### Contacto

### Academia

####

    Dashboard

####

    Curso (vista principal)

- Una sección que incluirá la descripción del curso
- Una sección de progreso del curso, donde el usuario podrá ver el itinerario del curso y su progreso actual.
- Una sección que mostrará las lecciones del curso e indicará el progreso actual del alumno

#### Sección de progreso del curso

- El alumno dispondrá de un menú lateral donde se indicará el progreso actual del curso.
- Dicho menú deberá ser un desplegable, que podrá mostrar u ocultar mediante un click.
- Cabecera del desplegable:
  - Título del curso
  - Porcentaje de completado del curso
  - Barra de progreso del curso, con la parte proporcional al porcentaje rellena
  - El porcentaje se calculará como: Lecciones completadas / lecciones totales
- Cuerpo del desplegable
  - Secciones del curso con su título y un botón para desplegar detalles
  - Las secciones desplegadas mostrarán un listado de los vídeos que la conforman, a modo de enlaces hacia cada uno de los vídeos.
  - Por defecto, la única sección desplegada será la que contenga el vídeo actual.
  - Los enlaces de las secciones estarán precedidos por un icono
    - Gris en caso de lección sin completar
    - Verde en caso de lección completada
- En caso de que el tamaño del contenido del desplegable sea mayor que el de la pantalla, se mostrará como un scroll.

####

    Lección del curso


    La pantalla constará de las siguientes secciones:

- Un reproductor de vídeo, donde el usuario podrá visualizar el vídeo del curso.
- Una sección de comentarios referente a la lección actual
- Una sección de descripción de la lección
  - En la parte inferior del vídeo, se mostrará una sección de descripción del vídeo, que incluirá los materiales adicionales.
  - La sección consistirá en:
    - Título de la lección
    - Descripción de la lección
    - Materiales adicionales
  - Los materiales adicionales pueden ser:
    - Enlaces a páginas externas
    - Archivos o ficheros adicionales
  - Los enlaces externos se presentarán como un enlace con un texto explicativo
  - Los ficheros se mostrarán como un enlace con un icono y un texto explicativo
- Una utilidad para controlar el progreso del curso, que permita marcar como completas las lecciones

#####

    Reproductor de vídeo

- El reproductor deberá disponer de una barra de controles en la parte inferior del vídeo.
  - Esta barra incluirá todos los controles necesarios para manipular el vídeo.
  - Se mostrará vinculada a un evento de hover sobre todo el contenedor del vídeo.
  - En la versión reducida, se ocultará en el momento en que el usuario deje de hacer hover.
  - En la versión de pantalla completa, se ocultará en el momento en que el mouse del usuario permanezca inactivo durante 2 segundos.
- El alumno debe poder reproducir el vídeo o pausarlo:
  - Haciendo click en cualquier parte del vídeo que no tenga otro control específico.
  - Haciendo click en el control play/pausa.
  - Pulsando la tecla espacio.
- El alumno debe poder poner el vídeo en pantalla completa:
  - Pulsando en el control de pantalla completa.
  - Haciendo doble click en cualquier parte del vídeo que no tenga control.
- El alumno debe poder avanzar o retroceder en el vídeo mediante el slider de reproducción:
  - Haciendo click en cualquier parte de la línea temporal del vídeo.
  - Arrastrando el slider y soltándolo en un punto de la línea temporal.
  - Utilizando las flechas izquierda y derecha del teclado para avanzar o retroceder en fracciones de 5 segundos.
- El alumno deberá poder controlar el volumen del vídeo:
  - Utilizando el slider vertical de volumen de los controles.
  - Utilizando las flechas arriba y abajo del teclado para subir o bajar el volumen en fracciones de 5%.
- El alumno deberá poder modificar la velocidad de reproducción del vídeo mediante un control.
  - La velocidad menor será 0.5x
  - La velocidad mayor será 2x
  - La velocidad normal, y por defecto, será 1x
  - Se avanzará en fracciones de 0.25x
- El alumno deberá ver en los controles, el momento actual y la duración total del vídeo.
  - La duración será siempre la misma, no se modificará independientemente de la velocidad de reproducción elegida.
- El minuto de reproducción actual del vídeo deberá guardarse cada 30 segundos, dentro de una propiedad del progreso del alumno en el curso, de forma que éste pueda continuar fácilmente desde donde lo dejó la última vez.
- De forma adicional, se proporcionarán al usuario controles sencillos para manejar su progreso en el curso, debajo de los vídeos.
  - Botón atrás
    - Aparecerá en todos los vídeos del curso, excepto en el primero.
    - Permitirá retroceder a la lección anterior
  - Botón adelante
    - Aparecerá en todos los vídeos del curso, excepto en el último.
    - Permitirá avanzar al siguiente vídeo del curso
  - Botón de completar lección
    - Aparecerá en todos los vídeos del curso
    - Permitirá marcar o desmarcar como completa una lección manualmente.

#####

    Sección de comentarios

- El alumno dispondrá de una sección de comentarios en la parte inferior de la pantalla.
- En primer lugar, encontrará el componente para realizar un comentario:
  - Caja de texto para el comentario
  - Botón de publicar
- El formato admitido para los comentarios será:
  - Texto, de longitud indeterminada
  - Posibilidad de incluir emoticonos unicode (no incluidos en la plataforma)
  - No se podrán incluir enlaces
- Debajo del componente de publicación, se situará un listado del resto de los comentarios realizados sobre el vídeo, con el siguiente formato:
  - Foto de perfil del autor
  - Nombre del autor
  - Fecha de publicación
  - Contador de likes
  - Botón de like
  - Botón de dislike
  - Botón de responder
  - Enlace a las respuestas
- Las acciones de like o dislike:
  - Se podrán deshacer volviendo a pulsar en el mismo botón
  - Serán mutuamente excluyentes
  - Un like / dislike por usuario y comentario
- Las respuestas a los comentarios
  - Podrán ser realizadas por cualquier otro usuario, alumno o instructor.
  - Permanecerán ocultas inicialmente, y serán accesibles mediante un enlace con un desplegable en la parte inferior.
  - El enlace incluirá el número de comentarios (Ver N respuestas).
  - En caso de no haber respuestas, el enlace no aparecerá.
  - Las respuestas se presentarán con el mismo formato que un comentario original, pero incluyendo una ligera tabulación izquierda respecto al original.
- El listado de los comentarios
  - Se presentará en forma vertical
  - El orden será el siguiente:
    - Comentarios realizados por el usuario, cronológicamente
    - Resto de comentarios cronológicamente
  - Inicialmente, se cargará un máximo de 15 comentarios, pudiendo cargar el resto a través de un botón que aparecerá al final del listado.

### Área de administración

#### Trabajador

####

    Instructor

####

    Soporte

####

    Administrador

- Los alumnos deberán poder registrarse en la plataforma a través de una de las siguientes formas:
  - Formulario de registro tradicional
  - Sign In with Google
  - Sign In with Facebook
- Los alumnos registrados deberán poder acceder a la plataforma a través de una de las siguientes opciones:
  - Pantalla de login, utilizando su correo y contraseña
  - Sign In with Google
  - Sign In with Facebook
- Los alumnos deberán poder restablecer su contraseña a través de una de las siguientes opciones:

  - Funcionalidad de recordar contraseña: Se enviará al email del usuario un enlace para poder restablecer su contraseña, si este no es capaz de iniciar sesión.
  - Funcionalidad de cambio de contraseña: Se podrá modificar la contraseña desde el perfil de usuario, habiéndo iniciado sesión previamente, indicando la contraseña anterior y la nueva contraseña.

- El área personal del alumno estará compuesta por las siguientes secciones
  - Datos personales
  - Personalización de la cuenta
  - Pedidos
  - Centro de soporte
  - Facturación
  - Cerrar sesión
- El alumno deberá poder consultar sus datos personales
- El alumno deberá poder modificar sus datos personales
- El alumno deberá poder añadir una o varias direcciones de facturación
- El alumno deberá poder elegir, en caso de que exista más de una, una dirección de facturación como favorita.
  - Por defecto, se seleccionará como favorita la primera de las direcciones introducidas.
- El alumno deberá poder editar o eliminar cualquiera de las direcciones de facturación introducidas.
- El alumno deberá poder modificar su contraseña
  - Se solicitará la contraseña anterior para confirmar este paso
- El alumno deberá poder seleccionar el tema por defecto que desea para su aplicación entre las siguientes opciones:
  - Tema claro
  - Tema oscuro
- El alumno deberá poder elegir las notificaciones que recibirá mediante correo electrónico, indicando qué eventos deben ser notificados y cuáles no.
  - Pendiente de definir los tipos de eventos admitidos.
- El alumno deberá poder seleccionar sus opciones de reproducción por defecto para los vídeos y los cursos.
  - Pendiente de definir las opciones permitidas.
- El alumno deberá poder consultar, a través de un listado, los pedidos que ha realizado en la plataforma.
- En el listado se mostrará una vista resumen de cada uno de los pedidos, junto con una opción para ver los detalles de un pedido concreto.
- En dicho listado se podrá filtrar cada uno de los pedidos en función de:
  - Fecha de pedido
  - Importe del pedido
- El listado contará con un buscador de pedido, cuyo término de búsqueda se utilizará para filtrar el nombre de los productos adquiridos, mostrándose todos aquellos pedidos en los que alguno de los productos coincida.
- Por defecto, el listado se mostrará en orden cronológico.
- En caso de existir múltiples pedidos, el listado se mostrará paginado.
- El listado deberá contener una opción para elegir el número de elementos a mostrar por página.
- El alumno tendrá acceso a una página de preguntas frecuentes (FAQ), donde se indicarán los procedimientos y respuestas más habituales.
- El alumno tendrá la opción de contactar con la plataforma a través de un formulario de contacto para exponer su problema.
- El formulario de contacto permitirá, de forma opcional, referenciar uno de los pedidos realizados por el alumno, para poder concretar el artículo al que hace referencia en su consulta, si lo hubiera.
- El alumno podrá encontrar las formas adicionales de contacto dentro de la plataforma, tales como un acceso directo a Whatsapp, teléfono o correo electrónico.
- El alumno deberá poder solicitar una factura para su pedido en caso de necesitar la misma.

## 4. Requisitos no funcionales

- Autenticación
  - La autenticación se realizará mediante token JWT
- Cifrado
  - Las comunicaciones de la aplicación deben realizarse en base a un cifrado SSL.
- Sistemas de protección de la privacidad de los usuarios
  - Se debe encriptar la contraseña del usuario
- Sistemas anti piratería de los cursos
  - Instalación de un sistema de particionado de vídeo (HLS) para evitar la descarga directa.

## 5. Propuestas a futuro

- MongoDB Injection
- Certificación en la plataforma (xAPI)
- Analíticas de rendimiento de los cursos
- Personalización de la experiencia de usuario (alumnos)
- Obtener información de nuevos cursos, ofertas… (newsletters)
- Comunicación personal instructor-alumno y alumno-alumno (chat ?).
- Gamificación (trofeos, logros, progresos…).
- Cambio de express a fastify.
- Twilio
- Cambiar autenticación para peticiones GraphQL.
