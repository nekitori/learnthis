import gql from 'graphql-tag';

//#region Section

export const course_admin_section_modify = gql`
	mutation course_admin_section_modify(
		$courseId: ID!
		$sectionId: ID!
		$input: SectionModifyDto!
	) {
		course_admin_section_modify(
			courseId: $courseId
			sectionId: $sectionId
			input: $input
		)
	}
`;

export const course_admin_section_sort_lessons = gql`
	mutation course_admin_section_sort_lessons(
		$courseId: ID!
		$sectionId: ID!
		$lessonIdsSorted: [ID!]!
	) {
		course_admin_section_sort_lessons(
			courseId: $courseId
			sectionId: $sectionId
			lessonIdsSorted: $lessonIdsSorted
		)
	}
`;

export const course_admin_section_delete = gql`
	mutation course_admin_section_delete($courseId: ID!, $sectionId: ID!) {
		course_admin_section_delete(courseId: $courseId, sectionId: $sectionId)
	}
`;

//#endregion

//#region Publish

export const course_admin_section_publish = gql`
	mutation course_admin_section_publish($courseId: ID!, $sectionId: ID!) {
		course_admin_section_publish(courseId: $courseId, sectionId: $sectionId)
	}
`;

export const course_admin_section_unpublish = gql`
	mutation course_admin_section_unpublish($courseId: ID!, $sectionId: ID!) {
		course_admin_section_unpublish(courseId: $courseId, sectionId: $sectionId)
	}
`;

//#endregion
