export enum LoginStrategies {
	GOOGLE = 'google',
	GITHUB = 'github',
	GITLAB = 'gitlab',
}

export type LoginStrategy = 'google' | 'github' | 'gitlab';
