import {
	getCurrentSession,
	WithTransaction,
} from '@Common/decorators/transactional';
import { Env } from '@Common/enums/env.enum';
import { IPaginate } from '@Common/interfaces/mongoose.interface';
import { AgendaService } from '@Common/modules/agenda/services/agenda.service';
import { ObjectIdTokenPayload } from '@Common/types/objectid-token-payload.type';
import { FOLDER_UPLOADS } from '@Common/utils/file-upload';
import { CourseErrors } from '@Courses/enums/course-errors.enum';
import { ICourseDoc } from '@Courses/interfaces/course-document.interface';
import { ILessonDoc } from '@Courses/interfaces/lesson-document.interface';
import { CourseService } from '@Courses/services/course.service';
import {
	BadRequestException,
	ConflictException,
	forwardRef,
	Inject,
	Injectable,
	Logger,
	NotFoundException,
	UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { ChangePasswordDto } from '@Students/dto/change-password.dto';
import { LoginDto } from '@Students/dto/login.dto';
import { ModifyProfileDto } from '@Students/dto/modify-profile.dto';
import { RecoverPasswordDto } from '@Students/dto/recover-password.dto';
import { RegisterDto } from '@Students/dto/register.dto';
import { SocialType } from '@Students/enums/social-type.enum';
import { StudentErrors } from '@Students/enums/student-errors.enum';
import { StudentModels } from '@Students/enums/student-models.enum';
import { StudentTokenTypes } from '@Students/enums/student-token-types.enum';
import { LoginOutput } from '@Students/types/login-output.type';
import {
	ICourseEnrolled,
	ICourseProgress,
	ISocialAccount,
	IStudent,
	IStudentDoc,
	IStudentModel,
} from '@Students/interfaces/student-document.interface';
import { compare, hash } from 'bcrypt';
import { promises as fspromises } from 'fs';
import { Model } from 'mongoose';
import { resolve } from 'path';
import { TokenService } from './student-token.service';
import { ObjectID } from '@Common/types/object-id.type';

/**
 * Service to manage database operations on the students table.
 */
@Injectable()
export class StudentsService {
	/**
	 * Dependency injection.
	 * @param studentModel Students mongoose entity
	 * @param courseService Course service
	 * @param configService Config service
	 * @param jwtService JWT service
	 * @param tokenService StudentTokenService
	 * @param mailService MailService
	 */
	constructor(
		@InjectModel(StudentModels.STUDENT)
		private readonly studentModel: Model<IStudentModel>,
		@Inject(forwardRef(() => CourseService))
		private readonly courseService: CourseService,
		private readonly configService: ConfigService,
		private readonly jwtService: JwtService,
		private readonly tokenService: TokenService,
		private readonly agendaService: AgendaService
	) {}

	/**
	 * SALT for password hash
	 */
	private readonly HASH_SALT = 10;

	//#region Find

	/**
	 * Finds all existing students (with pagination).
	 * @param offset Number of elements to skip
	 * @param limit Number of elements to return
	 * @returns Students array paginated
	 */
	async findPaginate(
		offset: number = 0,
		limit: number = 10
	): Promise<IPaginate<IStudentDoc>> {
		return {
			data: (await this.studentModel
				.find()
				.skip(offset)
				.limit(limit)
				.exec()) as IStudentDoc[],
			limit,
			offset,
			total: await this.studentModel.countDocuments().exec(),
		};
	}

	/**
	 * Finds a student by email.
	 * @param email Student email
	 */
	findByEmail(email: string): Promise<IStudentDoc> {
		return this.studentModel
			.findOne({
				email: email.toLowerCase(),
			})
			.session(getCurrentSession())
			.exec() as Promise<IStudentDoc>;
	}

	/**
	 * Finds a student by id.
	 * @param studentId Student ObjectId
	 * @returns Student data
	 */
	findById(studentId: ObjectID): Promise<IStudentDoc> {
		return this.studentModel
			.findById(studentId)
			.session(getCurrentSession())
			.exec() as Promise<IStudentDoc>;
	}

	/**
	 * Finds a student by social id.
	 * @param socialId Student social id
	 * @param type Social type (Github, Gitlab...)
	 * @returns Student data
	 */
	findBySocialAccount(
		socialId: string,
		type: SocialType
	): Promise<IStudentDoc> {
		return this.studentModel
			.findOne({ 'socialAccounts.id': socialId, 'socialAccounts.type': type })
			.exec() as Promise<IStudentDoc>;
	}

	/**
	 * Populates courses progress in a student.
	 * @param student Student data
	 * @returns Student data with course progress
	 */
	populateCourseProgress(
		student: IStudentModel
	): Promise<IStudentDoc<ObjectID, ICourseDoc, ILessonDoc, ObjectID>> {
		return student
			.populate('coursesEnrolled.course')
			.populate('coursesEnrolled.progress.currentLesson')
			.populate('coursesEnrolled.progress.lessons.lesson')
			.execPopulate() as Promise<
			IStudentDoc<ObjectID, ICourseDoc, ILessonDoc, ObjectID>
		>;
	}

	//#endregion

	//#region Social

	/**
	 * Student social sign in.
	 * @param input.socialAccount Social account data
	 * @param input.socialAccount Social account data
	 * @returns JWT token and student data
	 */
	async socialLogin({
		socialAccount,
		studentData,
	}: {
		socialAccount: ISocialAccount;
		studentData: IStudent;
	}): Promise<LoginOutput> {
		let student = await this.findBySocialAccount(
			socialAccount.id,
			socialAccount.type
		);

		if (!student) {
			student = await this.findByEmail(studentData.email);

			if (!student) {
				student = (await this.studentModel.create({
					...studentData,
					socialAccounts: [socialAccount],
					coursesEnrolled: [],
					orders: [],
				})) as IStudentDoc;
			} else {
				student.socialAccounts.push(socialAccount);
				await student.save();
			}
		}

		const payload: ObjectIdTokenPayload = {
			id: student._id.toString(),
		};

		const token = await this.jwtService.signAsync(payload, {
			expiresIn: this.configService.get(Env.STUDENT_TOKEN_EXPIRATION),
			secret: this.configService.get(Env.STUDENT_TOKEN_KEY),
		});

		return {
			token,
			user: {
				_id: student.id,
				...student.toJSON(),
				isSocialLogin: !student.password,
			},
		};
	}

	/**
	 * Links a social account to an student.
	 * @param socialAccount Social account data
	 * @param studentToLink Student data
	 */
	async linkSocialProfile(
		socialAccount: ISocialAccount,
		studentToLink: IStudentDoc
	): Promise<void> {
		let studentLinked = await this.findBySocialAccount(
			socialAccount.id,
			socialAccount.type
		);

		if (!studentLinked) {
			studentToLink.socialAccounts.push(socialAccount);
			await studentToLink.save();
		} else {
			throw new ConflictException(StudentErrors.SOCIAL_IN_USE);
		}
	}

	/**
	 *	Unlink a social account to an student.
	 * @param socialAccount Social account data
	 * @param studentToLink Student data
	 */
	async unlinkSocialProfile(
		socialAccount: ISocialAccount,
		studentToUnlink: IStudentDoc
	): Promise<void> {
		const index = studentToUnlink.socialAccounts.findIndex(
			account =>
				account.id === socialAccount.id && account.type === socialAccount.type
		);

		if (index !== -1) {
			studentToUnlink.socialAccounts.splice(index, 1);
			await studentToUnlink.save();
		} else throw new NotFoundException(StudentErrors.SOCIAL_NOT_FOUND);
	}

	//#endregion

	//#region Student

	/**
	 * Student login.
	 * @param input.email Student email
	 * @param input.password Student password
	 * @returns JWT token and student data
	 */
	async login({ email, password }: LoginDto): Promise<LoginOutput> {
		const student = await this.findByEmail(email);
		if (!student) throw new UnauthorizedException(StudentErrors.INVALID_LOGIN);

		const checkPass = await compare(password, student.password);
		if (!checkPass)
			throw new UnauthorizedException(StudentErrors.INVALID_LOGIN);

		if (!student.active)
			throw new UnauthorizedException(StudentErrors.NOT_ACTIVATED);

		const payload: ObjectIdTokenPayload = {
			id: student._id.toString(),
		};

		const token = await this.jwtService.signAsync(payload, {
			expiresIn: this.configService.get(Env.STUDENT_TOKEN_EXPIRATION),
			secret: this.configService.get(Env.STUDENT_TOKEN_KEY),
		});

		return {
			token,
			user: {
				_id: student.id,
				...student.toJSON(),
				isSocialLogin: !student.password,
			},
		};
	}

	/**
	 * Registers a new student.
	 * @param input.email Student email
	 * @param input.name Student name
	 * @param input.surname Student surname
	 * @param input.password Student password
	 */
	@WithTransaction
	async register({
		email,
		name,
		surname,
		password,
	}: RegisterDto): Promise<void> {
		const existUser = await this.findByEmail(email);
		if (existUser) throw new ConflictException(StudentErrors.EMAIL_IN_USE);

		const hashedPassword = await hash(password, this.HASH_SALT);
		const student = (
			await this.studentModel.create(
				[
					{
						email: email.toLowerCase(),
						name: name,
						surname: surname,
						password: hashedPassword,
						active: false,
						socialAccounts: [],
						cart: [],
						coursesEnrolled: [],
						orders: [],
					},
				],
				{ session: getCurrentSession() }
			)
		)[0];

		const token = await this.tokenService.generateToken(
			student._id.toString(),
			StudentTokenTypes.ACTIVATE_TOKEN
		);

		this.agendaService.enQueueActivationMail(email, name, token);
	}

	/**
	 * Changes student password.
	 * @param student Student data
	 * @param input.oldPassword Old student password
	 * @param input.newPassword New student password
	 */
	async changePassword(
		student: IStudentDoc,
		{ oldPassword, newPassword }: ChangePasswordDto
	): Promise<void> {
		if (!oldPassword) {
			if (student.password)
				throw new BadRequestException(StudentErrors.FORMAT_OLD_PASSWORD);
		} else {
			const checkOldPass = await compare(oldPassword, student.password);
			if (!checkOldPass)
				throw new BadRequestException(StudentErrors.FORMAT_OLD_PASSWORD);
		}

		student.password = await hash(newPassword, this.HASH_SALT);
		await student.save();
	}

	/**
	 * Changes student email.
	 * @param student Student data
	 * @param newEmail New student email
	 * @returns New student email
	 */
	async changeEmail(student: IStudentDoc, newEmail: string): Promise<string> {
		student.email = newEmail;
		await student.save();
		return newEmail;
	}

	/**
	 * Changes student username.
	 * @param student Student data
	 * @param newUsername New student username
	 * @returns New student username
	 */
	async changeUsername(
		student: IStudentDoc,
		newUsername: string
	): Promise<string> {
		student.username = newUsername;
		await student.save();
		return newUsername;
	}

	/**
	 * Modifies student profile data.
	 * @param student Student data
	 * @param input New student data
	 */
	async modifyProfile(
		student: IStudentDoc,
		newData: ModifyProfileDto
	): Promise<IStudentDoc> {
		return await student.set({ ...newData }).save();
	}

	/**
	 * Sets a new student photo.
	 * @param student Student data
	 * @param filename Image path
	 * @returns New student photo path
	 */
	async setPhoto(student: IStudentDoc, filename: string): Promise<string> {
		if (student.photo)
			await fspromises
				.unlink(
					resolve(
						FOLDER_UPLOADS,
						student.photo.replace(
							this.configService.get(Env.SELF_DOMAIN) +
								this.configService.get(Env.UPLOADS_STATICS_PATH) +
								'/',
							''
						)
					)
				)
				.catch(error => Logger.error(error));
		student.photo =
			this.configService.get(Env.SELF_DOMAIN) +
			this.configService.get(Env.UPLOADS_STATICS_PATH) +
			'/' +
			filename;
		await student.save();
		return student.photo;
	}

	//#endregion

	//#region Activate and recover

	/**
	 * Creates a new forgot password token and send it by email.
	 * @param email Student email
	 */
	async createForgotPasswordToken(email: string): Promise<void> {
		const student = await this.findByEmail(email);
		if (!student) throw new NotFoundException(StudentErrors.EMAIL_NOT_FOUND);

		const token = await this.tokenService.generateToken(
			student._id.toString(),
			StudentTokenTypes.RECOVER_TOKEN
		);

		this.agendaService.enQueueForgotPasswordMail(
			student.email,
			student.name,
			token
		);
	}

	/**
	 * Changes student password from a forgot password token.
	 * @param input.token Forgot password token
	 * @param input.newPassword New student password
	 */
	@WithTransaction
	async changeForgotPassword({
		token,
		newPassword,
	}: RecoverPasswordDto): Promise<void> {
		const payload: ObjectIdTokenPayload = await this.tokenService.checkToken(
			token
		);

		const student = await this.findById(payload.id);
		if (!student) throw new NotFoundException(StudentErrors.EMAIL_NOT_FOUND);

		student.password = await hash(newPassword, this.HASH_SALT);

		await this.tokenService.removeToken(token);
		await student.save({ session: getCurrentSession() });
	}

	/**
	 * Activates student account from an activation token.
	 * @param token Activation token
	 */
	@WithTransaction
	async activateAccount(token: string): Promise<void> {
		const payload: ObjectIdTokenPayload = await this.tokenService.checkToken(
			token
		);

		const student = await this.findById(payload.id);
		if (!student) throw new NotFoundException(StudentErrors.EMAIL_NOT_FOUND);

		student.active = true;

		await this.tokenService.removeToken(token);
		await student.save({ session: getCurrentSession() });
	}

	//#endregion

	//#region Courses

	/**
	 * Gets student progress for a specific course.
	 * @param student Student data
	 * @param courseUrl Course url
	 * @returns Course progress populated
	 */
	async getCourseProgress(
		student: IStudentDoc,
		courseUrl: string
	): Promise<ICourseProgress<ILessonDoc>> {
		const studentPopulated = await this.populateCourseProgress(student);

		const courseProgress = studentPopulated.coursesEnrolled.find(
			(courseProgress: ICourseEnrolled<ICourseDoc, ILessonDoc>) =>
				courseProgress.course.url === courseUrl
		);

		if (!courseProgress)
			throw new NotFoundException(StudentErrors.STUDENT_NOT_ENROLLED);

		return courseProgress.progress;
	}

	/**
	 * Update student cart.
	 * @param student Student data
	 * @param courses Cart courses
	 */
	async updateCart(student: IStudentDoc, courses: ObjectID[]): Promise<void> {
		for (let i = 0; i < courses.length; i++) {
			const course = await this.courseService.publicFindById(courses[i]);
			if (!course) throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);
		}
		student.cart = courses;

		await student.save();
	}

	//#endregion
}
