import Button from '@Components/generic/form/button/button';
import CloseIcon from '@Components/icons/menu/close-icon';
import { ResolutionBreakpointValues } from '@Config/resolution.settings';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { menuController } from '@ionic/core';
import { IonMenu, IonSplitPane } from '@ionic/react';
import { AuthContext } from '@Lib/context/auth.context';
import { FC, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { LogoTextIcon } from '../icons/logo/logo-text-icon';
import ClientsIcon from '../icons/menu/clients-icon';
import CoursesIcon from '../icons/menu/courses-icon';
import HomeIcon from '../icons/menu/home-icon';
import OrdersIcon from '../icons/menu/orders-icon';
import UsersIcon from '../icons/menu/users-icon';
import Header from './header';
import MenuItem from './menu-item';

/**
 * Component to render a page, add the side menu (both on desktop and mobile) and add app header.
 * Side menu content is based on the user's role, with only the role's options available.
 * @param props.children App routes
 */
const PageWithMenu: FC = ({ children }) => {
	const history = useHistory<string>();
	const { authState, logout } = useContext(AuthContext);

	useEffect(() => {
		const unsubscribe = history.listen(async () => {
			(await menuController.isOpen('menuMob')) &&
				menuController.close('menuMob');
		});
		return () => unsubscribe();
	}, []);

	const isAdmin = authState.worker?.roles.includes(WorkerRoles.ADMIN);

	return (
		<IonSplitPane
			contentId='main'
			when={`(min-width: ${ResolutionBreakpointValues.MD + 1}px)`}>
			<IonMenu contentId='main' menuId='menuMob'>
				<div className='h-screen bg-white dark:bg-white-dark'>
					<div className='mdlg:hidden w-full flex-e-c px-0_5 pt-0_5 mb--1'>
						<CloseIcon
							className='w-2_5 h-2_5 text-primary'
							onClick={() => menuController.close('menuMob')}
						/>
					</div>

					<div className='h-6 flex-s-c'>
						<LogoTextIcon className='h-3 ml-1_5' />
					</div>

					<div className='w-full px-1 smmd:px-1_75'>
						<MenuItem
							title='Dashboard'
							href={MainPaths.DASHBOARD}
							icon={HomeIcon}
						/>
						<MenuItem
							title='Cursos'
							href={MainPaths.COURSES_LIST}
							icon={CoursesIcon}
						/>
						{isAdmin && (
							<>
								<MenuItem
									title='Trabajadores'
									href={MainPaths.WORKERS_LIST}
									icon={UsersIcon}
								/>
								<MenuItem
									title='Pedidos'
									href={MainPaths.ORDERS}
									icon={OrdersIcon}
								/>
								<MenuItem
									title='Clientes'
									href={MainPaths.CLIENTS_LIST}
									icon={ClientsIcon}
								/>
							</>
						)}
					</div>
				</div>

				<div className='w-full text-center pb-1_5 bg-white dark:bg-white-dark'>
					<p className='text-gray mb-0_5'>Provisional</p>
					<Button kind='primary' onClick={logout}>
						Logout
					</Button>
				</div>
			</IonMenu>
			<div id='main' className='page border-gray-light dark:border-gray-dark'>
				<Header />
				<div className='page-content h-full w-full overflow-y-auto bg-background dark:bg-background-dark'>
					{children}
				</div>
			</div>
		</IonSplitPane>
	);
};

export default PageWithMenu;
