import { trimAllStrings } from '@Common/utils/clean-object';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { LoginDto } from '@Students/dto/login.dto';
import { WorkerErrors } from '@Workers/enums/worker-errors.enum';
import { FormValidation } from 'learnthis-utils';

@Injectable()
export class WorkerLoginPipe implements PipeTransform {
	transform(value: LoginDto): LoginDto {
		if (
			!FormValidation.emailValidation(value.email) ||
			!FormValidation.passwordValidation(value.password)
		)
			throw new BadRequestException(WorkerErrors.INVALID_LOGIN);

		trimAllStrings(value);

		return value;
	}
}
