import Paginated from '@Common/gql-types/paginate-filter.gqltypes';
import { ObjectType } from '@nestjs/graphql';
import { Student } from './student.gqltype';

/**
 * Graphql type: Paginated list of students
 */
@ObjectType()
export class StudentPaginated extends Paginated<Student>(Student) {}
