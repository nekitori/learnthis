import Pagination from '@Components/generic/pagination';
import Loader from '@Components/other/loader';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { IStudent } from '@Interfaces/client.interface';
import { Dispatch, FC, SetStateAction } from 'react';
import { useHistory } from 'react-router-dom';
import ClientRow from './client-row';

type ClientsTableProps = {
	clients: IStudent[] | null;
	totalPages: number | null;
	page: number;
	setPage: Dispatch<SetStateAction<number>>;
};

const ClientsTable: FC<ClientsTableProps> = ({
	clients,
	totalPages,
	page,
	setPage,
}) => {
	const history = useHistory<string>();

	return (
		<div className='font-semibold text-sm'>
			<div
				key={-1}
				className='flex-c-c xssm:hidden border-b border-gray-light dark:border-gray-dark pb-0_75'>
				<div className='xssm:w-full mdlg:w-3/6 flex-s-c px-1_5'>
					<span className='text-white-dark dark:text-white'>ID</span>
				</div>
				<div className='xssm:w-full mdlg:w-1/6 text-center'>
					<span className='text-white-dark dark:text-white'>PEDIDOS</span>
				</div>
				<div className='xssm:w-full mdlg:w-1/6 text-center'>
					<span className='text-white-dark dark:text-white'>CURSOS</span>
				</div>
				<div className='xssm:w-full mdlg:w-1/6 text-center'>
					<span className='text-white-dark dark:text-white'>STATUS</span>
				</div>
			</div>
			{clients && totalPages ? (
				<>
					{clients.map((i, index) => (
						<ClientRow key={index} client={i} />
					))}
					<Pagination
						current={page}
						total={totalPages}
						onPageChange={e => {
							setPage(e);
							history.push(MainPaths.WORKERS_LIST + '?page=' + e);
						}}
					/>
				</>
			) : (
				<Loader />
			)}
		</div>
	);
};

export default ClientsTable;
