import { BadRequestException } from '@nestjs/common';
import { RecoverPasswordPipe } from './recover-password.pipe';
import { RecoverPasswordDto } from '../dto/recover-password.dto';

describe('RecoverPasswordPipe', () => {
	let recoverPasswordPipe: RecoverPasswordPipe;

	beforeAll(() => (recoverPasswordPipe = new RecoverPasswordPipe()));

	it('Todo correcto', () => {
		const recoverPassword: RecoverPasswordDto = {
			newPassword: 'asd123ASD.',
			token:
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
		};
		expect(recoverPasswordPipe.transform(recoverPassword)).toBe(
			recoverPassword
		);
	});

	it('Contraseña incorrecta', () => {
		const recoverPassword: RecoverPasswordDto = {
			newPassword: 'ásd123',
			token:
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
		};
		expect(() => recoverPasswordPipe.transform(recoverPassword)).toThrow(
			BadRequestException
		);
	});

	it('Token incorrecto', () => {
		const recoverPassword: RecoverPasswordDto = {
			newPassword: 'asd123ASD.',
			token:
				'eyJhbR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
		};
		expect(() => recoverPasswordPipe.transform(recoverPassword)).toThrow(
			BadRequestException
		);
	});
});
