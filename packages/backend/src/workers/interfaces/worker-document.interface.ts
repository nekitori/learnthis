import { ObjectID } from '@Common/types/object-id.type';
import { ICourseDoc } from '@Courses/interfaces/course-document.interface';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import { Document } from 'mongoose';

export type CoursesOrObjectId = ICourseDoc | ObjectID;

export type IWorkerModel = IWorkerDoc<CoursesOrObjectId>;

export interface IWorker<T extends CoursesOrObjectId = ObjectID> {
	email: string;
	name: string;
	surname: string;
	password?: string;
	active: boolean;
	displayName: string;
	phone?: string;
	username?: string;
	photo?: string;
	bio?: string;
	roles: WorkerRoles[];
	teaches: T[];
}

export interface IWorkerDoc<T extends CoursesOrObjectId = ObjectID>
	extends IWorker<T>,
		Document {}
