import SectionAdvanced from '@Components/courses/section/section-advanced';
import Loader from '@Components/other/loader';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { withAuth } from '@Lib/hoc/withAuth';
import { SectionWrapperProps } from '@Types/props/with-section-props.type';
import { FC } from 'react';

const PageSectionAdvanced: FC<SectionWrapperProps> = ({
	course,
	section,
	sectionId,
	refetch,
}) => {
	return section && course ? (
		<SectionAdvanced
			course={course}
			sectionId={sectionId}
			section={section}
			refetch={refetch}
		/>
	) : (
		<Loader />
	);
};

export default withAuth(PageSectionAdvanced, [WorkerRoles.ADMIN]);
