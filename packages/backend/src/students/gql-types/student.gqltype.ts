import { Gender } from '@Common/enums/gender.enum';
import { ObjectID } from '@Common/types/object-id.type';
import { ObjectType } from '@nestjs/graphql';
import { SocialType } from '@Students/enums/social-type.enum';
import { ISocialAccount } from '@Students/interfaces/student-document.interface';

/**
 * Graphql type: Student
 */
@ObjectType()
export class Student {
	_id: string;
	email: string;
	active?: boolean;
	name: string;
	surname: string;
	photo?: string;
	bio?: string;
	username?: string;
	gender?: Gender;
	birthDate?: Date;
	socialAccounts?: SocialAccount[];
	cart?: ObjectID[];
	coursesEnrolled?: CourseEnroll[];
	orders?: ObjectID[];
}

/**
 * Graphql type: Social account
 */
@ObjectType()
class SocialAccount implements ISocialAccount {
	id: string;
	type: SocialType;
}

@ObjectType()
class CourseEnroll {
	course: ObjectID;
}
