import { CommonErrors } from '@Common/enums/common-errors.enum';
import { Env } from '@Common/enums/env.enum';
import { ImageTypes } from '@Common/enums/image-types.enum';
import { BadRequestException } from '@nestjs/common';
import { IStudentDoc } from '@Students/interfaces/student-document.interface';
import { IWorkerDoc } from '@Workers/interfaces/worker-document.interface';
import { config } from 'dotenv';
import { Request } from 'express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';

config();

export const FOLDER_UPLOADS = join(
	__dirname,
	`../../..${process.env[Env.UPLOADS_STATICS_PATH]}`
);
export const FOLDER_VIDEOS = join(
	__dirname,
	`../../..${process.env[Env.VIDEO_PATH]}`
);

/**
 * Filter upload image by file extension function
 *
 * @param  {Request} _req
 * @param  {any} file
 * @param  {(error:Error,acceptFile:boolean)=>void} cb
 */
export const fileFilter = (
	_req: Request,
	file: any,
	cb: (error: Error, acceptFile: boolean) => void
) => {
	const regExpImg = new RegExp(
		'\\.(' + Object.values(ImageTypes).join('|') + ')$'
	);
	if (!file.originalname.toLocaleLowerCase().match(regExpImg)) {
		return cb(new BadRequestException(CommonErrors.FILE_NOT_ALLOWED), false);
	}
	cb(null, true);
};

/**
 * Multer Storage config
 */
export const imageStorage = diskStorage({
	destination: FOLDER_UPLOADS,
	filename: (req, file, callback) => {
		const fileExtName = extname(file.originalname);
		callback(
			null,
			`student-${(req['user'] as IStudentDoc)._id}-${Date.now()}${fileExtName}`
		);
	},
});

export const imageCourse = diskStorage({
	destination: FOLDER_UPLOADS,
	filename: (req, file, callback) => {
		const fileExtName = extname(file.originalname);
		callback(null, `course-${req.params.id}-${Date.now()}${fileExtName}`);
	},
});

export const imageWorker = diskStorage({
	destination: FOLDER_UPLOADS,
	filename: (req, file, callback) => {
		const fileExtName = extname(file.originalname);
		callback(
			null,
			`worker-${
				req['user'] ? (req['user'] as IWorkerDoc)._id : req.params.id
			}-${Date.now()}${fileExtName}`
		);
	},
});

export const videoStorage = diskStorage({
	destination: FOLDER_VIDEOS,
	filename: (req, file, callback) => {
		callback(null, file.originalname);
	},
});
