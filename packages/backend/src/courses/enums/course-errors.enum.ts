export enum CourseErrors {
	COURSE_ALREADY_EXISTS = 'Ya existe un curso con esa URL',
	COURSE_NOT_FOUND = 'Curso no encontrado',
	COURSE_ALREADY_PUBLISHED = 'El curso ya es público',
	COURSE_ALREADY_PRIVATE = 'El curso ya es privado',
	COURSE_DONT_HAVE_SECTIONS = 'El curso no tiene secciones',
	COURSE_COMPARE_AT_LESS_THAN_PRICE = 'El precio anterior no puede ser menor que el actual',
	COURSE_CANNOT_DELETE_WITH_SECTIONS = 'Antes de eliminar un curso, es necesario eliminar todas sus secciones',
	COURSE_CANNOT_DELETE_WHEN_PUBLIC = 'No se puede eliminar un curso público',
	COURSE_CANNOT_PUBLISH_WITHOUT_PUBLIC_SECTIONS = 'No se puede publicar un curso sin secciones',
	COURSE_CANNOT_PUBLISH_WITH_PUBLIC_SECTION_WITHOUT_LESSONS = 'No se puede publicar un curso con secciones públicas sin lecciones',
	COURSE_CANNOT_UNPUBLISH_WITH_STUDENTS = 'No se puede despublicar un curso con alumnos',

	SECTION_NOT_FOUND = 'Sección no encontrada',
	SECTION_ALREADY_PUBLISHED = 'La sección ya es pública',
	SECTION_ALREADY_PRIVATE = 'La sección ya es privada',
	SECTION_INVALID_REORDER = 'Las secciones a reordenar no coinciden',
	SECTION_CANNOT_DELETE_WITH_LESSONS = 'La sección con leciones no puede ser borrada',
	SECTION_CANNOT_DELETE_PUBLIC = 'No se puede eliminar una sección pública',
	SECTION_CANNOT_PUBLISH_WITHOUT_LESSONS = 'No se puede publicar una sección sin lecciones',
	SECTION_CANNOT_UNPUBLISH_ALL_WHEN_PUBLIC_COURSE = 'No se pueden despublicar todas las lecciones de un curso público',

	LESSON_INVALID_REORDER = 'Las lecciones a reordenar no coinciden',
	LESSON_NOT_FOUND = 'Lección no encontrada',

	FORMAT_COURSE_PRICE = 'Formato de precio inválido',
	FORMAT_COURSE_COMPARE_AT = 'Formato de precio anterior inválido',

	WORKER_ALREADY_TUTOR = 'El trabajador ya es tutor de este curso',
	WORKER_NOT_TUTOR = 'El trabajador no es tutor de este curso',
}
