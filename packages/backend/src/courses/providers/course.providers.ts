import { CourseResolver } from '@Courses/resolvers/course.resolver';
import { CourseService } from '@Courses/services/course.service';
import { Provider } from '@nestjs/common';

export const courseProviders: Provider[] = [CourseResolver, CourseService];
