import { QueryResult, useQuery } from '@apollo/client';
import CheckoutItems from '@Components/checkout/checkout-items';
import CheckoutSummary from '@Components/checkout/checkout-summary';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { RedirectConditions } from '@Enums/redirect-conditions.enum';
import { GSSProps } from '@Interfaces/props/gss-props.interface';
import { IRedirect } from '@Interfaces/redirect.interface';
import { ShoppingCartState } from '@Interfaces/states/shoppingcart-context.interface';
import { createApolloClient } from '@Lib/apollo/apollo-client';
import { ShoppingCartContext } from '@Lib/context/shoppingcart.context';
import { withCSRRedirect } from '@Lib/hoc/with-csr-redirect.hoc';
import { getJwtFromCookie } from '@Lib/login/jwt-cookie.utils';
import {
	isRequestSSR,
	loadAuthProps,
	serverRedirect,
} from '@Lib/utils/ssr.utils';
import { getThemeFromCookie } from '@Lib/utils/theme.utils';
import { decode } from 'jsonwebtoken';
import { GraphqlCourse } from 'learnthis-utils';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { FC, useContext } from 'react';

export type CheckoutProps = {};

const Checkout: FC = () => {
	const {
		shoppingCart,
		removeItemShoppingCart,
		clearShoppingCart,
	} = useContext(ShoppingCartContext);
	const coursesInfo = getCoursesInfo(shoppingCart);

	return (
		<div className='container-xl flex-s-s flex-wrap py-2'>
			<div className='mdlg:w-8/12 xssm:w-full px-1 my-1'>
				<div className='flexcol-c-c px-1_25 py-1_5 bg-white dark:bg-white-dark text-white-dark dark:text-white rounded-lg shadow-lg'>
					<CheckoutItems
						coursesInfo={coursesInfo}
						removeItemShoppingCart={removeItemShoppingCart}
					/>
				</div>
			</div>
			<div className='mdlg:w-4/12 xssm:w-full px-1 my-1'>
				<CheckoutSummary
					coursesInfo={coursesInfo}
					clearShoppingCart={clearShoppingCart}
				/>
			</div>
		</div>
	);
};

const getCoursesInfo = (shoppingCart: ShoppingCartState): QueryResult => {
	const query = GraphqlCourse.course_public_find_by_url_array;

	const response = useQuery(query, {
		variables: { coursesUrls: shoppingCart.courses },
	});

	return response;
};

const redirect: IRedirect = {
	href: MainPaths.LOGIN,
	statusCode: 302,
	condition: RedirectConditions.REDIRECT_WHEN_USER_NOT_EXISTS,
	query: { returnTo: MainPaths.CHECKOUT },
};

export const getServerSideProps: GetServerSideProps = async (
	ctx: GetServerSidePropsContext
) => {
	const props: GSSProps = { lostAuth: false };
	const isSSR = isRequestSSR(ctx.req.url);

	const jwt = getJwtFromCookie(ctx.req.headers.cookie);

	if (jwt) {
		if (isSSR) {
			const apolloClient = createApolloClient();
			const authProps = await loadAuthProps(ctx.res, jwt, apolloClient);

			props.apolloCache = apolloClient.cache.extract();

			if (!authProps) serverRedirect(ctx.res, redirect);
			else {
				props.authProps = authProps;
				await apolloClient.query({
					query: GraphqlCourse.course_public_find_by_url_array,
					variables: { coursesUrls: authProps.student?.cart || [] },
				});
				//TODO error
				props.apolloCache = apolloClient.cache.extract();
			}
		} else if (!decode(jwt)) props.lostAuth = true;
	}

	props.componentProps = {
		shouldRender: !!props.authProps,
		theme: getThemeFromCookie(ctx.req.headers.cookie),
	};

	return { props };
};

export default withCSRRedirect(Checkout, redirect);
