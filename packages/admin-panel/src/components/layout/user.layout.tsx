import BlankCard from '@Components/generic/cards/blank-card';
import Loader from '@Components/other/loader';
import ProfileNav from '@Components/workers/profile-nav';
import { FC } from 'react';
import SectionLayout from './section.layout';

type UserLayoutProps = {
	workerId: string | undefined;
};

const UserLayout: FC<UserLayoutProps> = ({ workerId, children }) => (
	<SectionLayout title='Usuarios'>
		{workerId ? <ProfileNav workerId={workerId} /> : <Loader />}
		<BlankCard className='mx-auto min-w-32 xssm:w-full'>{children}</BlankCard>
	</SectionLayout>
);

export default UserLayout;
