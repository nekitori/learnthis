import { EyeIcon } from '@Components/icons/generic/eye-icon';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { IWorker } from '@Interfaces/worker.interface';
import { FC } from 'react';
import { Link } from 'react-router-dom';
import ActiveSpan from '../active-span';
import RolesSpans from '../roles-spans';
import UserPhoto from '../user-photo';

type WorkerRowProps = {
	worker: IWorker;
};

/**
 * Worker table row.
 * @param props.worker Worker data
 */
const WorkerRow: FC<WorkerRowProps> = ({ worker }) => (
	<div className='flex-c-c flex-wrap border-b border-gray-light dark:border-gray-dark py-0_5'>
		<div className='w-full mdlg:w-3/12 xssm:pb-1 flex-s-c px-1_5'>
			<UserPhoto
				className='h-2_25 w-2_25 rounded-full'
				name={worker.name}
				photo={worker.photo}
				surname={worker.surname}
			/>
			<div className='flexcol-c-s ml-1'>
				<div className='flex-c-c mdlg:flexcol-c-s'>
					<span className='text-white-dark dark:text-white'>{worker.name}</span>
					<span className='text-white-dark dark:text-white xssm:ml-0_5'>
						{worker.surname}
					</span>
				</div>
				<span className='mdlg:hidden text-gray'>{worker.email}</span>
			</div>
		</div>
		<div className='w-4/12 xssm:hidden xssm:text-center xssm:py-0_75'>
			<span className='text-white-dark dark:text-white'>{worker.email}</span>
		</div>
		<div className='w-1/3 mdlg:w-2/12 text-center'>
			<ActiveSpan active={worker.active} />
		</div>
		<div className='w-1/3 mdlg:w-2/12 text-center'>
			<RolesSpans roles={worker.roles} />
		</div>
		<div className='w-1/3 mdlg:w-1/12 flex-c-c'>
			<Link to={MainPaths.WORKERS.replace(MainParams.WORKER_ID, worker._id)}>
				<div className='h-2 w-2 flex-c-c rounded-full text-white bg-primary'>
					<EyeIcon className='h-1_5 w-1_5' />
				</div>
			</Link>
		</div>
	</div>
);

export default WorkerRow;
