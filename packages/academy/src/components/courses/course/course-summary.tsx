import { PublicSection } from '@Interfaces/course/course.interface';
import { FC, useState } from 'react';
import SectionCourse from './course-section';

export type CourseTypeProps = {
	sections: PublicSection[];
};

const CourseSummary: FC<CourseTypeProps> = ({ sections }) => {
	const [sectionState, setSectionState] = useState(-1);

	return (
		<div className='container-xl'>
			<div className='w-full py-3'>
				<h2 className='text-3xl font-semibold text-center text-white-dark dark:text-white'>
					Resumen del curso
				</h2>
				<div className='container-sm px-1 py-2'>
					{sections.map((section, index) => (
						<SectionCourse
							key={index}
							section={section}
							index={index}
							sectionState={sectionState}
							setSectionState={setSectionState}
						/>
					))}
				</div>
			</div>
		</div>
	);
};

export default CourseSummary;
