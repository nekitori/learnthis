import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { AuthProps } from '@Interfaces/props/gss-props.interface';
import { StudentProfile } from '@Interfaces/student/student.interface';
import { createApolloClient } from '@Lib/apollo/apollo-client';
import { GraphqlStudent } from 'learnthis-utils';

export const loadCurrentUserSSR = async (
	jwt: string,
	apolloClient: ApolloClient<NormalizedCacheObject>,
	loadEntireUser: boolean = false
): Promise<AuthProps | undefined> => {
	const query = loadEntireUser
		? GraphqlStudent.student_profile
		: GraphqlStudent.student_profile_min;

	const response = await apolloClient.query({
		query,
		context: {
			headers: { Authorization: `Bearer ${jwt}` },
		},
	});

	const student = response.data.student_profile as StudentProfile;

	if (!student) return;

	const {
		email,
		name,
		surname,
		username,
		photo,
		cart,
		coursesEnrolled,
	} = student;

	const authProps: AuthProps = {
		student: {
			email,
			name,
			surname,
			username,
			photo,
			cart,
			coursesEnrolled,
		},
		jwt,
	};

	return authProps;
};

export const checkActivationToken = async (token: string | string[]) => {
	const apolloClient = createApolloClient();

	try {
		if (typeof token === 'string') {
			const response = await apolloClient.mutate({
				mutation: GraphqlStudent.student_activate_account,
				variables: { token },
			});
			if (response?.data?.student_activate_account) return true;
		}
		return false;
	} catch (e) {
		return false;
	}
};

export const checkRecoverToken = async (token: string | string[]) => {
	const apolloClient = createApolloClient();

	try {
		if (typeof token === 'string') {
			const response = await apolloClient.mutate({
				mutation: GraphqlStudent.student_valid_forgot_password_token,
				variables: { token },
			});
			if (response?.data?.student_valid_forgot_password_token) return true;
		}
		return false;
	} catch (e) {
		return false;
	}
};
