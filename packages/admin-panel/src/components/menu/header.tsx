import MenuIcon from '@Components/icons/menu/menu-icon';
import { AuthContext } from '@Lib/context/auth.context';
import { FC, SyntheticEvent, useContext } from 'react';
import { menuController } from '@ionic/core';
import { useToggleDarkMode } from '@Lib/hooks/useToggleDarkMode';
import SwitchDarkMode from '@Components/generic/switch-dark-mode';

const Header: FC = () => {
	const { authState } = useContext(AuthContext);
	const { hasActiveDarkMode, toggleDarkMode } = useToggleDarkMode();
	return (
		<header className='appBar h-4 shadow-md bg-white dark:bg-white-dark'>
			<div className='container-xl flex-c-c px-1 h-4'>
				<div className='flex-s-c flex-1'>
					<MenuIcon
						className='lg:hidden h-1_75 fill-current cursor-pointer stroke-primary'
						onClick={() => menuController.toggle('menuMob')}
					/>
				</div>
				<div className='relative flex-e-c flex-1 h-full text-white-dark dark:text-white'>
					<SwitchDarkMode
						checked={hasActiveDarkMode}
						onChange={toggleDarkMode}
					/>
					{authState.worker && (
						<>
							<span className='xssm:hidden'>{authState.worker.name}</span>
							<div className='h-full px-0_5 flex-c-c'>
								<img
									className='rounded-full h-2 w-2 overflow-hidden'
									src={
										authState.worker.photo ||
										`https://avatars.dicebear.com/api/initials/${authState.worker.name}-${authState.worker.surname}.svg`
									}
									onError={(event: SyntheticEvent<HTMLImageElement, Event>) => {
										authState.worker &&
											(event.currentTarget.src = `https://avatars.dicebear.com/api/initials/${authState.worker.name}-${authState.worker.surname}.svg`);
									}}
									alt='Foto de perfil'
								/>
							</div>
						</>
					)}
				</div>
			</div>
		</header>
	);
};

export default Header;
