import { FC } from 'react';

const LoginLayout: FC = ({ children }) => (
	<>
		<div className='absolute'>
			<div id='stars'></div>
			<div id='stars2'></div>
			<div id='stars3'></div>
		</div>
		{children}
	</>
);

export default LoginLayout;
