export enum AccountSocial {
	GOOGLE_LINK = 'google-link',
	GITHUB_LINK = 'github-link',
	GITLAB_LINK = 'gitlab-link',
	GOOGLE_UNLINK = 'google-unlink',
	GITHUB_UNLINK = 'github-unlink',
	GITLAB_UNLINK = 'gitlab-unlink',
}

export type AccountSocialType =
	| 'google-link'
	| 'github-link'
	| 'gitlab-link'
	| 'google-unlink'
	| 'github-unlink'
	| 'gitlab-unlink';
