export enum ButtonTypes {
	PRIMARY = 'primary',
	CTA = 'cta',
	RED = 'red',
	GREEN = 'green',
	BORDER = 'border',
	BORDER_RED = 'border-red',
}

const baseStyles = 'px-1_75 py-0_5 rounded-md';

export const ButtonStyles: { [key in ButtonType]: string } = {
	[ButtonTypes.PRIMARY]: `${baseStyles} text-white bg-primary hover:bg-primary-hover`,
	[ButtonTypes.BORDER]: `${baseStyles} text-primary border border-primary`,
	[ButtonTypes.BORDER_RED]: `${baseStyles} text-red border border-red`,
	[ButtonTypes.CTA]: `${baseStyles} text-white bg-cta hover:bg-cta-hover`,
	[ButtonTypes.RED]: `${baseStyles} text-white bg-red hover:bg-red-hover`,
	[ButtonTypes.GREEN]: `${baseStyles} text-white bg-green hover:bg-green-hover`,
};

export type ButtonType =
	| 'primary'
	| 'red'
	| 'green'
	| 'cta'
	| 'border'
	| 'border-red';
