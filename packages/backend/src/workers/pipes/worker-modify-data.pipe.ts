import { cleanObject, trimAllStrings } from '@Common/utils/clean-object';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { WorkerModifyDataDto } from '@Workers/dto/worker-modify-data.dto';
import { WorkerErrors } from '@Workers/enums/worker-errors.enum';
import { FormValidation } from 'learnthis-utils';

@Injectable()
export class WorkerModifyDataPipe implements PipeTransform {
	transform(value: WorkerModifyDataDto): WorkerModifyDataDto {
		if (!Object.keys(value).length)
			throw new BadRequestException(WorkerErrors.NOTHING_TO_MODIFY);

		const { displayName, email, name, surname, bio, phone, username } = value;

		const errors = [];

		if (displayName && !FormValidation.nameValidation(displayName))
			errors.push(WorkerErrors.FORMAT_DISPLAYNAME);

		if (email && !FormValidation.emailValidation(email))
			errors.push(WorkerErrors.FORMAT_EMAIL);

		if (name && !FormValidation.nameValidation(name))
			errors.push(WorkerErrors.FORMAT_NAME);

		if (surname && !FormValidation.nameValidation(surname))
			errors.push(WorkerErrors.FORMAT_SURNAME);

		if (
			typeof bio === 'string' &&
			bio !== '' &&
			!FormValidation.bioValidation(bio)
		)
			errors.push(WorkerErrors.FORMAT_BIO);

		if (
			typeof username === 'string' &&
			username !== '' &&
			!FormValidation.usernameValidation(username)
		)
			errors.push(WorkerErrors.FORMAT_USERNAME);

		if (
			typeof phone === 'string' &&
			phone !== '' &&
			!FormValidation.phoneValidation(phone)
		)
			errors.push(WorkerErrors.FORMAT_PHONE);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		cleanObject(value);
		trimAllStrings(value);

		return value;
	}
}
