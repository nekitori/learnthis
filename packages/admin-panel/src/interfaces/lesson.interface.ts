import { LessonTypes } from '@Enums/course/lesson-types.enum';

export interface ILesson {
	_id: string;
	__typename: LessonTypes;
	url: string;
	title: string;
	description: string;
	visibility: boolean;
	section: number;
	comments: string[];
	duration: number;
	resources: {
		links: string[];
		files: string[];
	};
}

export interface ILessonVideo extends ILesson {
	videoSrc?: string;
	duration: number;
}

export interface ILessonInfo extends ILesson {
	content?: string;
}
