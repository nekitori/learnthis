import { ButtonStyles, ButtonType } from '@Enums/generic/button-types.enum';
import { FC } from 'react';
import { Link, LinkProps } from 'react-router-dom';

export interface LinkButtonProps extends LinkProps {
	kind: ButtonType;
}

/**
 * Custom link element with button appearence with predefined stypes for each link kind.
 *
 * @param props.kind Link predefined kind
 * @param props.classname Additional className
 * @param props.children Child elements
 * @param props.props Other link props
 *
 */
const LinkButton: FC<LinkButtonProps> = ({
	kind,
	className,
	children,
	...props
}) => (
	<Link className={`${ButtonStyles[kind]} ${className} inline-flex`} {...props}>
		{children}
	</Link>
);

export default LinkButton;
