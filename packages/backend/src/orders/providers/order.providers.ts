import { Provider } from '@nestjs/common';
import { OrderResolver } from '@Orders/resolvers/order.resolver';
import { OrderService } from '@Orders/services/order.service';

export const orderProviders: Provider[] = [OrderService, OrderResolver];
