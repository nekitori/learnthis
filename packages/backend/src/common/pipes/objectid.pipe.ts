import { CommonErrors } from '@Common/enums/common-errors.enum';
import { ObjectID } from '@Common/types/object-id.type';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { isValidObjectId } from 'mongoose';

/**
 * Object id's validator
 */
@Injectable()
export class ObjectIDPipe implements PipeTransform {
	/**
	 * ObjectID handler validator
	 *
	 * @param  {ObjectID}
	 * @returns ObjectID
	 */
	transform(value: string): ObjectID {
		if (!isValidObjectId(value))
			throw new BadRequestException(CommonErrors.FORMAT_OBJECT_ID);

		return value;
	}
}
