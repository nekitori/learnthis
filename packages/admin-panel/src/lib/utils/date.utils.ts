export const formatDuration = (duration: number): string => {
	const durationMinutes = duration / 60000;

	if (durationMinutes > 60) {
		return `${(durationMinutes / 60).toPrecision(1)}h ${(
			durationMinutes % 60
		).toPrecision(1)}m`;
	} else return `${durationMinutes.toPrecision(1)}m`;
};
