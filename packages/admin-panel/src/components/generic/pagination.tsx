import {
	LeftArrowIcon,
	RightArrowIcon,
} from '@Components/icons/generic/arrow-icons';
import { FC } from 'react';

type PaginationProps = {
	total: number;
	current: number;
	onPageChange?: (page: number) => void;
};

const Pagination: FC<PaginationProps> = ({ current, total, onPageChange }) => (
	<div className='flex flex-col items-center my-1'>
		<div className='flex text-gray'>
			<button
				disabled={current === 0}
				className='h-2 w-2 mx-0_5 flex-c-c rounded-full'
				onClick={() =>
					onPageChange && current > 0 && onPageChange(current - 1)
				}>
				<LeftArrowIcon className='w-2 h-2 stroke-gray' />
			</button>
			<div className='flex-c-c h-2 font-medium rounded-full'>
				{Array.from(Array(total).keys())
					.filter(
						item =>
							item === 0 || item === total - 1 || Math.abs(item - current) < 2
					)
					.map((item, index, arr) => {
						return (
							<div className='flex-c-c' key={item + 1}>
								{arr[index - 1] !== undefined && item - arr[index - 1] > 1 && (
									<div className='w-2 flex-c-c xssm:hidden cursor-default leading-5 transition duration-150 ease-in  rounded-full'>
										...
									</div>
								)}
								<div
									className={`w-2 h-2 flex-c-c ${
										item !== current ? 'xssm:hidden' : 'bg-primary text-white'
									} cursor-pointer leading-5 transition duration-150 ease-in rounded-full`}
									onClick={() => {
										onPageChange && current !== item && onPageChange(item);
									}}>
									{item + 1}
								</div>
							</div>
						);
					})}
			</div>
			<button
				disabled={current === total - 1}
				className='h-2 w-2 mx-0_5 flex-c-c rounded-full'
				onClick={() =>
					onPageChange && current < total - 1 && onPageChange(current + 1)
				}>
				<RightArrowIcon className='w-2 h-2 stroke-gray' />
			</button>
		</div>
	</div>
);

export default Pagination;
