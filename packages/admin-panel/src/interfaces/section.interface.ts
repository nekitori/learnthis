import { ILesson } from './lesson.interface';

export interface ISection {
	_id: string;
	title: string;
	description?: string;
	visibility: boolean;
	lessons: ILesson[];
}
