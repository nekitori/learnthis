import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Course } from './course.gqltype';
import { SectionPublic } from './section-public.gqltype';

/**
 * Graphql type: Course with sections that only contains data that can be sent to the student.
 */
@ObjectType()
export class CourseWithSectionsPublic extends Course {
	@Field(type => Int)
	studentsCount: number;
	sections: SectionPublic[];
}
