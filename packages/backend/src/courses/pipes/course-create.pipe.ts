import { CommonErrors } from '@Common/enums/common-errors.enum';
import { trimAllStrings } from '@Common/utils/clean-object';
import { CourseCreateDto } from '@Courses/dto/course-create.dto';
import { CourseErrors } from '@Courses/enums/course-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { CourseValidation } from 'learnthis-utils';

@Injectable()
export class CourseCreatePipe implements PipeTransform {
	transform(value: CourseCreateDto): CourseCreateDto {
		const { title, description, price, compareAtPrice, url } = value;

		const errors = [];

		if (!CourseValidation.titleValidation(title))
			errors.push(CommonErrors.FORMAT_INVALID_TITLE);

		if (!CourseValidation.descriptionValidation(description))
			errors.push(CommonErrors.FORMAT_INVALID_DESCRIPTION);

		if (!CourseValidation.priceValidation(price))
			errors.push(CourseErrors.FORMAT_COURSE_PRICE);

		if (!CourseValidation.priceValidation(compareAtPrice))
			errors.push(CourseErrors.FORMAT_COURSE_COMPARE_AT);

		if (price >= compareAtPrice)
			errors.push(CourseErrors.COURSE_COMPARE_AT_LESS_THAN_PRICE);

		if (!CourseValidation.urlValidation(url))
			errors.push(CommonErrors.FORMAT_URL);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		trimAllStrings(value);

		return value;
	}
}
