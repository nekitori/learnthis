export interface IEmailJobData {
	email: string;
	name: string;
	token: string;
}

export interface IUploadVideoMuxData {
	fileName: string;
	url: string;
	progress?: number;
}
