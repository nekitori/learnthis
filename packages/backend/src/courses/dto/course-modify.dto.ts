import { Field, Float, InputType } from '@nestjs/graphql';

@InputType()
export class CourseModifyDto {
	@Field()
	url?: string;
	@Field()
	title?: string;
	@Field()
	description?: string;
	@Field(type => Float)
	price?: number;
	@Field(type => Float, { nullable: true })
	compareAtPrice?: number;
}
