import { ThemeEnum } from '@Enums/config/theme.enum';
import Document, {
	DocumentContext,
	Head,
	Html,
	Main,
	NextScript,
} from 'next/document';

class MyDocument extends Document {
	static async getInitialProps(ctx: DocumentContext) {
		const initialProps = await Document.getInitialProps(ctx);

		return initialProps;
	}

	render() {
		return (
			<Html
				lang='es'
				className={`${
					this.props.__NEXT_DATA__.props.pageProps.componentProps?.theme ||
					ThemeEnum.LIGHT
				}`}>
				<Head>
					<link
						rel='icon'
						type='image/png'
						sizes='32x32'
						href='/static/favicon-32x32.png'
					/>
					<link
						rel='icon'
						type='image/png'
						sizes='16x16'
						href='/static/favicon-16x16.png'
					/>
					<link rel='manifest' href='/static/site.webmanifest' />
					<link
						rel='mask-icon'
						href='/static/safari-pinned-tab.svg'
						color='#5bbad5'
					/>
					<meta name='msapplication-tap-highlight' content='no'></meta>
					<meta name='msapplication-TileColor' content='#00aba9' />
					<meta name='theme-color' content='#ffffff' />
					<meta name='apple-mobile-web-app-title' content='LearnThis' />
					<meta name='apple-mobile-web-app-capable' content='yes' />
					<meta name='apple-mobile-web-app-status-bar-style' content='black' />
					<link rel='apple-touch-icon' href='/static/apple-touch-icon.png' />
					<link
						rel='apple-touch-icon'
						sizes='57x57'
						href='/static/apple-icon-57x57-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='60x60'
						href='/static/apple-icon-60x60-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='72x72'
						href='/static/apple-icon-72x72-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='76x76'
						href='/static/apple-icon-76x76-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='114x114'
						href='/static/apple-icon-114x114-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='120x120'
						href='/static/apple-icon-120x120-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='144x144'
						href='/static/apple-icon-144x144-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='152x152'
						href='/static/apple-icon-152x152-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='180x180'
						href='/static/apple-icon-180x180-manifest.png'
					/>
					<link
						rel='apple-touch-icon'
						sizes='167x167'
						href='/static/apple-icon-167x167-manifest.png'
					/>
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

export default MyDocument;
