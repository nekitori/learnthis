import { Env } from '@Common/enums/env.enum';
import { ObjectID } from '@Common/types/object-id.type';
import { MuxService } from '@Common/modules/agenda/services/mux.service';
import { FileProcessService } from '@Common/utils/file-process.service';
import {
	imageCourse,
	videoStorage,
	fileFilter,
	FOLDER_UPLOADS,
} from '@Common/utils/file-upload';
import { CourseService } from '@Courses/services/course.service';
import {
	BadRequestException,
	Body,
	Controller,
	Headers,
	Param,
	Post,
	UnauthorizedException,
	UploadedFile,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { WorkerRestAuthGuard } from '@Workers/auth/worker-rest-auth.guard';
import { join } from 'path';

@Controller('course')
export class CourseController {
	constructor(
		private readonly muxService: MuxService,
		private readonly courseService: CourseService,
		private readonly fileProcessService: FileProcessService,
		private readonly configService: ConfigService
	) {}

	@Post('uploadPublicVideo')
	@UseInterceptors(
		FileInterceptor('video', {
			storage: videoStorage,
		})
	)
	async uploadPublic(@UploadedFile() file): Promise<void> {
		if (!file) throw new BadRequestException();
		await this.muxService.uploadPublicVideo(file.filename);
	}

	@Post('webhooks')
	async webhooks(
		@Headers('mux-signature') sig: string,
		@Body() body: any
	): Promise<any> {
		if (!this.muxService.verifyWebhook(sig, body))
			throw new UnauthorizedException();

		return { received: true };
	}

	@Post('/:id/image')
	@UseGuards(WorkerRestAuthGuard)
	@UseInterceptors(
		FileInterceptor('image', {
			fileFilter,
			storage: imageCourse,
			limits: { fileSize: 4 * 1024 * 1024 },
		})
	)
	async uploadPhotoCourse(
		@UploadedFile() file,
		@Param('id') courseId: ObjectID
	) {
		if (!file) throw new BadRequestException();
		this.fileProcessService.transformImage(join(FOLDER_UPLOADS, file.filename));
		return {
			url: await this.courseService.setCourseImage(courseId, file.filename),
		};
	}

	@Post('/:id/bgImage')
	@UseGuards(WorkerRestAuthGuard)
	@UseInterceptors(
		FileInterceptor('bgImage', {
			fileFilter,
			storage: imageCourse,
			limits: { fileSize: 4 * 1024 * 1024 },
		})
	)
	async uploadBgCourse(@UploadedFile() file, @Param('id') courseId: ObjectID) {
		if (!file) throw new BadRequestException();
		this.fileProcessService.transformImage(
			join(FOLDER_UPLOADS, file.filename),
			Number(this.configService.get(Env.WIDTHBG_COURSE)),
			Number(this.configService.get(Env.HEIGHTBG_COURSE))
		);
		return {
			url: await this.courseService.setCourseBgImage(courseId, file.filename),
		};
	}
}
