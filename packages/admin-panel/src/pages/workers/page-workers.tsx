import { useQuery } from '@apollo/client';
import BlankCard from '@Components/generic/cards/blank-card';
import TitleCard from '@Components/generic/cards/title-card';
import LinkButton from '@Components/generic/form/button/link-button';
import SectionLayout from '@Components/layout/section.layout';
import Loader from '@Components/other/loader';
import WorkersTable from '@Components/workers/table/workers-table';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { withAuth } from '@Lib/hoc/withAuth';
import { GraphqlWorker } from 'learnthis-utils';
import { FC, useState } from 'react';
import { useLocation } from 'react-router-dom';

/**
 * Page component for worker user accounts management.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 */
const PageWorkers: FC = () => {
	const { data, loading, page, setPage } = getUsers();

	const users = data && !loading ? data.worker_find.data : null;
	const totalPages =
		data && !loading ? Math.ceil(data.worker_find.total / 10) : null;

	return (
		<SectionLayout title='Trabajadores'>
			<TitleCard
				className='w-3/4 xssm:w-full'
				title='Trabajadores'
				description='Listado de todos los trabajadores'>
				<WorkersTable
					workers={users}
					totalPages={totalPages}
					page={page}
					setPage={setPage}
				/>
			</TitleCard>
			<BlankCard className='w-1/4 xssm:w-full'>
				<div className='flex flex-wrap px-1_5 pb-2'>
					<div className='w-1/2 flexcol-s-c'>
						<div className='flex-c-c'>
							<div className='h-1 w-1 bg-pink rounded-md mr-1'></div>
							<span className='font-semibold text-gray'>Usuarios</span>
						</div>
						<span className='text-4xl font-bold px-2 text-black dark:text-white'>
							{data ? data.worker_find.total : <Loader />}
						</span>
					</div>
				</div>
				<div className='flex-c-c'>
					<LinkButton kind='primary' to={MainPaths.WORKER_CREATE}>
						Crear trabajador
					</LinkButton>
				</div>
			</BlankCard>
		</SectionLayout>
	);
};

const getUsers = () => {
	const location = useLocation<string>();

	const [page, setPage] = useState<number>(
		parseInt(new URLSearchParams(location.search).get('page') || '0')
	);
	const { data, loading } = useQuery(GraphqlWorker.worker_find, {
		fetchPolicy: 'cache-and-network',
		variables: {
			paginate: {
				offset: page * 10,
				limit: 10,
			},
		},
	});

	return { data, loading, page, setPage };
};

export default withAuth(PageWorkers, [WorkerRoles.ADMIN]);
