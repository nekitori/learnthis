import { ShoppingCartState } from '@Interfaces/states/shoppingcart-context.interface';
import { createContext } from 'react';

interface IShoppingCartContext {
	shoppingCart: ShoppingCartState;
	addItemShoppingCart: (courseId: string) => Promise<void>;
	removeItemShoppingCart: (courseId: string) => Promise<void>;
	clearShoppingCart: () => Promise<void>;
}

export const ShoppingCartContext = createContext<IShoppingCartContext>(
	{} as any
);
