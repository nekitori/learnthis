import { StudentErrors } from '@Students/enums/student-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation, TokenValidation } from 'learnthis-utils';
import { RecoverPasswordDto } from '../dto/recover-password.dto';

/**
 * RecoverPasswordDto pipe validator
 */
@Injectable()
export class RecoverPasswordPipe implements PipeTransform {
	/**
	 * RecoverPasswordDto handler validator
	 *
	 * @param  {RecoverPasswordDto} value
	 * @returns RecoverPasswordDto
	 */
	transform(value: RecoverPasswordDto): RecoverPasswordDto {
		const { newPassword, token } = value;

		if (!TokenValidation.validateJwt(token))
			throw new BadRequestException(StudentErrors.FORMAT_TOKEN);

		if (!FormValidation.passwordValidation(newPassword))
			throw new BadRequestException(StudentErrors.FORMAT_PASSWORD);

		return value;
	}
}
