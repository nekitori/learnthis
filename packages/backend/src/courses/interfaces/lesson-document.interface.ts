import { ObjectID } from '@Common/types/object-id.type';
import { Document } from 'mongoose';
import { ICommentDoc } from './comment-document.interface';
import { ICourseDoc } from './course-document.interface';

export type CourseOrObjectID = ICourseDoc | ObjectID;
export type CommentOrObjectID = ICommentDoc | ObjectID;

export type TLessonModel = ILessonDoc<CourseOrObjectID, CommentOrObjectID>;
export type TVideoLessonModel = IVideoLessonDoc<
	CourseOrObjectID,
	CommentOrObjectID
>;
export type TInfoLessonModel = IInfoLessonDoc<
	CourseOrObjectID,
	CommentOrObjectID
>;

export interface ILesson<
	T extends CourseOrObjectID = ObjectID,
	S extends CommentOrObjectID = ObjectID
> {
	title: string;
	description: string;
	course: T;
	comments: S[];
	visibility: boolean;
	resources: {
		links: string[];
		files: string[];
	};
	kind?: string;
}

export interface ILessonDoc<
	T extends CourseOrObjectID = ObjectID,
	S extends CommentOrObjectID = ObjectID
> extends ILesson<T, S>,
		Document {}

export interface IVideoLesson<
	T extends CourseOrObjectID = ObjectID,
	S extends CommentOrObjectID = ObjectID
> extends ILesson<T, S> {
	videoSrc: string;
	duration: number;
}
export interface IVideoLessonDoc<
	T extends CourseOrObjectID = ObjectID,
	S extends CommentOrObjectID = ObjectID
> extends IVideoLesson<T, S>,
		Document {}

export interface IInfoLesson<
	T extends CourseOrObjectID = ObjectID,
	S extends CommentOrObjectID = ObjectID
> extends ILesson<T, S> {
	content: string;
}

export interface IInfoLessonDoc<
	T extends CourseOrObjectID = ObjectID,
	S extends CommentOrObjectID = ObjectID
> extends IInfoLesson<T, S>,
		Document {}
