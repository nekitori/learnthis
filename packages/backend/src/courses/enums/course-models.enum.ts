export enum CourseModels {
	COURSE = 'Course',
	LESSON = 'Lesson',
	LESSON_VIDEO = 'VideoLesson',
	LESSON_INFO = 'InfoLesson',
	COMMENT = 'Comment',
	COMMENT_ROOT = 'RootComment',
	COMMENT_REPLY = 'ReplyComment',
}
