import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { ICourse } from '@Interfaces/course.interface';

export type CourseWrapperProps = {
	course?: ICourse;
	refetch: ApolloQueryRefetch;
};
