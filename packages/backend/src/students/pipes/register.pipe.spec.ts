import { BadRequestException } from '@nestjs/common';
import { RegisterDto } from '@Students/dto/register.dto';
import { RegisterPipe } from './register.pipe';

describe('RegisterPipe', () => {
	let registerPipe: RegisterPipe;

	beforeAll(() => (registerPipe = new RegisterPipe()));

	it('Todo correcto', () => {
		const register: RegisterDto = {
			email: 'yoquese@gmail.com',
			name: 'Albert',
			surname: 'hvdev',
			password: 'asd123ASD.',
		};
		expect(registerPipe.transform(register)).toBe(register);
	});

	it('Email incorrecto', () => {
		const register: RegisterDto = {
			email: 'yoquese@gmail.com.es.',
			name: 'Albert',
			surname: 'hvdev',
			password: 'asd123ASD.',
		};
		expect(() => registerPipe.transform(register)).toThrow(BadRequestException);
	});

	it('Nombre incorrecto', () => {
		const register: RegisterDto = {
			email: 'yoquese@gmail.com',
			name: 'Albert12',
			surname: 'hvdev',
			password: 'asd123ASD.',
		};
		expect(() => registerPipe.transform(register)).toThrow(BadRequestException);
	});

	it('Apellido incorrecto', () => {
		const register: RegisterDto = {
			email: 'yoquese@gmail.com',
			name: 'Albert',
			surname: 'Crack-32',
			password: 'asd123ASD.',
		};
		expect(() => registerPipe.transform(register)).toThrow(BadRequestException);
	});

	it('Contraseña incorrecta', () => {
		const register: RegisterDto = {
			email: 'yoquese@gmail.com',
			name: 'Albert',
			surname: 'Crack',
			password: 'ásd123äSD.',
		};
		expect(() => registerPipe.transform(register)).toThrow(BadRequestException);
	});
});
