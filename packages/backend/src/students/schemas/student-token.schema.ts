import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { StudentModels } from '@Students/enums/student-models.enum';
import { StudentTokenTypes } from '@Students/enums/student-token-types.enum';
import { Schema as MongooseSchema } from 'mongoose';
import { Student } from './students.schema';

@Schema()
export class StudentToken {
	@Prop({ type: String, required: true, unique: true, index: true })
	token: string;

	@Prop({ type: MongooseSchema.Types.ObjectId, ref: StudentModels.STUDENT })
	student: Student;

	@Prop({ type: String, enum: Object.values(StudentTokenTypes) })
	type: StudentTokenTypes;

	@Prop({
		type: Date,
		required: true,
		index: { expires: 1, sparse: true },
	})
	expiresAt: Date;
}

export const StudentTokenSchema = SchemaFactory.createForClass(StudentToken);
