import { Gender } from '@Common/enums/gender.enum';
import { CourseModels } from '@Courses/enums/course-models.enum';
import { Course } from '@Courses/schemas/course.schema';
import { Lesson } from '@Courses/schemas/lesson.schema';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { OrderModels } from '@Orders/enums/order-models.enum';
import { Order } from '@Orders/schemas/order.schema';
import { SocialType } from '@Students/enums/social-type.enum';
import { Schema as MongooseSchema } from 'mongoose';

type SocialAccountsProps = 'id' | 'type';

interface ISocialAccountsProps {
	id: string[];
	type: string[];
}

type CourseEnrolledProps = 'course' | 'progress';
interface ICourseEnrolledProps {
	course: Course;
	progress: {
		currentLesson: Lesson;
		lessons: {
			lesson: Lesson;
			completed: boolean;
		}[];
	};
}

@Schema()
export class Student {
	@Prop({ type: String, required: true, unique: true })
	email: string;

	@Prop({ type: String, required: true })
	name: string;

	@Prop({ type: String, required: true })
	surname: string;

	@Prop({ type: String })
	password: string;

	@Prop({ type: Boolean, required: true, default: false })
	active: boolean;

	@Prop({ type: String })
	username: string;

	@Prop({ type: String })
	photo: string;

	@Prop({ type: String })
	bio: string;

	@Prop({ type: String, enum: Object.values(Gender) })
	gender: Gender;

	@Prop({ type: Date })
	birthDate: Date;

	@Prop(
		raw([
			{
				id: { type: String, required: true },
				type: { type: String, enum: Object.values(SocialType) },
			},
		])
	)
	socialAccounts: Record<SocialAccountsProps, ISocialAccountsProps>;

	@Prop(
		raw([
			{
				course: {
					type: MongooseSchema.Types.ObjectId,
					ref: CourseModels.COURSE,
					required: true,
				},
				progress: {
					currentLesson: {
						type: MongooseSchema.Types.ObjectId,
						ref: CourseModels.LESSON,
					},
					lessons: [
						{
							lesson: {
								type: MongooseSchema.Types.ObjectId,
								ref: CourseModels.LESSON,
								required: true,
							},
							completed: { type: Boolean, required: true, default: false },
						},
					],
				},
			},
		])
	)
	coursesEnrolled: Record<CourseEnrolledProps, ICourseEnrolledProps>;

	@Prop({
		type: [{ type: MongooseSchema.Types.ObjectId, ref: OrderModels.ORDER }],
	})
	orders: Order[];

	@Prop({
		type: [{ type: MongooseSchema.Types.ObjectId, ref: CourseModels.COURSE }],
	})
	cart: Course[];
}

export const StudentSchema = SchemaFactory.createForClass(Student);

StudentSchema.index({ email: 1 });
StudentSchema.index({ 'socialAccounts.id': 1, 'socialAccounts.type': 1 });
