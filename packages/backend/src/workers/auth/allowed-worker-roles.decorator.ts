import { SetMetadata } from '@nestjs/common';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';

export const AllowedWorkerRoles = (...roles: WorkerRoles[]) =>
	SetMetadata('roles', roles);
