import { GetGqlAuthUser } from '@Common/auth/get-user.decorator';
import { PaginateDto } from '@Common/dto/paginate.dto';
import { IPaginate } from '@Common/interfaces/mongoose.interface';
import { ObjectIDArrayPipe } from '@Common/pipes/objectid-array.pipe';
import { ObjectID } from '@Common/types/object-id.type';
import { ILessonDoc } from '@Courses/interfaces/lesson-document.interface';
import { UseGuards } from '@nestjs/common';
import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';
import { StudentGqlAuthGuard } from '@Students/auth/student-gql-auth.guard';
import { ChangePasswordDto } from '@Students/dto/change-password.dto';
import { LoginDto } from '@Students/dto/login.dto';
import { ModifyProfileDto } from '@Students/dto/modify-profile.dto';
import { RecoverPasswordDto } from '@Students/dto/recover-password.dto';
import { RegisterDto } from '@Students/dto/register.dto';
import { SocialLoginDto } from '@Students/dto/social-login.dto';
import { SocialUnlinkDto } from '@Students/dto/social-unlink.dto';
import { StudentCourseProgress } from '@Students/gql-types/student-course-progress.gqltype';
import { StudentLogin } from '@Students/gql-types/student-login.gqltype';
import { StudentPaginated } from '@Students/gql-types/student-paginated.gqltype';
import { StudentProfile } from '@Students/gql-types/student-profile.gqltype';
import { Student } from '@Students/gql-types/student.gqltype';
import {
	ICourseProgress,
	ISocialAccount,
	IStudent,
	IStudentDoc,
} from '@Students/interfaces/student-document.interface';
import { ChangePasswordPipe } from '@Students/pipes/change-password.pipe';
import { EmailPipe } from '@Students/pipes/email.pipe';
import { LoginPipe } from '@Students/pipes/login.pipe';
import { ModifyProfilePipe } from '@Students/pipes/modify-profile.pipe';
import { RecoverPasswordPipe } from '@Students/pipes/recover-password.pipe';
import { RegisterPipe } from '@Students/pipes/register.pipe';
import { SocialLoginPipe } from '@Students/pipes/social-login.pipe';
import { TokenPipe } from '@Students/pipes/token.pipe';
import { UsernamePipe } from '@Students/pipes/username.pipe';
import { TokenService } from '@Students/services/student-token.service';
import { StudentsService } from '@Students/services/students.service';
import { LoginOutput } from '@Students/types/login-output.type';
import { SocialAccountInfo } from '@Students/types/social-account-info.type';
import { WorkerGqlAuthGuard } from '@Workers/auth/worker-gql-auth.guard';

/**
 * Student graphql queries and resolvers.
 */
@Resolver(() => Student)
export class StudentsResolver {
	/**
	 * Dependency injection.
	 * @param studentService Student service
	 * @param tokenService Student token service
	 */
	constructor(
		private readonly studentService: StudentsService,
		private readonly tokenService: TokenService
	) {}

	//#region Find

	/**
	 * Gets gurrent user.
	 *
	 * - AUTH: Student
	 * @param student Student data
	 */
	@Query(() => StudentProfile)
	@UseGuards(StudentGqlAuthGuard)
	async student_profile(
		@GetGqlAuthUser() student: IStudentDoc
	): Promise<StudentProfile> {
		return {
			_id: student.id,
			...student.toJSON(),
			isSocialLogin: !student.password,
		};
	}

	/**
	 * Finds all existing students (with pagination).
	 *
	 * - AUTH: Workers
	 * - ROLES: All
	 * @param paginate Pagination options
	 * @returns Students array paginated
	 */
	@Query(() => StudentPaginated)
	@UseGuards(WorkerGqlAuthGuard)
	student_find(
		@Args('paginate', { type: () => PaginateDto, nullable: true })
		paginate: PaginateDto
	): Promise<IPaginate<IStudentDoc>> {
		return this.studentService.findPaginate(
			(paginate && paginate.offset) || 0,
			(paginate && paginate.limit) || 10
		);
	}

	//#endregion

	//#region Students

	/**
	 * Student login.
	 *
	 * - AUTH: Public
	 * @param input.email Student email
	 * @param input.password Student password
	 * @returns JWT token and student data
	 */
	@Query(() => StudentLogin)
	async student_login(
		@Args('input', { type: () => LoginDto }, LoginPipe)
		input: LoginDto
	): Promise<LoginOutput> {
		return this.studentService.login(input);
	}

	/**
	 * Registers a new student.
	 *
	 * - AUTH: Public
	 * @param input New student data
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	async student_register(
		@Args('input', { type: () => RegisterDto }, RegisterPipe)
		input: RegisterDto
	): Promise<boolean> {
		await this.studentService.register(input);
		return true;
	}

	/**
	 * Changes student password.
	 *
	 * - AUTH: Student
	 * @param student Student data
	 * @param input Old and new password
	 */
	@Mutation(() => Boolean)
	@UseGuards(StudentGqlAuthGuard)
	async student_change_password(
		@GetGqlAuthUser() student: IStudentDoc,
		@Args('input', { type: () => ChangePasswordDto }, ChangePasswordPipe)
		input: ChangePasswordDto
	): Promise<boolean> {
		await this.studentService.changePassword(student, input);
		return true;
	}

	/**
	 * Changes student email.
	 *
	 * - AUTH: Student
	 * @param student Student data
	 * @param newEmail New student email
	 * @returns New student email
	 */
	@Mutation(() => String)
	@UseGuards(StudentGqlAuthGuard)
	async student_change_email(
		@GetGqlAuthUser() student: IStudentDoc,
		@Args('newEmail', EmailPipe) newEmail: string
	): Promise<string> {
		return this.studentService.changeEmail(student, newEmail);
	}

	/**
	 * Changes student username.
	 *
	 * - AUTH: Student
	 * @param student Student data
	 * @param newUsername New student username
	 * @returns New student username
	 */
	@Mutation(() => String)
	@UseGuards(StudentGqlAuthGuard)
	async student_change_username(
		@GetGqlAuthUser() student: IStudentDoc,
		@Args('newUsername', UsernamePipe) newUsername: string
	): Promise<string> {
		return this.studentService.changeUsername(student, newUsername);
	}

	/**
	 * Modifies student profile data.
	 *
	 * - AUTH: Student
	 * @param student Student data
	 * @param input New student data
	 * @returns True if success
	 */
	@Mutation(() => Student)
	@UseGuards(StudentGqlAuthGuard)
	async student_modify_profile(
		@GetGqlAuthUser() student: IStudentDoc,
		@Args('input', { type: () => ModifyProfileDto }, ModifyProfilePipe)
		input: Partial<ModifyProfileDto>
	): Promise<IStudentDoc> {
		return this.studentService.modifyProfile(student, input);
	}

	//#endregion

	//#region Activate and recover

	/**
	 * Activates student account from an activation token.
	 *
	 * - AUTH: Public
	 * @param token Activation token
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	async student_activate_account(
		@Args('token', { type: () => ID }, TokenPipe) token: string
	): Promise<boolean> {
		await this.studentService.activateAccount(token);
		return true;
	}

	/**
	 * Checks if a recover password token is valid.
	 *
	 * - AUTH: Public
	 * @param token Forgot password token
	 */
	@Query(() => Boolean)
	async student_valid_forgot_password_token(
		@Args('token', { type: () => ID }, TokenPipe) token: string
	): Promise<boolean> {
		await this.tokenService.checkToken(token);
		return true;
	}

	/**
	 * Creates a new forgot password token and send it by email.
	 *
	 * - AUTH: Public
	 * @param email Student email
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	async student_create_forgot_password_token(
		@Args('email', EmailPipe) email: string
	): Promise<boolean> {
		await this.studentService.createForgotPasswordToken(email);
		return true;
	}

	/**
	 * Changes student password from a forgot password token.
	 *
	 * - AUTH: Public
	 * @param input Recover password data
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	async student_change_forgot_password(
		@Args('input', { type: () => RecoverPasswordDto }, RecoverPasswordPipe)
		input: RecoverPasswordDto
	): Promise<boolean> {
		await this.studentService.changeForgotPassword(input);
		return true;
	}

	//#endregion

	//#region Courses

	/**
	 * Gets student progress for a specific course.
	 *
	 * - AUTH: Student
	 * @param student Student data
	 * @param courseUrl Course url
	 * @returns Course progress populated
	 */
	@Query(() => StudentCourseProgress)
	@UseGuards(StudentGqlAuthGuard)
	async student_course_progress(
		@GetGqlAuthUser() student: IStudentDoc,
		@Args('courseUrl') courseUrl: string
	): Promise<ICourseProgress<ILessonDoc>> {
		return this.studentService.getCourseProgress(student, courseUrl);
	}

	//#endregion

	//#region Social

	/**
	 * Student social login.
	 *
	 * - AUTH: Public
	 * @param input Social login data
	 * @returns JWT token and student data
	 */
	@Mutation(() => StudentLogin)
	async student_social_login(
		@Args('input', { type: () => SocialLoginDto }, SocialLoginPipe)
		input: {
			socialAccount: ISocialAccount;
			studentData: IStudent;
		}
	): Promise<LoginOutput> {
		return this.studentService.socialLogin(input);
	}

	/**
	 * Links a social account to an student.
	 *
	 * - AUTH: Student
	 * @param input Social account data
	 * @param student Student data
	 */
	@Mutation(() => Boolean)
	@UseGuards(StudentGqlAuthGuard)
	async student_link_social_profile(
		@Args('input', { type: () => SocialLoginDto }, SocialLoginPipe)
		input: SocialAccountInfo,
		@GetGqlAuthUser()
		student: IStudentDoc
	): Promise<boolean> {
		await this.studentService.linkSocialProfile(input.socialAccount, student);
		return true;
	}

	/**
	 *	Unlink a social account to an student.
	 *
	 * - AUTH: Student
	 * @param input Social account data
	 * @param student Student data
	 */
	@Mutation(() => Boolean)
	@UseGuards(StudentGqlAuthGuard)
	async student_unlink_social_profile(
		@Args('input', { type: () => SocialUnlinkDto })
		input: ISocialAccount,
		@GetGqlAuthUser()
		student: IStudentDoc
	): Promise<boolean> {
		await this.studentService.unlinkSocialProfile(input, student);
		return true;
	}

	//#endregion

	//#region Cart

	@Mutation(() => Boolean)
	@UseGuards(StudentGqlAuthGuard)
	async student_update_cart(
		@GetGqlAuthUser() student: IStudentDoc,
		@Args('courses', { type: () => [ID!]! }, ObjectIDArrayPipe)
		courses: ObjectID[]
	): Promise<boolean> {
		await this.studentService.updateCart(student, courses);
		return true;
	}

	//#endregion
}
