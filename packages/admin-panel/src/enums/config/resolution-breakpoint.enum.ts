export enum ResolutionBreakpoints {
	XS = 'xs',
	SM = 'sm',
	MD = 'md',
	LG = 'lg',
}
