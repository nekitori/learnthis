import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { AuthContext } from '@Lib/context/auth.context';
import { FC, useContext } from 'react';
import { NavLink } from 'react-router-dom';

type ProfileNavProps = {
	workerId: string;
};

const ProfileNav: FC<ProfileNavProps> = ({ workerId }) => {
	const links = mapLinks(workerId);

	return (
		<div className='w-full px-0_5 xssm:order-1 mb-1'>
			<div className='flex-c-c'>{links}</div>
		</div>
	);
};

const mapLinks = (workerId: string) => {
	const { authState } = useContext(AuthContext);
	const links = [
		{
			href: MainPaths.WORKERS.replace(MainParams.WORKER_ID, workerId),
			text: 'Ficha',
		},
		{
			href: MainPaths.WORKER_DATA.replace(MainParams.WORKER_ID, workerId),
			text: 'Datos',
		},
		{
			href: MainPaths.WORKER_COURSES.replace(MainParams.WORKER_ID, workerId),
			text: 'Cursos',
		},
	];

	if (authState.worker?._id !== workerId) {
		links.push({
			href: MainPaths.WORKER_ADVANCED.replace(MainParams.WORKER_ID, workerId),
			text: 'Avanzado',
		});
	}

	return links.map((i, index) => {
		return (
			<NavLink
				to={i.href}
				exact={true}
				className={`py-0_5 px-1_25 mx-0_5 text-gray font-semibold rounded-lg hover:text-white hover:bg-primary`}
				key={index}
				activeClassName='bg-primary text-white'>
				{i.text}
			</NavLink>
		);
	});
};

export default ProfileNav;
