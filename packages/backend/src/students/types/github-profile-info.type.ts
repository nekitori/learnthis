export type GithubProfileInfo = {
	login: string;
	id: number;
	avatar_url: string;
	gravatar_id: string;
	name: any;
	email: any;
};
