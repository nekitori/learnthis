import { ObjectType } from '@nestjs/graphql';
import { StudentProfile } from './student-profile.gqltype';

/**
 * Graphql type: Student login output
 */
@ObjectType()
export class StudentLogin {
	token: string;
	user: StudentProfile;
}
