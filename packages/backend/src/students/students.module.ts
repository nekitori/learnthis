import { AgendaModule } from '@Common/modules/agenda/agenda.module';
import { FileProcessService } from '@Common/utils/file-process.service';
import { imageStorage } from '@Common/utils/file-upload';
import { CoursesModule } from '@Courses/courses.module';
import { forwardRef, HttpModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { WorkersModule } from '@Workers/workers.module';
import { StudentGqlAuthGuard } from './auth/student-gql-auth.guard';
import { StudentController } from './controllers/student.controller';
import { StudentModels } from './enums/student-models.enum';
import { studentTokenProviders } from './providers/student-token.providers';
import { studentProviders } from './providers/student.providers';
import { StudentTokenSchema } from './schemas/student-token.schema';
import { StudentSchema } from './schemas/students.schema';
import { StudentsService } from './services/students.service';

@Module({
	imports: [
		JwtModule.register({}),
		MongooseModule.forFeature([
			{ name: StudentModels.STUDENT, schema: StudentSchema },
			{ name: StudentModels.TOKEN, schema: StudentTokenSchema },
		]),
		MulterModule.register({
			storage: imageStorage,
		}),
		HttpModule,
		CoursesModule,
		forwardRef(() => WorkersModule),
		AgendaModule,
	],
	providers: [
		...studentProviders,
		...studentTokenProviders,
		StudentGqlAuthGuard,
		FileProcessService,
	],
	controllers: [StudentController],
	exports: [StudentsService, StudentGqlAuthGuard, JwtModule],
})
export class StudentsModule {}
