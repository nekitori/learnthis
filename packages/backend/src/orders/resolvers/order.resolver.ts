import { GetGqlAuthUser } from '@Common/auth/get-user.decorator';
import { ObjectID } from '@Common/types/object-id.type';
import { ObjectIDArrayPipe } from '@Common/pipes/objectid-array.pipe';
import { ObjectIDPipe } from '@Common/pipes/objectid.pipe';
import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { IOrderDoc } from '@Orders/interfaces/order-document.interface';
import { OrderService } from '@Orders/services/order.service';
import { Order } from '@Orders/gql-types/order.gqltype';
import { StudentGqlAuthGuard } from '@Students/auth/student-gql-auth.guard';
import { IStudentDoc } from '@Students/interfaces/student-document.interface';

@Resolver(of => Order)
export class OrderResolver {
	constructor(private readonly orderService: OrderService) {}

	@Query(() => [Order])
	order_student_find(
		// TODO: Extraer este ID del AuthGuard
		@Args('studentId', ObjectIDPipe) studentId: ObjectID
	): Promise<IOrderDoc[]> {
		return this.orderService.findByStudent(studentId);
	}

	@Query(() => [Order])
	order_admin_find(): Promise<IOrderDoc[]> {
		return this.orderService.find();
	}

	@Mutation(() => [String])
	@UseGuards(StudentGqlAuthGuard)
	async order_create(
		@GetGqlAuthUser() student: IStudentDoc,
		@Args('coursesIds', { type: () => [String!]! }, ObjectIDArrayPipe)
		coursesIds: ObjectID[]
	): Promise<string[]> {
		return await this.orderService.create(coursesIds, student);
	}

	@Mutation(() => Boolean)
	async order_process(
		@Args('orderId', ObjectIDPipe) orderId: ObjectID
	): Promise<boolean> {
		await this.orderService.process(orderId);
		return true;
	}
}
