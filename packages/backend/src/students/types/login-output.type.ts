import { StudentProfile } from '@Students/gql-types/student-profile.gqltype';

/**
 * Login response data
 */
export type LoginOutput = {
	/** JWT auth token */
	token: string;
	/** Student data */
	user: StudentProfile;
};
