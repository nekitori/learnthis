import nextConnect from 'next-connect';
import passport from 'passport';

import { LoginStrategies } from '@Enums/config/login-strategies.enum';

import { ghStrategy } from '@Lib/login/github';

const signin = nextConnect();

passport.use(ghStrategy);

signin.use(passport.initialize());

signin.get((req: any, res, next) => {
	const state = Buffer.from(JSON.stringify({ link: true })).toString('base64');
	const authenticator = passport.authenticate(LoginStrategies.GITHUB, {
		session: false,
		state,
	});
	authenticator(req, res, next);
});

export default signin;
