import Pagination from '@Components/generic/pagination';
import Loader from '@Components/other/loader';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { IWorker } from '@Interfaces/worker.interface';
import { Dispatch, FC, SetStateAction } from 'react';
import { useHistory } from 'react-router-dom';
import WorkerRow from './workers-row';

type WorkersTableProps = {
	workers: IWorker[] | null;
	totalPages: number | null;
	page: number;
	setPage: Dispatch<SetStateAction<number>>;
};

/**
 * Users data table.
 * @param props.workers Workers array
 * @param props.totalPages Pagination count
 * @param props.page Current page number
 * @param props.setPage SetState for current page
 */
const WorkersTable: FC<WorkersTableProps> = ({
	workers,
	totalPages,
	page,
	setPage,
}) => {
	const history = useHistory<string>();

	return (
		<div className='font-semibold text-sm'>
			<div
				key={-1}
				className='flex-c-c xssm:hidden border-b border-gray-light dark:border-gray-dark pb-0_75'>
				<div className='w-3/12 flex-s-c px-1_5'>
					<span className='text-white-dark dark:text-white'>USER</span>
				</div>
				<div className='w-4/12'>
					<span className='text-white-dark dark:text-white'>EMAIL</span>
				</div>
				<div className='w-2/12 text-center'>
					<span className='text-white-dark dark:text-white'>ACTIVE</span>
				</div>
				<div className='w-2/12 text-center'>
					<span className='text-white-dark dark:text-white'>ROLES</span>
				</div>
				<div className='w-1/12 text-center'></div>
			</div>
			{workers && totalPages ? (
				<>
					{workers.map((i, index) => (
						<WorkerRow key={index} worker={i} />
					))}
					<Pagination
						current={page}
						total={totalPages}
						onPageChange={e => {
							setPage(e);
							history.push(MainPaths.WORKERS_LIST + '?page=' + e);
						}}
					/>
				</>
			) : (
				<Loader />
			)}
		</div>
	);
};

export default WorkersTable;
