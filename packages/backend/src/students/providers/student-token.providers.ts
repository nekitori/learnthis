import { Provider } from '@nestjs/common';
import { TokenService } from '@Students/services/student-token.service';

export const studentTokenProviders: Provider[] = [TokenService];
