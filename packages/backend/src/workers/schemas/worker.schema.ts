import { CourseModels } from '@Courses/enums/course-models.enum';
import { Course } from '@Courses/schemas/course.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import { Schema as MongooseSchema } from 'mongoose';
@Schema()
export class Worker {
	@Prop({ type: String, required: true, unique: true })
	email: string;

	@Prop({ type: String, required: true })
	name: string;

	@Prop({ type: String, required: true })
	surname: string;

	@Prop({ type: String, required: true })
	password: string;

	@Prop({ type: Boolean, required: true, default: false })
	active: boolean;

	@Prop({ type: String })
	displayName: string;

	@Prop({ type: String })
	phone: string;

	@Prop({ type: String })
	username: string;

	@Prop({ type: String })
	photo: string;

	@Prop({ type: String })
	bio: string;

	@Prop({ type: [{ type: String, enum: Object.values(WorkerRoles) }] })
	roles: WorkerRoles[];

	@Prop({
		type: [{ type: MongooseSchema.Types.ObjectId, ref: CourseModels.COURSE }],
	})
	teaches: Course[];
}

export const WorkerSchema = SchemaFactory.createForClass(Worker);

WorkerSchema.index({ email: 1 });
