import {
	ApolloClient,
	ApolloLink,
	InMemoryCache,
	NormalizedCacheObject,
} from '@apollo/client';
import { BatchHttpLink } from '@apollo/client/link/batch-http';
import { onError } from '@apollo/client/link/error';
import { setContext } from '@apollo/client/link/context';
import { GQL_ENDPOINT } from '@Keys';
import { LOCAL_STORAGE_JWT } from '@Lib/hooks/useAuthAndApollo.hook';

export const createApolloClient = (unAuthCallback: () => void) => {
	const authLink = setContext((_, { headers }) => {
		const jwt = localStorage.getItem(LOCAL_STORAGE_JWT);

		return {
			headers: {
				...headers,
				Authorization: jwt ? `Bearer ${jwt}` : '',
			},
		};
	});

	const errorLink = onError(({ graphQLErrors, networkError }) => {
		if (graphQLErrors) {
			let logOutFired = false;
			graphQLErrors.map(({ message, locations, path, extensions }) => {
				if (extensions?.exception.status === 401 && !logOutFired) {
					logOutFired = true;
					unAuthCallback();
				}
				console.log(
					`[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
						locations
					)}, Path: ${path}`
				);
			});
		}
		if (networkError) console.log(`[Network error]: ${networkError}`);
	});

	const batchLink = new BatchHttpLink({
		uri: GQL_ENDPOINT,
		batchInterval: 30,
	});

	return new ApolloClient<NormalizedCacheObject>({
		link: ApolloLink.from([errorLink, authLink, batchLink]),
		defaultOptions: {
			watchQuery: {
				fetchPolicy: 'cache-and-network',
			},
		},
		cache: new InMemoryCache({
			addTypename: false,
		}),
	});
};

let _currentApolloClient: ApolloClient<NormalizedCacheObject> | undefined;

export const generateApolloClient = (unAuthCallback: () => void) => {
	if (!_currentApolloClient)
		_currentApolloClient = createApolloClient(unAuthCallback);
	return _currentApolloClient;
};
