import {
	getCurrentSession,
	WithTransaction,
} from '@Common/decorators/transactional';
import { ObjectID } from '@Common/types/object-id.type';
import { CourseErrors } from '@Courses/enums/course-errors.enum';
import { ICourseDoc } from '@Courses/interfaces/course-document.interface';
import { CourseService } from '@Courses/services/course.service';
import {
	ConflictException,
	Injectable,
	NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { OrderErrors } from '@Orders/enums/order-errors.enum';
import { OrderModels } from '@Orders/enums/order-models.enum';
import { OrderStatus } from '@Orders/enums/order-status.enum';
import { IStudentDoc } from '@Students/interfaces/student-document.interface';
import { Model } from 'mongoose';
import {
	IOrderDoc,
	IOrderLine,
	TOrderModel,
} from '../interfaces/order-document.interface';

@Injectable()
export class OrderService {
	constructor(
		@InjectModel(OrderModels.ORDER)
		private readonly orderModel: Model<TOrderModel>,
		private readonly courseService: CourseService
	) {}

	findById(orderId: string): Promise<IOrderDoc> {
		return this.orderModel.findById(orderId).exec() as Promise<IOrderDoc>;
	}

	find(): Promise<IOrderDoc[]> {
		return this.orderModel.find().exec() as Promise<IOrderDoc[]>;
	}

	findByStudent(studentId: string): Promise<IOrderDoc[]> {
		return this.orderModel.find({ student: studentId }).exec() as Promise<
			IOrderDoc[]
		>;
	}

	findByIdAndPopulateAll(
		orderId: string
	): Promise<IOrderDoc<IStudentDoc, ICourseDoc>> {
		return this.orderModel
			.findById(orderId)
			.session(getCurrentSession())
			.populate('student')
			.populate('orderLines.course')
			.exec() as Promise<IOrderDoc<IStudentDoc, ICourseDoc>>;
	}

	async create(courseIds: ObjectID[], student: IStudentDoc): Promise<string[]> {
		const orderLines: IOrderLine[] = [];
		for (const courseId of courseIds) {
			const course = await this.courseService.findById(courseId);
			if (!course) throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);
			if (course.studentsEnrolled.includes(student._id))
				throw new ConflictException();

			orderLines.push({
				course: course._id,
				priceWithTax: course.price,
				priceWithoutTax: course.price,
			});
		}

		const order = await this.orderModel.create({
			status: OrderStatus.UNPAID,
			student: student._id,
			orderLines,
		});

		//FIXME Remove on payment development
		await this.process(order._id);

		return courseIds;
	}

	@WithTransaction
	async process(orderId: ObjectID): Promise<void> {
		const session = getCurrentSession();
		const order = await this.findByIdAndPopulateAll(orderId);
		if (!order) throw new NotFoundException(OrderErrors.ORDER_NOT_FOUND);

		const { student, orderLines } = order;

		for (const orderLine of orderLines) {
			const { course } = orderLine;

			const lessons: { lesson: ObjectID; completed: boolean }[] = [];

			for (const section of course.sections) {
				for (const lesson of section.lessons) {
					lessons.push({ lesson, completed: false });
				}
			}

			//TODO utilizar servicios de Student y Course para realizar estas operaciones
			student.coursesEnrolled.push({
				course: course._id,
				progress: {
					currentLesson: lessons[0]?.lesson,
					lessons,
				},
			});

			course.studentsEnrolled.push(student._id);

			await course.save({ session });
		}

		await student.save({ session });

		order.status = OrderStatus.PROCESSED;
		await order.save({ session });
	}
}
