import { ObjectID } from '@Common/types/object-id.type';
import { StudentOrObjectID } from '@Courses/interfaces/course-document.interface';
import { CourseOrObjectID } from '@Courses/interfaces/lesson-document.interface';
import { OrderStatus } from '@Orders/enums/order-status.enum';
import { Document } from 'mongoose';

export type TOrderModel = IOrderDoc<StudentOrObjectID, CourseOrObjectID>;

export interface IOrder<
	S extends StudentOrObjectID = ObjectID,
	T extends CourseOrObjectID = ObjectID
> {
	student: S;
	status: OrderStatus;
	orderLines: IOrderLine<T>[];
	totalWithTax?: number;
	totalWithoutTax?: number;
}

export interface IOrderLine<T extends CourseOrObjectID = ObjectID> {
	priceWithoutTax: number;
	priceWithTax: number;
	course: T;
}

export interface IOrderDoc<
	S extends StudentOrObjectID = ObjectID,
	T extends CourseOrObjectID = ObjectID
> extends IOrder<S, T>,
		Document {}
