import { CoursesModule } from '@Courses/courses.module';
import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StudentsModule } from '@Students/students.module';
import { OrderModels } from './enums/order-models.enum';
import { orderProviders } from './providers/order.providers';
import { OrderSchema } from './schemas/order.schema';

@Module({
	imports: [
		MongooseModule.forFeature([
			{ name: OrderModels.ORDER, schema: OrderSchema },
		]),
		forwardRef(() => StudentsModule),
		forwardRef(() => CoursesModule),
	],
	providers: [...orderProviders],
})
export class OrdersModule {}
