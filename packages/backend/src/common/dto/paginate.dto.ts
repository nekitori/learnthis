import { Field, InputType, Int } from '@nestjs/graphql';

/**
 * Graphql input type: Pagination
 */
@InputType()
export class PaginateDto {
	@Field(() => Int, { nullable: true })
	offset!: number;

	@Field(() => Int, { nullable: true })
	limit!: number;
}
