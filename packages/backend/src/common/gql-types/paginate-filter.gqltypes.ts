import { Type } from '@nestjs/common';

import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';

export default function Paginated<TItem>(TItemClass: Type<TItem>): any {
	const { name } = TItemClass;
	@ObjectType(`${name}Paginated`, { isAbstract: true })
	class _Paginated<TItem> {
		@Field(() => [TItemClass!])
		data!: TItem[];

		@Field(() => Int)
		offset!: number;

		@Field(() => Int)
		limit!: number;

		@Field(() => Int)
		total!: number;
	}

	return _Paginated;
}
