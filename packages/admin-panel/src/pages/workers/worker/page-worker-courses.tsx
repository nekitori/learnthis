import { useQuery } from '@apollo/client';
import CoursePhoto from '@Components/courses/course-photo';
import Loader from '@Components/other/loader';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { ICourse } from '@Interfaces/course.interface';
import { WorkerProfileProps } from '@Interfaces/props/worker-profile-props.interface';
import { GraphqlCourse } from 'learnthis-utils';
import { FC } from 'react';
import { Link } from 'react-router-dom';

/**
 * Subpage component to retrieve a list of all courses tutored by a worker.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 *
 * @param props.worker Worker data
 */
const PageWorkerCourses: FC<WorkerProfileProps> = ({ worker }) => {
	const { data, loading } = getCourses(worker.teaches);

	if (loading || !data)
		return (
			<div className='flex-c-c'>
				<Loader />
			</div>
		);

	return (
		<div className='w-full max-w-24 mx-auto'>
			<h2 className='text-center font-semibold mb-1 text-lg text-black dark:text-white'>
				Cursos que tutoriza {worker.displayName}
			</h2>
			<div className='border border-gray-light dark:border-gray-dark rounded-lg px-1'>
				{mapCourses(data.course_admin_find_by_id_array)}
			</div>
		</div>
	);
};

/**
 * Graphql query to get the details of the courses tutored by the user.
 * @param courseIds Array of course ids
 */
const getCourses = (courseIds: string[]) => {
	const { data, loading } = useQuery(
		GraphqlCourse.course_admin_find_by_id_array,
		{
			variables: { courseIds },
		}
	);

	return { data, loading };
};

/**
 * Renders the list of courses tutored by the user.
 * @param courses Array of courses
 */
const mapCourses = (courses: ICourse[]) => {
	if (courses.length)
		return courses.map((i, index) => (
			<Link
				to={MainPaths.COURSE.replace(':id', i._id)}
				key={index}
				className={`flex-s-c p-1 ${index > 0 && 'border-t border-gray-300'}`}>
				<CoursePhoto
					className='h-2_25 w-2_25 rounded-full'
					title={i.title}
					photo={i.image}
				/>
				<span className='ml-1_5 text-black dark:text-white'>{i.title}</span>
			</Link>
		));
	else
		return (
			<p className='text-center p-1 text-black dark:text-white'>
				El usuario no tiene cursos asociados
			</p>
		);
};

export default PageWorkerCourses;
