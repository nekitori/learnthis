import ActiveSpan from '@Components/workers/active-span';
import RolesSpans from '@Components/workers/roles-spans';
import UserPhoto from '@Components/workers/user-photo';
import { WorkerProfileProps } from '@Interfaces/props/worker-profile-props.interface';
import { IWorker } from '@Interfaces/worker.interface';
import { FC } from 'react';

/**
 * Subpage component to display a summary of the worker's data.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 *
 * @param props.worker Worker data
 */
const PageWorkerMain: FC<WorkerProfileProps> = ({ worker }) => {
	const table = renderTable(worker);

	return (
		<>
			<div className='xssm:w-full max-w-20 flex-c-c flex-wrap pt-1 pb-2 mdlg:px-2'>
				<div className='w-2/5'>
					<UserPhoto
						className='rounded-full h-6 w-6 xssm:w-5 xssm:h-5 overflow-hidden'
						name={worker.name}
						photo={worker.photo}
						surname={worker.surname}
					/>
				</div>
				<div className='w-3/5'>
					<div className='flexcol-c-s ml-1'>
						<span className='font-semibold text-lg text-black dark:text-white'>
							{worker.displayName}
						</span>
						<span className='text-black dark:text-white'>
							@{worker.username || 'sin_username'}
						</span>
					</div>
				</div>
				<div className='w-full mt-1_5'>
					<p className='text-left text-black dark:text-white'>
						{worker.bio || 'Sin bio'}
					</p>
				</div>
			</div>
			<ul className='w-full'>{table}</ul>
		</>
	);
};

/**
 * Renders the worker data table.
 * @param worker Worker data
 */
const renderTable = (worker: IWorker) => {
	const data = [
		['Estado', <ActiveSpan active={worker.active} />],
		['Roles', <RolesSpans roles={worker.roles} />],
		['Nombre', worker.name],
		['Apellidos', worker.surname],
		['Email', worker.email],
		['Teléfono', worker.phone || '-'],
	];

	return data.map((i, index) => (
		<li className='flex-c-c w-full py-0_25' key={index}>
			<span className='w-1/3 text-right pr-1 font-semibold text-black dark:text-white'>
				{i[0]}:
			</span>
			<span className='w-2/3 text-left pl-1 text-black dark:text-white'>
				{i[1]}
			</span>
		</li>
	));
};

export default PageWorkerMain;
