import { CommonErrors } from '@Common/enums/common-errors.enum';
import { cleanObject, trimAllStrings } from '@Common/utils/clean-object';
import { SectionModifyDto } from '@Courses/dto/section-modify.dto';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { SectionValidation } from 'learnthis-utils';

@Injectable()
export class SectionModifyPipe implements PipeTransform {
	transform(value: SectionModifyDto): SectionModifyDto {
		if (!Object.keys(value).length)
			throw new BadRequestException(CommonErrors.NOTHING_TO_MODIFY);
		const { title, description } = value;

		const errors = [];

		if (title && !SectionValidation.titleValidation(title))
			errors.push(CommonErrors.FORMAT_INVALID_TITLE);

		if (description && !SectionValidation.descriptionValidation(description))
			errors.push(CommonErrors.FORMAT_INVALID_DESCRIPTION);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		cleanObject(value);
		trimAllStrings(value);

		return value;
	}
}
