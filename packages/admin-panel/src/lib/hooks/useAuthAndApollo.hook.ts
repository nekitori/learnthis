import { IWorker } from '@Interfaces/worker.interface';
import {
	createApolloClient,
	generateApolloClient,
} from '@Lib/apollo/apollo-client';
import { IAuthState } from '@Lib/context/auth.context';
import { GraphqlWorker } from 'learnthis-utils';
import { useEffect, useState } from 'react';

export const LOCAL_STORAGE_JWT = 'jwt';

/**
 * Hook for manage auth state and linked apollo client
 */
export const useAuthAndApollo = () => {
	const [authState, setAuthState] = useState<IAuthState>({
		jwt: localStorage.getItem(LOCAL_STORAGE_JWT),
		worker: undefined,
	});

	const [apolloClient, setApolloClient] = useState(
		generateApolloClient(() => logout())
	);

	const logout = () => {
		localStorage.removeItem(LOCAL_STORAGE_JWT);
		setAuthState({
			jwt: null,
			worker: null,
		});
		setApolloClient(createApolloClient(() => logout()));
	};

	const login = (jwt: string, worker: IWorker) => {
		localStorage.setItem(LOCAL_STORAGE_JWT, jwt);
		setAuthState({
			jwt,
			worker,
		});
		setApolloClient(createApolloClient(() => logout()));
	};

	const updateUser = (updateData: Partial<IWorker>) => {
		setAuthState(oldValue => ({
			...oldValue,
			worker: { ...((oldValue.worker as IWorker) || {}), ...updateData },
		}));
	};

	useEffect(() => {
		const loadCurrentWorker = async () => {
			if (authState.jwt && !authState.worker) {
				try {
					const query = GraphqlWorker.current_worker;
					const { data } = await apolloClient.query({
						query,
					});
					if (data.current_worker) {
						updateUser({ ...data.current_worker });
					} else {
						logout();
					}
				} catch (error) {
					logout();
				}
			}
		};
		loadCurrentWorker();
	}, [authState]);

	return {
		logout,
		login,
		authState,
		updateUser,
		apolloClient,
	};
};
