import { Controller, forwardRef, Get, Module, UseGuards } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GqlModuleOptions, GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { Env } from '../enums/env.enum';
// import { prometheusPlugin } from '@thecodenebula/apollo-prometheus-plugin';
import createMetricsPlugin from 'apollo-metrics';
import { Registry, collectDefaultMetrics } from 'prom-client';
import { WorkersModule } from '@Workers/workers.module';
import { BasicAuthGuard } from '@Common/auth/basic-auth.guard';
import { config } from 'dotenv';

config();

const instance = process.env.NODE_APP_INSTANCE || '0';

const register = new Registry();
register.setDefaultLabels({
	NODE_APP_INSTANCE: 'instance_' + instance,
});
collectDefaultMetrics({ register: register });

const apolloMetricsPlugin = createMetricsPlugin(register);

/**
 * Endpoint to get Graphql metrics from Prometheus.
 */
@Controller('metrics')
class MetricsController {
	@Get()
	@UseGuards(BasicAuthGuard)
	index() {
		return register.metrics();
	}
}

/**
 * Module that manages Graphql schema, playground and plugins.
 */
@Module({
	imports: [
		GraphQLModule.forRootAsync({
			useFactory: (configService: ConfigService): GqlModuleOptions => {
				const isDev = configService.get(Env.NODE_ENV) === 'dev';

				return {
					autoSchemaFile: join(__dirname, '../../schema.gql'),
					sortSchema: true,
					debug: isDev,
					tracing: true,
					playground: isDev,
					plugins: [apolloMetricsPlugin as any],
				};
			},
			inject: [ConfigService],
		}),
		forwardRef(() => WorkersModule),
	],
	controllers: [MetricsController],
})
export class GraphqlModule {}
